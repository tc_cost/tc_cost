
import re
import zipfile

import numpy as np
import pandas as pd
import pycountry
import xarray as xr

import tc_cost.util.constants as u_const


SSP_MODEL = "OECD Env-Growth"

SSP_SCENARIO_SUFFIX = "_v9_130325"

SSP_VAR_GDP = 'GDP|PPP|billion US$2005/yr'

SSP_VAR_POP = 'Population|million'


def load_ssp_data():
    if u_const.SSP_FILE_NC.exists():
        print(f"Loading cached data from {u_const.SSP_FILE_NC} ...")
        return xr.open_dataset(u_const.SSP_FILE_NC)

    print(f"Loading full SSP database from {u_const.SSP_FILE_CSV} ...")
    df = pd.read_csv(u_const.SSP_FILE_CSV)
    assert (
        df[['VARIABLE']].drop_duplicates().shape[0]
        == df[['VARIABLE', 'UNIT']].drop_duplicates().shape[0]
    )
    df['VARIABLE'] += "|" + df['UNIT']
    df = df.drop(columns=['UNIT'])

    id_vars = ['MODEL', 'SCENARIO', 'REGION', 'VARIABLE']
    ds_coords = ['MODEL', 'SCENARIO', 'REGION', 'YEAR']
    ds = (
        df.melt(id_vars=id_vars, var_name="YEAR")
        .pivot_table(values='value', columns='VARIABLE', index=ds_coords)
        .to_xarray()
    )
    ds = ds.sel(MODEL=SSP_MODEL).drop_vars("MODEL")
    ds = ds.sel(SCENARIO=ds.SCENARIO.str.endswith(SSP_SCENARIO_SUFFIX))
    ds['SCENARIO'] = ds.SCENARIO.str.replace(r"^.*(SSP[1-5]).*$", r"\1", regex=True)
    ds = ds.rename({"SCENARIO": "SSP", "REGION": "ISO", "YEAR": "year"})

    ds['year'] = ds['year'].astype(int)
    ds['gdp'] = ds[SSP_VAR_GDP] * 1e9
    ds['pop'] = ds[SSP_VAR_POP] * 1e6
    ds = ds[['gdp', 'pop']]
    ds = (
        ds.reindex(year=np.arange(ds.year.min(), ds.year.max() + 1))
        .interpolate_na(dim="year", method="cubic", order=3)
    )
    ds['gdp'].values[ds['gdp'].values <= 0] = np.nan
    ds['gdppc'] = ds['gdp'] / ds['pop']

    # restrict to countries that have at least some notnull entries for each SSP
    mask = ds["gdppc"].notnull().any(dim="year").all(dim="SSP")
    print("Discarding SSP entries for", ds["ISO"].sel(ISO=~mask).values)
    ds = ds.sel(ISO=mask)

    ds['gdppc_ch'] = np.log(ds['gdppc']).diff(dim="year")

    print(f"Writing to {u_const.SSP_FILE_NC} ...")
    encoding = {v: {'zlib': True} for v in ds.data_vars}
    ds.to_netcdf(u_const.SSP_FILE_NC, encoding=encoding)

    return ds


def read_betas_from_da(da, group_type, group_df):
    countries = da.coeff.sel(coeff=da.coeff.str.contains("factor(ISO)", regex=False))
    countries = countries.str.replace(r".*factor\(ISO\)([A-Za-z]+).*", r"\1", regex=True)
    countries = np.unique(countries.values)

    beta_labels = [v for v in da.coeff.values if "aff" in v]
    beta_df = da.sel(coeff=beta_labels).to_dataframe().reset_index()

    if group_type == "glb":
        beta_ds = xr.Dataset({"ISO": countries})
        beta_ds['coeff'] = (
            beta_df.rename(columns={'coeff': 'aff'})
            .set_index(['elt', 'aff'])['Estimate']
            .to_xarray()
        )
    else:
        labels_splitted = (
            beta_df["coeff"]
            .str.replace(r"factor\((ISO|group)\)([A-Za-z]+)", r"\2", regex=True)
            .str.replace(r"(.*):(aff[0-9]+)", r"\2:\1", regex=True)
            .str.split(":", n=1, expand=True)
        )
        beta_df['aff'] = labels_splitted[0]
        if group_type == "grp":
            beta_df["group"] = labels_splitted[1]
            beta_df = beta_df.merge(group_df, on="group").drop(columns=["group"])
        else:
            beta_df["ISO"] = labels_splitted[1]
        beta_ds = beta_df.drop(columns="coeff").set_index(['elt', 'ISO', 'aff']).to_xarray()

    beta_ds['aff'] = beta_ds['aff'].str.replace("aff", "").astype(int)

    return beta_ds


def read_betas(timehorizon, subdir=None, group_type="ctry",
               nobootstrap=False, use_ols=False, use_temp=False, legacy=False, nb_elt=None):
    if legacy:
        return (
            read_ctry_betas_legacy(timehorizon)
            .set_index(["ISO", "aff"]).to_xarray()
        )

    if nobootstrap:
        suffix = f"_nb{nb_elt or ''}-{'ols' if use_ols else 'glmrob'}"
    else:
        suffix = "" if subdir is None else f"_{subdir.replace('/', '_')}"
        if subdir == "bootstrap":
            suffix += f"{'-ols' if use_ols else ''}{'-temp' if use_temp else ''}"
    cache_fname = f"{group_type}{suffix}_{u_const.INDICATOR}_{timehorizon}.nc"
    cache_path = u_const.CACHE_DIR / "betas" / cache_fname
    if cache_path.exists():
        specific = 'global' if group_type == 'glb' else f"{group_type}-specific"
        print(f"Reading cached {specific} data from {cache_path.name} ...")
        return xr.open_dataset(cache_path)

    if nobootstrap:
        coeff_paths = get_coeff_paths_nobootstrap(
            timehorizon, group_type=group_type, use_ols=use_ols, use_temp=use_temp, elt=nb_elt)
    elif subdir == "bootstrap":
        suffix = f"{'_ols' if use_ols else ''}{'_temp' if use_temp else ''}"
        coeff_paths = [
            u_const.BOOTSTRAP_DIR / "estimates" / f"coeffs_{timehorizon}{suffix}.nc"
        ]
    else:
        raise NotImplementedError(
            "Support for groups with nobootstrap=False has been removed from the code in 2023-03."
        )

    group_df = None
    if group_type == "grp":
        group_name = subdir.split("/")[-1]
        group_df = pd.read_csv(u_const.INPUT_DIR / "groups" / f"{group_name}.csv")

    print(f"Reading raw coefficients from {len(coeff_paths)} files ...")
    betas = []
    for path in coeff_paths:
        if path.name.endswith(".nc"):
            ds = xr.open_dataset(path)
            if nobootstrap:
                ds['elt'].values[:] = [path.stem]
        else:
            beta_df = (
                pd.read_csv(path)
                .reset_index()
                .rename(columns={"index": "coeff", 'coefficients.Estimate': "Estimate"})
            )
            beta_df['elt'] = (
                path.stem if nobootstrap
                else "/".join([p.name for p in list(path.parents)[:3][::-1]])
            )
            ds = beta_df.set_index(["elt", "coeff"]).to_xarray()
        betas.append(read_betas_from_da(ds['Estimate'], group_type, group_df))
    betas_ds = xr.combine_nested(betas, "elt") if len(betas) > 1 else betas[0]

    print(f"Writing to {cache_path} ...")
    betas_ds = betas_ds.reindex(elt=sorted(betas_ds.elt.values))
    betas_ds['elt'] = betas_ds['elt'].astype(object)
    encoding = {v: {'zlib': True} for v in betas_ds.data_vars}
    betas_ds.to_netcdf(cache_path, encoding=encoding)

    return betas_ds


def get_coeff_paths_nobootstrap(timehorizon, group_type="ctry",
                                use_ols=False, use_temp=False, elt=None):
    base_path = u_const.NOBOOTSTRAP_DIR
    suffix = f"{'_ols' if use_ols else '_glmrob'}{'_temp' if use_temp else ''}"

    # aggregate across all periods of this length within this period
    max_period = (1971, 2015)
    yearcount = 35

    fnames = []
    for minyear in range(max_period[0], max_period[1] - yearcount + 2):
        maxyear = minyear + yearcount - 1
        fname = (
            f"{group_type}_{u_const.INDICATOR}_{minyear}-{maxyear}_{timehorizon}{suffix}.nc"
        )
        if elt is None or elt in fname:
            fnames.append(fname)
    return [base_path / "coefficients" / fname for fname in fnames]


def read_ctry_betas_legacy(timehorizon):
    # This refers to beta + alpha values from the legacy regression model.
    # The raw data is overwritten already, but we can use cache files we produced earlier.
    cache_path = lambda coeff: (
        u_const.CACHE_DIR / "betas" / f"legacy_{coeff}_{u_const.INDICATOR}_{timehorizon}.csv"
    )
    print(f"Reading cached data from {cache_path('alphas')} ...")
    df_alphas = pd.read_csv(cache_path('alphas'))
    df_betas = pd.read_csv(cache_path('betas'))

    cols = [f'aff{i}' for i in range(0, timehorizon + 1)]
    betas_df = pd.merge(df_alphas, df_betas, on="elt", how="left", suffixes=("", "_glb"))
    for c in cols:
        betas_df[c] += betas_df[f"{c}_glb"]
    betas_df = betas_df.drop(columns=[f"{c}_glb" for c in cols])
    betas_df = betas_df.melt(id_vars=['ISO', 'elt'], var_name='aff', value_name='coeff')
    return betas_df


def rename_sm_params(ser):
    ser = ser.str.replace("C(", "factor(", regex=False)
    ser = ser.str.replace(r"\[(T\.)?(.*)\]", r"\2", regex=True)
    ser = ser.str.replace(r"^Intercept$", "(Intercept)", regex=True)
    return ser


def load_tce_dat_annual():
    df = (
        pd.read_csv(u_const.TCE_DAT_FILE)
        .rename(columns={u_const.TCE_DAT_INDICATOR_COL: "affected", "ISO3": "ISO"})
    )
    df = df.loc[df["affected"] > 0, ["year", "ISO", "affected"]]
    ds = df.groupby(["year", "ISO"]).sum().to_xarray()
    ds = ds.reindex(year=np.arange(ds.year.min(), ds.year.max() + 1)).fillna(0)
    return ds


def load_tce_dat_annual_shares():
    ds = load_tce_dat_annual()
    ds['pop'] = (
        pd.read_csv(u_const.TCDATA_DIR / "TotalPopulation.csv")
        # there are some duplicated entries for ISO="UMI", drop those:
        .groupby(['year', 'ISO']).last()
        .to_xarray()['pop']
    )

    # the Isle of Man has 0 population, discard it
    ds = ds.sel(ISO=(ds["ISO"] != "IMN"))
    assert (ds["pop"] == 0).any(dim="year").sum(dim="ISO") == 0

    ds['affected'] /= ds["pop"]
    return ds


def load_avg_aff(timehorizon):
    ds = load_tce_dat_annual_shares()
    ds['avg_aff'] = (
        ds['affected']
        .rolling(year=timehorizon + 1)
        .construct(year="aff")
        .isel(aff=slice(None, None, -1))
        .mean(dim="year")
    )
    return ds


def load_hist_gdppc():
    cache_file = u_const.HIST_GDP_NC
    if cache_file.exists():
        print(f"Loading cached GDP per capita data from {cache_file} ...")
        return xr.open_dataset(cache_file)

    print(f"Loading full IHME GDP database from {u_const.HIST_GDP_CSV} ...")
    df = (
        pd.read_csv(u_const.HIST_GDP_CSV, encoding="latin1")
        .rename(columns={u_const.IHME_GDPPC_COL: "gdppc", " Year ": "year", " ISO3 ": "ISO"})
        [["ISO", "year", "gdppc"]]
    )
    df['ISO'] = df['ISO'].str.replace(" ", "", regex=False)
    df['gdppc'] = (
        df['gdppc']
        .str.replace(" ", "", regex=False)
        .str.replace(",", "", regex=False)
        .astype(float)
    )
    ds = df.set_index(["ISO", "year"]).to_xarray()
    ds['gdppc_ch'] = np.log(ds['gdppc']).diff(dim="year")

    print(f"Writing to {cache_file} ...")
    encoding = {v: {'zlib': True} for v in ds.data_vars}
    ds.to_netcdf(cache_file, encoding=encoding)

    return ds


def load_bhm_data():
    df = pd.read_stata(u_const.INPUT_DIR / "GrowthClimateDataset.dta")
    df = df[[c for c in df.columns if not c.startswith("_")]]
    df = df.rename(columns={
        "iso": "ISO",
        "growthWDI": "gdppc_ch",
        "UDel_temp_popweight": "temp",
        "UDel_precip_popweight": "precip",
        "UDel_precip_popweight_2": "precip2",
        "time": "time",
        "time2": "time2",
    })
    df = (
        df[["ISO", "year", "gdppc_ch", "temp", "precip", "precip2", "time", "time2"]]
        .dropna(how="any")
        .reset_index(drop=True)
    )
    for col in df.columns:
        df[col] = df[col].astype(
            str if col == "ISO" else
            int if col in ["year", "time", "time2"]
            else np.float64
        )
    df["ISO"] = (
        df["ISO"]
        # "Zaire" is now "Democratic Republic of the Congo"
        .str.replace("ZAR", "COD")
        # Romania changed its alpha3 code in 2002
        .str.replace("ROM", "ROU")
    )
    df["temp2"] = df["temp"]**2
    return df.set_index(["year", "ISO"]).to_xarray()


def load_hist_temp(source="gswp3-w5e5"):
    # source is one of "burke", "hazem", or "gswp3-w5e5"
    # in any case, the countries that are neither TC-affected nor in the BHM study are excluded

    # load BHM data first, because it's used in all cases to define the set of countries
    ds_bhm = load_bhm_data()

    if source == "hazem":
        # this data (from Hazem) includes years until 2017
        df = (
            pd.read_csv(u_const.INPUT_DIR / "mean_temperature.csv")
            .rename(columns={"meantemp": "temp"})
        )
    elif source.startswith("gswp3-w5e5"):
        # this data (from Christof) includes years until 2019
        # there is also a `_legacy` version of the data where the population weights were too
        # low for coastal pixels (fixed on 2023-05-30)
        df = (
            pd.read_csv(u_const.INPUT_DIR / f"mean_temperature_{source}.csv")
            .rename(columns={"iso": "ISO", "tasMean_pop": "temp"})
            [["year", "ISO", "temp"]]
        )

        # KNA is missing data starting from 1950, we replace it with ATG below
        df = df[df["ISO"] != "KNA"].reset_index(drop=True)

        # WSM has a single missing entry in 1967
        cty_mask = (df["ISO"] == "WSM")
        df.loc[cty_mask, "temp"] = df.loc[cty_mask, "temp"].interpolate(method="linear")

        # convert K to °C
        df["temp"] -= 273.15
    elif source == "burke":
        # the Burke data is only until 2010
        df = ds_bhm[["temp"]].to_dataframe().reset_index()
    else:
        raise NotImplementedError

    # discard NaN temperatures
    df = df.dropna(subset=["temp"])

    add_countries = []
    for c1, c2 in [
        # both Hazem's and Burke's data are missing these countries
        # we replace them by values of neighboring countries
        ("BRB", "TTO"),
        ("LCA", "TTO"),
        ("TON", "WSM"),
        ("HKG", "VNM"),
    ]:
        if not (df["ISO"] == c1).any():
            print(f"[{source}] Set missing {c1} temperatures to {c2}'s temperatures...")
            row = df[df["ISO"] == c2].copy()
            row["ISO"] = c1
            add_countries.append(row)
    df = pd.concat([df] + add_countries)

    # only consider countries that are either TC-affected or in the BHM study
    l_countries = np.union1d(u_const.L_COUNTRIES, ds_bhm["ISO"].values)
    df = df[df["ISO"].isin(l_countries)].reset_index(drop=True)

    return df.set_index(["ISO", "year"]).to_xarray()


def load_ssp_pop_from_grid(ssp):
    country_id2iso = pd.read_csv(u_const.ISO_NUMERIC_CSV).rename(columns={"ID": "cty_numeric"})
    return (
        pd.read_csv(
            u_const.TC_EMULATOR_DIR / "population" / (
                f"population_by_country_{ssp.lower()}.csv"
            )
        )
        .merge(country_id2iso, on="cty_numeric", how="inner")
        .set_index(["ISO", "year"])
        .to_xarray()["population"]
    )


def load_affected_people(rcp, ssp, scenario, shares=False):
    country_id2iso = pd.read_csv(u_const.ISO_NUMERIC_CSV).rename(columns={"ID": "cty_numeric"})
    da = (
        pd.concat(
            [
                pd.read_csv(
                    u_const.TC_EMULATOR_DIR / scenario / (
                        f"TC-people_affected_by_country_gcm-{gcm}_{rcp}"
                        f"_minwind-{u_const.MINWIND}_{ssp.lower()}.csv"
                    )
                )
                .set_index(["year", "realisation", "cty_numeric"])
                for gcm in u_const.L_GCM
            ],
            keys=u_const.L_GCM,
            names=["gcm"],
        )
        .reset_index()
        .merge(country_id2iso, on="cty_numeric", how="inner")
        .set_index(["year", "realisation", "gcm", "ISO"])['affected_corr_country']
        .to_xarray()
        .reindex(year=np.arange(1980, 2101))
        .reindex(realisation=np.arange(0, 100))
        .reindex(gcm=u_const.L_GCM)
        .fillna(0)
    )
    if shares:
        da = da / load_ssp_pop_from_grid(ssp)
    return da


def _wid_get_country_data(alpha2):
    path = u_const.WID_INCOME_DIR / f"{alpha2}.csv"
    if path.exists():
        return pd.read_csv(path)

    try:
        match = pycountry.countries.lookup(alpha2)
        alpha3, name = match.alpha_3, match.name
    except LookupError:
        return None

    with zipfile.ZipFile(u_const.WID_ALL_DATA, "r") as wid_zip:
        with wid_zip.open(f"WID_data_{alpha2}.csv") as fp:
            wid_df = pd.read_csv(fp, encoding="Windows-1252", delimiter=";", keep_default_na=False)

    dfs = {}
    for key, variables in u_const.WID_VARIABLES.items():
        var_df = wid_df[np.isin(wid_df.variable, variables)]
        matching_vars = np.unique(var_df.variable.values)
        if matching_vars.size == 0:
            print(f"{key} variable not reported for country {name} ({alpha2})!")
            return None
        if matching_vars.size > 1:
            print(f"Several {key} variables matching for {alpha2}: {', '.join(matching_vars)}")
            return None
        dfs[key] = var_df.rename(columns={"value": key})

    inc_df = dfs["income_lc"]
    for key, df in dfs.items():
        if key != "income_lc":
            inc_df = inc_df.merge(df[['year', key]], on="year", how="left")
    inc_df = inc_df.rename(columns={"country": "alpha2"})
    inc_df['alpha3'] = alpha3
    inc_df['name'] = name
    inc_df.to_csv(path, index=None)

    return inc_df


def _wid_print_variable_info():
    with zipfile.ZipFile(u_const.WID_ALL_DATA, "r") as wid_zip:
        with wid_zip.open("WID_metadata_FR.csv") as fp:
            df = pd.read_csv(fp, encoding="Windows-1252", delimiter=";", keep_default_na=False)
    sel_variables = sum(u_const.WID_VARIABLES.values(), [])
    df = df[np.isin(df.variable, sel_variables)]
    for v, desc in zip(df.variable.values, df.longtype.values):
        print(f"Description of variable {v}: {desc}")
        print("")


def _wid_get_country_list():
    l_countries = []
    with zipfile.ZipFile(u_const.WID_ALL_DATA, "r") as wid_zip:
        for n in wid_zip.namelist():
            result = re.match(r"WID_data_([A-Z]{2})\.csv", n)
            if result is None:
                continue
            alpha2 = result.group(1)
            if alpha2 == "KV":
                # replace the unofficial alpha-2 code for Kosovo
                alpha2 = "XK"
            l_countries.append(alpha2)
    return l_countries


def _wid_extract_income():
    if u_const.WID_INCOME_CSV.exists():
        return pd.read_csv(u_const.WID_INCOME_CSV)

    _wid_print_variable_info()

    l_countries = _wid_get_country_list()

    invalid_countries = []
    inc_data = {}
    for alpha2 in l_countries:
        inc_df = _wid_get_country_data(alpha2)
        if inc_df is None:
            invalid_countries.append(alpha2)
        else:
            inc_data[alpha2] = inc_df
    print(f"No data or invalid: {', '.join(invalid_countries)}")

    print("")
    percentiles = ["p0p100"] + [f"p{i}p{i+10}" for i in range(0, 100, 10)]
    no_data_countries = []
    for alpha2, inc_df in inc_data.items():
        for perc in percentiles:
            df = inc_df[inc_df.percentile == perc]
            if (df.income_lc.isna() | (df.income_lc == 0)) .sum() > 0:
                no_data_countries.append(f"{inc_df.name.values[0]} ({alpha2})")
    print(f"Percentiles missing: {', '.join(no_data_countries)}")

    cat_df = pd.concat([df[np.isin(df.percentile, percentiles)] for df in inc_data.values()])
    cat_df = cat_df.sort_values(by=["alpha3", "percentile", "year"])
    cat_df = cat_df.drop(columns=["variable", "pop", "age"]).reset_index(drop=True)
    cat_df = cat_df[["alpha3", "alpha2", "name", "percentile", "year", "income_lc", "ppp"]]
    cat_df.to_csv(u_const.WID_INCOME_CSV, index=None)
    return cat_df


def load_wid_income(l_countries, year, percentile):
    # Average  pre-tax national income per group (includes different income groups)
    l_countries = np.array(l_countries)

    df = _wid_extract_income().rename(columns={"alpha3": "ISO"})
    df = df[
        (df['percentile'] == percentile)
        & df.ISO.isin(l_countries)
        & (df.year == year)
    ].copy()
    df["income_ppp"] = df["income_lc"] / df["ppp"]

    missing_mask = ~np.isin(l_countries, df.ISO.values)
    print(f"No {percentile} income data for countries: ",
          ', '.join(sorted(l_countries[missing_mask])))

    return df[["ISO", "income_ppp"]].copy()
