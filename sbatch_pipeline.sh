
FILE_SH="slurm/run_script_arr.sh"
CMD="sbatch"
FLAGS="--parsable --qos standby"

run_pipeline(){
J_BOOTSTRAP=$(${CMD} ${FLAGS} --array 0-0 ${FILE_SH} bootstrap.bootstrap "$@")
J_FIT=$(${CMD} ${FLAGS} -d afterany:${J_BOOTSTRAP} --array 0-15 ${FILE_SH} bootstrap.fit "$@")
J_PROJ=$(${CMD} ${FLAGS} -d afterany:${J_FIT} --array 0-95 ${FILE_SH} bootstrap.proj "$@")
J_PROJ_AGG=$(${CMD} ${FLAGS} -d afterany:${J_PROJ} --array 0-41 ${FILE_SH} aggregate.proj "$@")

J_DEC=$(${CMD} ${FLAGS} -d afterok:${J_PROJ} --array 0-255 ${FILE_SH} bootstrap.dec "$@")
J_DEC_AGG=$(${CMD} ${FLAGS} -d afterany:${J_DEC} --array 0-255 ${FILE_SH} aggregate.dec "$@")
J_DEC_UNC=$(${CMD} ${FLAGS} -d afterany:${J_DEC_AGG} --array 0-599 ${FILE_SH} aggregate.dec_unc "$@")
J_DEC_UNCC=$(${CMD} ${FLAGS} -d afterany:${J_DEC_UNC} --array 0-5 ${FILE_SH} aggregate.dec_unc_combine "$@")

J_DF=$(${CMD} ${FLAGS} -d afterok:${J_PROJ} --array 0-3359 ${FILE_SH} bootstrap.df "$@")
J_DFTEST=$(${CMD} ${FLAGS} -d afterok:${J_PROJ} --array 0-639 ${FILE_SH} bootstrap.df_ftest "$@")
J_DF_AGG=$(${CMD} ${FLAGS} -d afterany:${J_DF} --array 0-41 ${FILE_SH} aggregate.df "$@")
J_SCC=$(${CMD} ${FLAGS} -d afterany:${J_DF} --array 0-383 ${FILE_SH} bootstrap.scc "$@")
J_SCC_AGG=$(${CMD} ${FLAGS} -d afterany:${J_SCC} --array 0-95 ${FILE_SH} aggregate.scc "$@")
}

run_pipeline ols temp
