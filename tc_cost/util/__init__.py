
from datetime import datetime as dt
import signal


class GracefulInterruptHandler():
    """ Context manager for handling SIGTERM (e.g. when running in `standby` QOS on SLURM)

    Example
    -------
    >>> with GracefulInterruptHandler() as h:
    >>>     # do something that may not be interrupted ...
    >>>     if h.interrupted:
    >>>         print("Quitting gracefully after SIGTERM ...")
    >>>         sys.exit()
    >>> # if there was no interruption, the code continues here
    """
    def __init__(self, sigs=[signal.SIGTERM, signal.SIGINT]):
        self.sigs = sigs

    def __enter__(self):
        self.interrupted = False
        self.released = False

        self.original_handlers = []
        for sig in self.sigs:
            self.original_handlers.append(signal.getsignal(sig))
            signal.signal(sig, self.handle)

        return self

    def __exit__(self, type, value, tb):
        self.release()

    def handle(self, signum, frame):
        print(f"[{dt.now()}] Signal ({signum}) received. Cleaning up ...")
        self.release()
        self.interrupted = True

    def release(self):
        if self.released:
            return False

        for sig, handler in zip(self.sigs, self.original_handlers):
            signal.signal(sig, handler)

        self.released = True
        return True
