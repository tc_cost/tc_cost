
import numpy as np
import pandas as pd
import xarray as xr

from tc_cost.sourcedata.fig1 import get_data_qs
import tc_cost.util.constants as u_const


def data_figs02(max_timehorizon, use_ols, use_temp, glb, average_annual_aff, nobootstrap):
    suffix = f"{'_ols' if use_ols else ''}{'_temp' if use_temp else ''}"

    base_path = u_const.BOOTSTRAP_DIR / "estimates"

    df = []
    for l in range(0, max_timehorizon + 1):
        ds = xr.open_dataset(base_path / f"coeffs_{l}{suffix}.nc")
        ds = ds.sel(coeff=ds['coeff'].str.contains("aff"))

        # two-sided hypothesis test with H0 being that the distribution is shifted to mean zero
        coeff_est = ds['Estimate'].sel(elt=ds.elt != "original")
        means_bs = coeff_est.mean(dim="elt")
        da = (
            (np.sign(means_bs) * (coeff_est - 2 * means_bs) >= 0).mean(dim="elt")
            + (np.sign(means_bs) * coeff_est <= 0).mean(dim="elt")
        ).rename(coeff="aff")
        da = da.assign_coords(aff=[int(s[3:]) for s in da["aff"].values])
        ds = (
            get_data_qs(l, glb, nobootstrap, average_annual_aff, use_ols, use_temp)
            .sel(ISO="GLB")
            .reindex(aff=np.arange(0, l + 1))
            ["coeff_cum"].to_dataset(dim="quantile")
            .rename_vars({0.5: "median", 0.167: "17", 0.833: "83"})
            [["median", "17", "83"]]
        )
        ds["p_value"] = da
        _df = (
            ds
            .to_dataframe()
            .reset_index()
            .drop(columns="ISO")
        )
        _df["L"] = l
        df.append(_df)
    return pd.concat(df).reset_index(drop=True).rename(columns={"aff": "l"})


def main():
    max_timehorizon = 15
    use_ols = True
    use_temp = True
    glb = True
    average_annual_aff = False
    nobootstrap = False

    path = u_const.SOURCEDATA_DIR / "figs02.csv"
    df = data_figs02(max_timehorizon, use_ols, use_temp, glb, average_annual_aff, nobootstrap)
    print(f"Writing to {path} ...")
    df.to_csv(path, index=None)


if __name__ == "__main__":
    main()
