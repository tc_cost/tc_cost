
import matplotlib.gridspec as mgridspec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from tc_cost.plot.fig1 import plot_impact_hist
import tc_cost.util.constants as u_const
import tc_cost.util.plot as u_plot

def main():
    u_plot.main_setup_mpl()

    fig_no_min = 5
    n_figs = 7
    for fig_no in range(fig_no_min, fig_no_min + n_figs):
        data = pd.read_csv(u_const.SOURCEDATA_DIR / f"figs{fig_no:02d}.csv")
        l_countries = (
            data[data["variable"] == "median"]
            .groupby("ISO")["value"]
            .min()
            .sort_values(ascending=True).index
        )
        ymax = 1.33 * data.loc[data["variable"] != "median", "value"].max()

        fig = plt.figure(figsize=(u_const.PLOT_WIDTH_IN, 1.2 * len(l_countries)))
        gs = mgridspec.GridSpec(len(l_countries), 1)

        axes = [fig.add_subplot(gs[0])]
        axes += [
            fig.add_subplot(gs[i + 1], sharex=axes[0], sharey=axes[0])
            for i, _ in enumerate(l_countries[1:])
        ]

        ax_legend = axes[-1].inset_axes([0.0, 0.2, 0.45, 0.8], frameon=False)
        ax_legend.set_xticks([])
        ax_legend.set_yticks([])

        for i, c in enumerate(l_countries):
            df = data[data["ISO"] == c].copy()
            ax = axes[i]
            decorate = c == l_countries[-1]
            plot_impact_hist(
                ax, df,
                decorate=decorate,
                legend=(i == 0),
                ax_legend=ax_legend,
                ymax=ymax,
            )

        outpath = u_const.PLOTS_DIR / f"FigureS{fig_no:02d}.pdf"
        print(f"Writing to {outpath} ...")
        fig.savefig(outpath, bbox_inches="tight")


if __name__ == "__main__":
    main()
