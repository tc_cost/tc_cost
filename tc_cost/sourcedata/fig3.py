
import pandas as pd
import xarray as xr

import tc_cost.util.constants as u_const


def load_aggregate_losses(timehorizon, use_ols, use_temp, l_countries=None):
    if l_countries is None:
        l_countries = ["GLB"] + u_const.L_COUNTRIES
    base_path = u_const.BOOTSTRAP_DIR / "growth_losses"
    print(f"Reading aggregate losses from {base_path} ...")

    suffix = f"{'_ols' if use_ols else ''}{'_temp' if use_temp else ''}"
    paths = [
        base_path / f"aggregate_{timehorizon}-{iso}{suffix}.nc"
        for iso in l_countries
    ]
    if len(paths) == 1:
        ds = xr.open_dataset(paths[0])
    else:
        ds = xr.open_mfdataset(paths, combine="nested", concat_dim=["ISO"])
        ds['ISO'] = ("ISO", l_countries)
    return ds


def data_fig3a_ellipses(timehorizon, use_ols, use_temp):
    ds = load_aggregate_losses(timehorizon, use_ols, use_temp, l_countries=["GLB"])
    return (
        ds[["covs", "means"]]
        .to_dataframe()
        .reset_index()
        .drop(columns="quantile")
    )


def data_fig3a_markers(timehorizon, use_ols, use_temp, countries=None):
    countries = ["GLB"] if countries is None else countries
    ds = load_aggregate_losses(timehorizon, use_ols, use_temp, l_countries=countries)
    ds['gmt'] = xr.open_dataset(u_const.GMT_NC)['gmt']
    return (
        ds[["gmt", "cost_rng_66", "cost_mid_66"]]
        .to_dataframe()
        .reset_index()
        .rename(columns={"cost_rng_66": "cost_range", "cost_mid_66": "cost_mid"})
        .drop(columns=["quantile"])
    )


def data_fig3a_lines(timehorizon, use_ols, use_temp, countries=None):
    countries = ["GLB"] if countries is None else countries
    suffix = f"{'_ols' if use_ols else ''}{'_temp' if use_temp else ''}"
    gmt_bounds = (0.8, 4.9)
    ds = xr.open_mfdataset([
        u_const.BOOTSTRAP_DIR / "damage_functions" / (
            f"aggregate_{timehorizon}-{iso}{suffix}.nc"
        ) for iso in countries
    ], combine="nested", concat_dim=["ISO"])
    ds["ISO"] = countries
    ds = ds.sel(gmt=(ds["gmt"] >= gmt_bounds[0]) & (ds["gmt"] <= gmt_bounds[1]))

    df = (
        ds.to_dataframe()["damfun"]
        .unstack(level="quantile")
        .reset_index()
        .rename(columns={0.167: "17", 0.833: "83", 0.5: "50"})
    )
    df.columns.name = None
    cols = ["rcp", "gmt", "50", "17", "83"]
    if countries != ["GLB"]:
        cols = ["ISO"] + cols
    return df[cols]


def data_fig3b(timehorizon, use_ols, use_temp):
    l_countries = u_const.L_COUNTRIES
    l_gcm = ["allgcms"]  # alternatively, include GCMs in uncertainty: u_const.L_GCM

    path = u_const.BOOTSTRAP_DIR / "damage_functions"
    print(f"Reading country-level damage functions from {path} ...")

    suffix = f"{'_ols' if use_ols else ''}{'_temp' if use_temp else ''}"
    ds = xr.open_mfdataset([
        [
            path / f"{timehorizon}-{gcm}-{iso}{suffix}.nc"
            for iso in l_countries
        ] for gcm in l_gcm
    ], combine="nested", concat_dim=["gcm", "ISO"]).sel(coeff="gmt", rcp="__all__").compute()
    ds['ISO'] = ("ISO", l_countries)
    ds['gcm'] = ("gcm", l_gcm)

    qdims = ["realisation", "elt", "gcm"]
    for q in [0.167, 0.833]:
        ds[f"{100 * q:.0f}"] = ds['estimates'].quantile(q=q, dim=qdims)
    ds["median"] = ds['estimates'].median(dim=qdims)

    ds = ds[["median", "17", "83"]]

    # convert all fractions to percentage values for plotting convenience
    df = ds.to_dataframe().drop(columns=["coeff", "quantile", "rcp"])
    df *= 100
    df = df.reset_index()

    # assign the (2024) World Bank classification by income
    df = df.merge(pd.read_csv(u_const.COUNTRY_GROUP_CSV), on="ISO", how="left")

    return (
        df[["group", "ISO", "median", "17", "83"]]
        .sort_values(by=["group", "ISO"])
        .reset_index(drop=True)
    )


def main():
    use_ols = True
    use_temp = True
    timehorizon = 8

    args = (timehorizon, use_ols, use_temp)

    path = u_const.SOURCEDATA_DIR / "fig3a_ellipses.csv"
    df = data_fig3a_ellipses(*args)
    print(f"Writing to {path} ...")
    df.to_csv(path, index=None)

    path = u_const.SOURCEDATA_DIR / "fig3a_markers.csv"
    df = data_fig3a_markers(*args)
    print(f"Writing to {path} ...")
    df.to_csv(path, index=None)

    path = u_const.SOURCEDATA_DIR / "fig3a_lines.csv"
    df = data_fig3a_lines(*args)
    print(f"Writing to {path} ...")
    df.to_csv(path, index=None)

    path = u_const.SOURCEDATA_DIR / "fig3b.csv"
    df = data_fig3b(*args)
    print(f"Writing to {path} ...")
    df.to_csv(path, index=None)


if __name__ == "__main__":
    main()
