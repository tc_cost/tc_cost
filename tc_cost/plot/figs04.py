
import matplotlib.gridspec as mgridspec
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import numpy as np
import pandas as pd

import tc_cost.util.constants as u_const
import tc_cost.util.plot as u_plot


def plot_aff_projection(ax, df, break_ylabel=True):
    ssp = df["SSP"].values[0]
    l_rcp = np.unique(df["scenario"].values)
    l_colors = [u_const.L_RCP_COLORS[rcp] for rcp in l_rcp]

    for color, rcp in zip(l_colors, l_rcp):
        df_rcp = df[df["scenario"] == rcp].copy()
        ax.plot(
            df_rcp["year"],
            100 * df_rcp["aff_50"],
            c=color,
            lw=1.5,
            label="Baseline" if rcp == "baseline" else f"RCP{float(rcp[3:]) / 10:.1f}",
        )
        ax.fill_between(
            df_rcp["year"],
            100 * df_rcp["aff_17"],
            100 * df_rcp["aff_83"],
            facecolor=color,
            alpha=0.2,
            edgecolor="none",
        )

    ax.text(0.5, 0.96, ssp, ha="center", va="top", transform=ax.transAxes)

    ax.set_xlabel("Years")
    ax.set_xticks(np.arange(2020, 2101, 20))

    newline = "\n" if break_ylabel else " "
    ax.set_ylabel(f"Share of people{newline}exposed (%)")
    ax.yaxis.set_major_locator(mticker.MaxNLocator(integer=True))


def plot_aff_regression(ax, df, break_ylabel=True):
    ssp = df["SSP"].values[0]
    l_rcp = u_const.L_RCP_LOWER
    l_rcp_names = u_const.L_RCP
    l_colors = [u_const.L_RCP_COLORS[rcp] for rcp in l_rcp]

    for color, rcp_name, rcp in zip(l_colors, l_rcp_names, l_rcp):
        df_rcp = df[df["rcp"] == rcp].copy()
        ax.scatter(
            df_rcp["gmt"],
            100 * df_rcp["aff_avg"],
            c=color,
            s=6,
            marker="o",
            label=rcp_name,
        )

    ax.plot(df["gmt"], 100 * df["fit"], c="black", lw=0.75)
    ax.fill_between(
        df["gmt"],
        100 * df["fit_min"],
        100 * df["fit_max"],
        facecolor="black",
        alpha=0.3,
        edgecolor="none",
    )
    ax.xaxis.set_major_locator(mticker.MaxNLocator(integer=True))
    ax.yaxis.set_major_locator(mticker.MaxNLocator(integer=True))

    ax.text(0.5, 0.96, ssp, ha="center", va="top", transform=ax.transAxes)

    newline = "\n" if break_ylabel else " "
    ax.set_ylabel(f"Share of people{newline}exposed (%)")
    ax.set_xlabel("Global mean temperature change (°C)")


def plot_gdppc_projection(ax, df, xmax=2118, y_offset=None, yticks=None):
    # For a given country (iso), plot the GDPpc pathways: baseline, RCP2.6, RCP6.0, and RCP8.5.
    ssp = df["SSP"].values[0]
    l_rcp = np.unique(df["scenario"].values)[[1, 0, 2, 3, 4]]
    l_colors = [
        "black" if rcp == "notc" else u_const.L_RCP_COLORS[rcp]
        for rcp in l_rcp
    ]
    l_labels = {
        "notc": "No TC damages",
        "baseline": "Baseline",
        "rcp26": "RCP 2.6",
        "rcp60": "RCP 6.0",
        "rcp85": "RCP 8.5",
    }

    for rcp, color in zip(l_rcp, l_colors):
        label = l_labels[rcp]
        df_rcp = df[df["scenario"] == rcp]
        ax.plot(df_rcp["year"], df_rcp["gdppc50"], c=color, lw=1, label=label)
        if rcp != "notc":
            ax.fill_between(
                df_rcp["year"],
                df_rcp["gdppc17"],
                df_rcp["gdppc83"],
                facecolor=color,
                alpha=0.2,
                edgecolor="none",
            )

    gdppc2100 = {
        rcp: df.loc[(df["scenario"] == rcp) & (df["year"] == 2100), "gdppc50"].values[0]
        for rcp in l_rcp
    }
    rate2100 = {
        rcp: 100 * (1 - gdppc2100[rcp] / gdppc2100["notc" if rcp == "baseline" else "baseline"])
        for rcp in l_rcp
    }

    arrowprops = dict(width=0.5, lw=0, headwidth=2, headlength=3, facecolor='black', shrink=0.01)
    x = 2101
    rate = rate2100['baseline']
    ax.annotate(text="", xy=(x, gdppc2100['baseline']), xytext=(x, gdppc2100['notc']),
                arrowprops=arrowprops)
    ax.text(
        x + 1,
        0.5 * (gdppc2100['notc'] + gdppc2100['baseline']),
        f'{-rate:.1f}%',
        ha="left",
        va="center",
    )
    for i, rcp in enumerate(u_const.L_RCP_LOWER[::-1]):
        x = 2101 + 2 * i
        ax.annotate(text="", xy=(x, gdppc2100[rcp]), xytext=(x, gdppc2100['baseline']),
                    arrowprops=arrowprops)
        rate = rate2100[rcp]

        if y_offset is None:
            y_offset = (0.88, 0.05) if ssp == "SSP2" else (0.93, 0.03)
        y_factor = y_offset[0] + i * y_offset[1]
        ax.text(x + 0.7 * i, y_factor * gdppc2100[rcp], f'{-rate:.1f}%',
                ha="left", va="bottom")
    ax.set_xlim(right=xmax)

    ax.text(0.5, 0.98, ssp, ha="center", va="top", transform=ax.transAxes)

    ax.set_ylabel("Per capita GDP (10,000 US$)")
    yticks = np.arange(6, 20, 2) if yticks is None else yticks
    ax.set_yticks(list(10000 * yticks))
    ax.set_yticklabels([f"{i:d}" for i in yticks])

    ax.set_xlabel("Years")
    ax.set_xticks(np.arange(2020, 2101, 20))



def main():
    u_plot.main_setup_mpl()

    fig = plt.figure(figsize=(u_const.PLOT_WIDTH_IN, 6.5))

    gs = mgridspec.GridSpec(3, 2, wspace=0.04, hspace=0.25, height_ratios=[2, 2, 4])
    axs = [[fig.add_subplot(gs[i, 0]) for i in range(3)]]
    axs.append([fig.add_subplot(gs[i, 1], sharey=axs[0][i]) for i in range(3)])

    for i_ssp, ssp in enumerate(["SSP2", "SSP5"]):
        data = pd.read_csv(u_const.SOURCEDATA_DIR / f"figs04{'ab'[i_ssp]}.csv")
        plot_aff_projection(axs[i_ssp][0], data)

        ax = axs[i_ssp][1]
        data = pd.read_csv(u_const.SOURCEDATA_DIR / f"figs04{'cd'[i_ssp]}.csv")
        plot_aff_regression(ax, data)
        if i_ssp == 0:
            ax.legend(
                loc="lower right",
                ncol=1,
                frameon=False,
                handletextpad=0.4,
            )

        ax = axs[i_ssp][2]
        data = pd.read_csv(u_const.SOURCEDATA_DIR / f"figs04{'ef'[i_ssp]}.csv")
        plot_gdppc_projection(ax, data)
        if i_ssp == 0:
            ax.legend(
                bbox_to_anchor=(0.0, 0.92),
                loc="upper left",
                ncol=1,
                frameon=False,
            )

    ax = axs[0][0]
    ax.legend(bbox_to_anchor=(1.0, 1.0), loc="lower center", ncol=4, frameon=False)

    for ax, letter in zip(sum(axs, []), "acebdf"):
        ax.text(0.02, 0.98 if letter in "ef" else 0.97, letter, ha="left", va="top",
                fontweight="bold", fontsize=10, transform=ax.transAxes)

    for ax in axs[1]:
        plt.setp(ax.get_yticklabels(), visible=False)
        ax.set_ylabel("")

    outpath = u_const.PLOTS_DIR / f"FigureS04.pdf"
    print(f"Writing to {outpath} ...")
    fig.savefig(outpath, bbox_inches="tight")


if __name__ == "__main__":
    main()
