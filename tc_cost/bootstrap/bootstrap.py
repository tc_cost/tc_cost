
import re
import sys

import numpy as np
import pandas as pd
import rpy2.robjects
import rpy2.robjects.packages
import rpy2.robjects.pandas2ri
import xarray as xr

import tc_cost.util.constants as u_const
import tc_cost.util.data as u_data

rpy2.robjects.pandas2ri.activate()
r_meboot = rpy2.robjects.packages.importr("meboot")


def get_aff_df(minyear, maxyear, ds=None):
    if ds is None:
        ds = u_data.load_tce_dat_annual_shares()
    ds = ds.sel(year=(ds.year >= minyear) & (ds.year <= maxyear))
    return ds.to_dataframe().reset_index()


def get_gdppc_df(minyear, maxyear, ds=None):
    if ds is None:
        ds = u_data.load_hist_gdppc()
    ds = ds.sel(year=(ds.year >= minyear) & (ds.year <= maxyear))
    return ds.to_dataframe().reset_index()


def get_temp_df(minyear, maxyear, ds=None):
    if ds is None:
        ds = u_data.load_hist_temp()
    ds = ds.sel(year=(ds.year >= minyear) & (ds.year <= maxyear))
    return ds.to_dataframe().reset_index()


def df2ds(df, col, elt, new_data=None):
    df = df.copy()
    if new_data is not None:
        df[col] = new_data
    ds = df.set_index(["year", "ISO"])[[col]].to_xarray()
    ds['elt'] = elt
    return ds


def main():
    base_dir = u_const.BOOTSTRAP_DIR / "bootstrapped"
    period_args = [re.match(r"^([0-9]{4})-([0-9]{4})$", a) for a in sys.argv[1:]]
    period_args = [(int(m.group(1)), int(m.group(2))) for m in period_args if m is not None]
    if len(period_args) > 0:
        period = period_args[0]
        max_timehorizon = min(20, period[0] - u_const.HIST_PERIOD_TC[0])
        base_dir /= f"{period[0]}-{period[1]}"
    else:
        period = u_const.HIST_PERIOD_REF
        max_timehorizon = u_const.MAX_TIMEHORIZON
    base_dir.mkdir(parents=True, exist_ok=True)

    country2numeric = pd.read_csv(u_const.ISO_NUMERIC_CSV)

    bhm_df = u_data.load_bhm_data()[["gdppc_ch", "temp"]].to_dataframe().reset_index()
    bhm_period = (bhm_df["year"].min(), bhm_df["year"].max())
    bhm_df = (
        bhm_df[
            (bhm_df["year"] >= period[0])
            & (bhm_df["year"] <= period[1])
        ]
        .dropna(subset=["gdppc_ch", "temp"], how="any")
        .reset_index(drop=True)
        .rename(columns={"gdppc_ch": "gdppc_ch_bhm", "temp": "temp_bhm"})
    )

    input_dfs = {
        "aff": get_aff_df(period[0] - max_timehorizon, period[1]),
        "gdppc": get_gdppc_df(*period),
        "temp": get_temp_df(*period),
        "bhm": bhm_df,
    }

    for k, df in input_dfs.items():
        # meboot v1.4-9 accepts only numerical values as index,
        # so we replace ISO by the numerical ID of countries
        input_dfs[k] = df.merge(country2numeric, on="ISO", how="inner")

    input_data = [
        (input_dfs['aff'], "affected"),
        (input_dfs['gdppc'], "gdppc"),
        (input_dfs['gdppc'], "gdppc_ch"),
        (input_dfs['temp'], "temp"),
    ]

    if bhm_period[0] <= period[0] and period[1] <= bhm_period[1]:
        input_data.extend([
            (input_dfs["bhm"], "gdppc_ch_bhm"),
            (input_dfs["bhm"], "temp_bhm"),
        ])

    for df, col in input_data:
        path = base_dir / f"{col}.nc"

        if path.exists():
            continue

        col_results = [df2ds(df, col, "original")]
        for sample in range(u_const.BOOT_NUMBER):
            for rep in range(u_const.BOOT_REPLICATIONS):
                r_df = r_meboot.meboot_pdata_frame(
                    df[["year", "ID", col]].copy(),
                    reps=u_const.BOOT_SIZE, colsubj=2, coldata=3,
                )

                # convert the R-DataFrame to xarray-Datasets, one for each column:
                col_results.extend([
                    df2ds(df, col, f"B{sample}/{rep}/{i}", new_data=np.asarray(r_df.rx2(str(k))))
                    for i, k in enumerate(r_df.names)
                ])
        ds = xr.combine_nested(col_results, "elt")

        print(f"Writing to {path} ...")
        encoding = {v: {'zlib': True} for v in ds.data_vars}
        ds.to_netcdf(path, encoding=encoding)


if __name__ == "__main__":
    main()
