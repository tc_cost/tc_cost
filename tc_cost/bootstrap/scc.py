
import itertools
import os
import sys

import numpy as np
import pandas as pd
import pycountry
import xarray as xr

import tc_cost.util.constants as u_const
import tc_cost.util.data as u_data
import tc_cost.util.io as u_io


OUTPUT_PATH = u_const.BOOTSTRAP_DIR / "scc"
OUTPUT_PATH.mkdir(parents=True, exist_ok=True)

CMIP5_DIR = u_const.RICKE_DATA_DIR / "cmip5" / "RegionalSCC_rcpfits"
PULSE_DIR = u_const.RICKE_DATA_DIR / "pulse" / "RegionalSCC_pulseuncertainty"
BURKE_DIR = (
    u_const.RICKE_DATA_DIR / "BurkeHsiangMiguel2015_Replication" / "data" / "output" / "bootstrap"
)


def _ricke_add_and_delete_countries(df):
    excluded_countries = ["Netherlands Antilles"]
    df = df[~df["country"].isin(excluded_countries)].reset_index(drop=True)

    add_countries = []
    for c1, c2 in [
        ("Afghanistan", "Pakistan"),
        ("Sudan", "Chad"),
        ("Serbia", "Bosnia and Herzegovina"),
        ("Western Sahara", "Mauritania"),

        # the following replacements are not used in Ricke et al. 2018:
        ("Barbados", "Trinidad and Tobago"),
        ("Saint Lucia", "Trinidad and Tobago"),
        ("Puerto Rico", "Haiti"),
        ("Tonga", "Samoa"),
        ("Hong Kong", "Viet Nam"),
        ("Taiwan, Province of China", "Viet Nam"),
    ]:
        if not (df["country"] == c1).any():
            row = df[df["country"] == c2].copy()
            row["country"] = c1
            add_countries.append(row)

    return pd.concat([df] + add_countries).reset_index(drop=True)


def _ricke_lookup_country(c):
    c = c.split("(")[0].strip(" ")
    for n1, n2 in [
        ("Céte d'Ivoire", "Côte d'Ivoire"),
        ("Democratic Republic of the Congo", "Congo, The Democratic Republic of the"),
        ("Falkland Islands", "Falkland Islands (Malvinas)"),
        ("Iran", "Iran, Islamic Republic of"),
        ("Micronesia", "Micronesia, Federated States of"),
        ("Occupied Palestinian Territory", "Palestine, State of"),
        ("Republic of Korea", "Korea, Republic of"),
        ("R_union", "Réunion"),
        ("Swaziland", "Eswatini"),
        ("The former Yugoslav Republic of Macedonia", "Republic of North Macedonia"),
        ("Wallis and Futuna Islands", "Wallis and Futuna"),
    ]:
        if c == n1:
            c = n2
            break
    try:
        return pycountry.countries.lookup(c).alpha_3
    except LookupError:
        return pycountry.historic_countries.lookup(c).alpha_3


def load_poptemp():
    """Load the country-specific population-weighted temperature along climate scenarios

    The data is from the code repository of Ricke et al. 2018:
    https://github.com/country-level-scc/cscc-paper-2018

    Returns
    -------
    temp : xr.Dataset
        For the years 2006-2105, for each country, for each RCP, and for each GCM, the
        population-weighted annual mean temperature.
    """
    cache_path = u_const.CACHE_DIR / "poptemp_allmodels.nc"
    if cache_path.exists():
        return xr.open_dataset(cache_path)

    result = []
    for rcp in u_const.L_RCP_LOWER:
        for gcm in u_const.L_GCM:
            path = CMIP5_DIR / (
                # there is no rcp26 data, use rcp45 data instead
                f"popweightcountry_fromfit_{gcm}_{rcp.replace('rcp26', 'rcp45')}.csv"
            )
            df = pd.read_csv(
                path,
                header=None,
                names=["fao", "country"] + list(range(2006, 2106)),
                encoding="windows-1252",
            )

            # remove bad lines in rcp45 data (1.0, 3.0, 7.0, ..., 9901.0)
            df = df[df[2006] != 1.0].copy()

            df = _ricke_add_and_delete_countries(df)
            df["ISO"] = df["country"].apply(_ricke_lookup_country)
            df = df.drop(columns=["fao", "country"])

            df = df.melt(id_vars=["ISO"], var_name="year", value_name="poptemp")
            df["gcm"] = gcm
            df["rcp"] = rcp

            result.append(df)
    da = (
        pd.concat(result)
        .set_index(["gcm", "rcp", "ISO", "year"])
        .to_xarray()["poptemp"]
    )
    ds = (
        da + (u_data.load_hist_temp()["temp"] - da).mean(dim="year")
    ).to_dataset(name="poptemp")
    ds.to_netcdf(cache_path)
    return ds


def load_poptemp_pulse():
    """Load the country-specific population-weighted temperature change of additional emissions

    The data is from the code repository of Ricke et al. 2018:
    https://github.com/country-level-scc/cscc-paper-2018

    Note that the result is "per gigaton of carbon", instead of "per ton of CO2".

    Returns
    -------
    temp : xr.Dataset
        For up to 100 years, for each country, for each GCM, and for each of 15 carbon-cycle
        models, the change in population-weighted temperature (in K) per Gt of carbon emitted.
    """
    cache_path = u_const.CACHE_DIR / "poptemp_pulse_allmodels.nc"
    if cache_path.exists():
        return xr.open_dataset(cache_path)

    result = []
    for gcm in u_const.L_GCM:
        path = PULSE_DIR / f"popweightcountry_fromfit_{gcm}abrupt4xco2_allCmodels.csv"
        df = pd.read_csv(path).rename(columns={
            "FAO": "fao",
            "Country": "country",
            "C Model": "ccm",
        })
        df = _ricke_add_and_delete_countries(df)
        df["ISO"] = df["country"].apply(_ricke_lookup_country)
        df = df.drop(columns=["fao", "country"])

        df = df.melt(id_vars=["ccm", "ISO"], var_name="year", value_name="poptemp_response")
        df["year"] = df["year"].astype(float) - 0.5
        df["gcm"] = gcm

        result.append(df)
    ds = (
        pd.concat(result)
        .set_index(["gcm", "ccm", "ISO", "year"])
        .to_xarray()
    )

    # replace countries that are missing from some GCMs by the multi-model mean:
    ds = ds.fillna(ds.mean(dim="gcm"))

    # convert from mK to K
    ds["poptemp_response"] /= 1000

    ds.to_netcdf(cache_path)
    return ds


def load_temp_dmgfunc(timehorizon, use_ols):
    """Load the coefficients of our own panel model's temperature damage function

    Parameters
    ----------
    timehorizon : int
        Number of lag years.
    use_ols : bool
        If True, take the results from the OLS regression, otherwise from the robust GLM.

    Returns
    -------
    dmgfunc : xr.Dataset
        temp and temp2 are the coefficients for (population-weighted) temperature T and T**2.
        There is one set of coefficients for each bootstrap run.
    """
    suffix = '_ols' if use_ols else ''
    path = u_const.BOOTSTRAP_DIR / "estimates" / f"coeffs_{timehorizon}{suffix}_temp.nc"
    return (
        xr.open_dataset(path)["Estimate"]
        .sel(coeff=["temp", "temp2"])
        .to_dataset(dim="coeff")
    )


def load_bhm_dmgfunc():
    """Load the coefficients of the Burke et al. 2015 temperature damage function

    The data is from the code repository of Ricke et al. 2018:
    https://github.com/country-level-scc/cscc-paper-2018

    Returns
    -------
    dmgfunc : xr.Dataset
        temp and temp2 are the coefficients for (population-weighted) temperature T and T**2.
    """
    return (
        pd.read_csv(BURKE_DIR / "bootstrap_noLag.csv")
        .set_index("run")[["temp", "temp2"]]
        .to_xarray()
        # run ID 0 is the default one used in the Ricke scripts
        .sel(run=0)
    )


def load_gmt_pulse():
    """Load the global temperature change (in K) from additional emissions (in GtC)

    The data is from Ricke & Caldeira 2014 (see Figure 1).

    Note that the result is "per gigaton of carbon", instead of "per ton of CO2".

    Returns
    -------
    temp : xr.Dataset
        For unevenly spaced years (up to 100) and for each GCM and each of 15 carbon-cycle models,
        the change in global mean temperature (in K) per Gt of carbon emitted.
    """
    cache_path = u_const.CACHE_DIR / "temp_pulse_allmodels.nc"
    if cache_path.exists():
        return xr.open_dataset(cache_path)

    df = pd.read_csv(u_const.PULSE_RESPONSE_CSV)
    df = df.melt(
        id_vars=df.columns[:3].tolist(),
        value_vars=df.columns[3:].tolist(),
        var_name="year",
        value_name="temp_response",
    )
    df["year"] = df["year"].astype(float)
    ds = (
        df
        .set_index(df.columns[:4].tolist())
        .to_xarray()
        .rename({"CO2": "ccm", "T4X": "cmodel", "OCN": "ocmodel"})
    )
    ds["cmodel"] = u_const.PULSE_RESPONSE_CMIP5_MODELS
    ds["ocmodel"] = u_const.PULSE_RESPONSE_CMIP5_MODELS
    ds["ccm"] = [
        ccm.replace(" ", "") for ccm in
        u_const.PULSE_RESPONSE_CARB_CYCLE_MODELS
    ]

    # HadGEM2-ES is missing, we replace it by the multi-model median
    ds_hadgem2es = (
        ds
        .median(dim=["cmodel", "ocmodel"])
        .expand_dims(dim={"gcm": ["HadGEM2-ES"]})
    )

    # only consider combinations of T4X and OCN from the same GCM
    remaining_gcms = [gcm for gcm in u_const.L_GCM if gcm != "HadGEM2-ES"]
    ds = (
        ds
        .stack(gcm=["cmodel", "ocmodel"])
        .sel(gcm=[(gcm, gcm) for gcm in remaining_gcms])
        .drop_vars(['gcm'])
    )
    ds["gcm"] = remaining_gcms
    ds = xr.concat([ds, ds_hadgem2es], dim="gcm")
    ds.to_netcdf(cache_path)
    return ds


def compute_scc(
    ds_soc, ds_damfun_tc, ds_gmt, ds_gmt_pulse,
    ds_damfun_temp, ds_poptemp, ds_poptemp_pulse, discrates
):
    """Compute the social cost of carbon (SCC) using the provided discount rates

    Parameters
    ----------
    ds_soc : xarray Dataset with data variables "pop" and "gdppc", and dimension "year"
        Description of the socioeconomic development.
    ds_damfun_tc : xarray Dataset with data variables "const" and "gmt"
        Description of the (linear) temperature damage function. This dataset can come with
        additional dimensions, e.g. for probabilistic analyses.
    ds_gmt : xarray Dataset with data variable "gmt", and dimension "year"
        The GMT timeseries of the climate change scenario.
    ds_gmt_pulse : xarray Dataset with data variable "temp_response", and dimension "year"
        The temperature change from additional emissions.
    ds_damfun_temp : xarray Dataset with data variables "temp" and "temp2"
        Description of the (quadratic) population-weighted temperature damage function.
    ds_poptemp : xarray Dataset with data variable "poptemp"
        The population-weighted temperature time series of the climate change scenario.
    ds_poptemp_pulse : xarray Dataset with data variable "poptemp_response"
        The change in population-weighted temperature from additional emissions.
    discrates : numpy array of shape (nrates, 2)
        Each row is a pair rho/eta; eta=0 means constant discount rate.

    Returns
    -------
    ds : xarray Dataset with data variables "scc_temp", "scc", "rho", and "eta"
        The SCC is given in US$ per ton of CO2. While "scc_temp" is the SCC without TC effects
        and only based on the population-weighted temperature damage function (Burke et al.), the
        "scc" variables contains both the original Burke et al. and the new TC damage function.
        Furthermore, the data set has an additional dimension "ccm" which covers the 15
        carbon-cycle models that have been used in computing the temperature response to an
        emission pulse.
    """
    ds = ds_gmt.merge(ds_soc, join="left").merge(ds_poptemp, join="left")

    for v, ds_pulse, p in [("gmt", ds_gmt_pulse, ""), ("poptemp", ds_poptemp_pulse, "pop")]:
        dyears = ds['year'].values - u_const.PULSE_YEAR
        da_pulse = ds_pulse[f"{p}temp_response"].interp(year=dyears, method="cubic")
        da_pulse["year"].values[:] = ds["year"].values
        ds[f'{v}+pulse'] = ds[v] + da_pulse

    for s in ["", "+pulse"]:
        ds[f"temp_delta{s}"] = (
            ds_damfun_temp["temp"] * ds[f"poptemp{s}"]
            + ds_damfun_temp["temp2"] * ds[f"poptemp{s}"]**2
        ) - (
            ds_damfun_temp["temp"] * ds[f"poptemp{s}"].isel(year=0)
            + ds_damfun_temp["temp2"] * ds[f"poptemp{s}"].isel(year=0)**2
        )
        ds[f'tc_delta{s}'] = ds_damfun_tc['const'] + ds_damfun_tc['gmt'] * ds[f'gmt{s}']
        ds[f"gdppc_temp_cc{s}"] = ds['gdppc'] * np.exp(ds[f"temp_delta{s}"].cumsum(dim="year"))
        ds[f"gdppc_cc{s}"] = ds[f"gdppc_temp_cc{s}"] * np.exp(ds[f"tc_delta{s}"].cumsum(dim="year"))

    for s in ["", "_temp"]:
        # compute annual SCC according to Anthoff and Tol equation A3 in Appendix
        # the pulse is initially computed per GtCO2, but we want per tCO2,
        # hence divide by PULSE_SCALE
        ds[f'y_scc{s}'] = (
            -(ds[f'gdppc{s}_cc+pulse'] - ds[f'gdppc{s}_cc']) * ds['pop'] / u_const.PULSE_SCALE
        )

    # extrapolation of SCC to 2200 (before discounting)
    ds = ds.reindex(
        year=np.arange(u_const.PULSE_YEAR, u_const.VERY_LAST_YEAR + 1)
    ).ffill(dim="year")

    # discount SCC according to Anthoff and Tol equation A3 in Appendix
    ds['rho'] = ('dr', discrates[:, 0])
    ds['eta'] = ('dr', discrates[:, 1])

    for s in ["", "_temp"]:
        # attenuate negative growth rates after 2100
        ds[f'gdppc_ch{s}_cc'] = ds['gdppc_ch'] + ds['temp_delta'] + (
            ds['tc_delta'] if s == "" else 0
        )
        neg_msk = ((ds[f'gdppc_ch{s}_cc'] < 0) & (ds['year'] > 2100)).values
        ds[f'gdppc_ch{s}_cc'].values[neg_msk] = 0

        ds[f'dfac{s}'] = np.exp(-ds['rho'] - ds['eta'] * ds[f'gdppc_ch{s}_cc']).cumprod(dim="year")
        ds[f'scc{s}'] = (ds[f'dfac{s}'] * ds[f'y_scc{s}']).sum(dim="year")

    return ds[['scc_temp', 'scc', 'rho', 'eta']]


def main():
    try:
        task = int(os.environ["SLURM_ARRAY_TASK_ID"])
    except:
        task = int(sys.argv[-1])

    use_ols = "ols" in sys.argv
    use_temp = "temp" in sys.argv
    suffix = f"{'_ols' if use_ols else ''}{'_temp' if use_temp else ''}"

    # appx. ~40 seconds per parameter set, 177 sets (~120 minutes) per job
    timehorizon, ssp, rcp, gcm = list(itertools.product(
        # 384 configurations
        np.arange(0, u_const.MAX_TIMEHORIZON + 1),
        u_const.L_SSP,
        u_const.L_RCP_LOWER,
        u_const.L_GCM,
    ))[task]

    tc_countries = u_const.L_COUNTRIES
    ds_ssp = u_data.load_ssp_data().sel(SSP=ssp)
    ds_gmt = (
        xr.open_dataset(u_const.GMT_NC)
        .reindex(year=np.arange(u_const.PULSE_YEAR, 2101))
        .sel(gcm=gcm, rcp=rcp)
    )
    ds_gmt_pulse = load_gmt_pulse().sel(gcm=gcm)
    ds_damfun_temp = load_temp_dmgfunc(timehorizon, use_ols) if use_temp else load_bhm_dmgfunc()
    ds_poptemp = load_poptemp().sel(rcp=rcp, gcm=gcm)
    ds_poptemp_pulse = load_poptemp_pulse().sel(gcm=gcm)

    # the inner merge removes some very small countries like Nauru from our analysis
    l_countries = (
        ds_ssp[["ISO"]]
        .merge(ds_poptemp[["ISO"]], join="inner")
        .merge(ds_poptemp_pulse[["ISO"]], join="inner")
        ["ISO"].values
    )
    assert np.isin(tc_countries, l_countries).all()

    # each row is a pair rho/eta; eta=0 means constant discount rate
    discrates = np.array(
        sum([list(l_dr.values()) for l_dr in u_const.L_DR.values()], [])
    )

    for iso in l_countries:
        path = OUTPUT_PATH / f"{ssp}-{timehorizon}-{gcm}-{rcp}-{iso}{suffix}.nc"

        if u_io.path_exists_safely(path):
            continue

        print(f"{ssp} {rcp} {gcm} timehorizon={timehorizon} country={iso}")
        ds_ssp_sub = ds_ssp.sel(ISO=iso)
        ds_poptemp_sub = ds_poptemp.sel(ISO=iso)
        ds_poptemp_pulse_sub = ds_poptemp_pulse.sel(ISO=iso)

        gcm_damfun = "allgcms"  # alternative, use GCM-specific damage function estimate: gcm
        ds_damfun_tc = xr.open_dataset(
            u_const.BOOTSTRAP_DIR / "damage_functions" /
            f"{timehorizon}-{gcm_damfun}-{iso if iso in tc_countries else 'USA'}{suffix}.nc"
        ).sel(rcp="__all__")
        ds_damfun_tc = ds_damfun_tc['estimates'].to_dataset(dim="coeff")
        if iso not in tc_countries:
            # set TC effect to 0 outside the 41 TC-affected countries
            ds_damfun_tc["const"].values[:] = 0
            ds_damfun_tc["gmt"].values[:] = 0

        ds_scc = compute_scc(
            ds_ssp_sub,
            ds_damfun_tc, ds_gmt, ds_gmt_pulse,
            ds_damfun_temp, ds_poptemp_sub, ds_poptemp_pulse_sub,
            discrates,
        )
        ds_scc.compute()
        u_io.write_safely(ds_scc, path)


if __name__ == "__main__":
    main()
