
import cartopy.crs as ccrs
import cartopy.io.shapereader as shpreader
import matplotlib.colors as mcolors
import matplotlib.gridspec as mgridspec
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import geopandas as gpd
import numpy as np
import pandas as pd

import tc_cost.util.constants as u_const
import tc_cost.util.plot as u_plot


def plot_dad_timeseries(gs, df):
    fig = plt.gcf()
    outer = mgridspec.GridSpecFromSubplotSpec(
        2, 1, subplot_spec=gs, hspace=0, height_ratios=[1, 30])
    inner = mgridspec.GridSpecFromSubplotSpec(
        4, 1, subplot_spec=outer[1], wspace=0.12, hspace=0.15)

    axes = [fig.add_subplot(inner[0])]
    axes += [fig.add_subplot(inner[1], sharex=axes[0])]
    axes += [
        fig.add_subplot(inner[2], sharex=axes[0], sharey=axes[0]),
        fig.add_subplot(inner[3], sharex=axes[0], sharey=axes[1]),
    ]

    l_dr = {k: v for k, v in u_const.L_DR['ramsey'].items() if k != "Nordhaus"}

    for gs_i, ssp in enumerate(u_const.L_SSP):
        for gs_j, (dr_name, dr) in enumerate(l_dr.items()):
            ax = axes[2 * gs_i + gs_j]
            for i_rcp, rcp in enumerate(u_const.L_RCP_LOWER):
                rcp_cl = u_const.L_RCP_COLORS[rcp]
                df_sel = df[
                    (df["SSP"] == ssp)
                    & (df["rcp"] == rcp)
                    & (df["rho"] == dr[0])
                    & (df["eta"] == dr[1])
                ].copy()
                ax.plot(
                    df_sel["year"],
                    df_sel["dec50"],
                    c=rcp_cl,
                    label=f"RCP{float(rcp[3:]) / 10:.1f}",
                )
                ax.fill_between(
                    df_sel["year"],
                    df_sel["dec17"],
                    df_sel["dec83"],
                    facecolor=rcp_cl,
                    alpha=0.2,
                    edgecolor="none",
                )
            ax.axhline(0, color="black", linestyle=":", linewidth=0.75)
            ax.text(
                0.05,
                0.95,
                f"{ssp}, {dr_name}'s discounting choice",
                ha="left", va="top",
                transform=ax.transAxes,
            )

            if gs_i != 1 or gs_j != 1:
                plt.setp(ax.get_xticklabels(), visible=False)
            handles_right, labels_right = ax.get_legend_handles_labels()

    ax = fig.add_subplot(inner[:, :], frameon=False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_xlabel("Years", labelpad=15)
    ax.set_ylabel("Global DAD relative to 2021 gross world product (%)", labelpad=22)

    ax = fig.add_subplot(outer[0], frameon=False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.legend(
        handles_right,
        labels_right,
        loc="lower center",
        ncol=3,
        frameon=False,
    )


def plot_dadpc_maps(gs, df):
    # compare DADpc maps

    # world map from geopandas
    world = gpd.read_file(shpreader.natural_earth(
        resolution='110m', category='cultural', name='admin_0_countries',
    ))
    world = world[world["CONTINENT"] != "Antarctica"].copy()  # Exclude Antarctica
    world = world.rename(columns={"ISO_A3": "ISO"})

    # Common colorbar for both maps
    vmin = max(1, df["decpc50"].min())
    vmax = df["decpc50"].max()
    vmax_10 = 10**np.floor(np.log10(vmax) - 1)
    vmax = vmax_10 * np.ceil(vmax / vmax_10)
    norm = mcolors.LogNorm(vmin=vmin, vmax=vmax)
    cmap = plt.get_cmap("autumn_r")

    fig = plt.gcf()
    gs_outer = mgridspec.GridSpecFromSubplotSpec(
        2, 1, subplot_spec=gs, height_ratios=[30, 1], hspace=0.04)

    gs_inner = mgridspec.GridSpecFromSubplotSpec(
        2, 1, subplot_spec=gs_outer[0], hspace=0.02)

    # shift the antimeridian to focus on the most exposed areas
    proj = ccrs.PlateCarree(central_longitude=185)

    axes = [
        fig.add_subplot(gs_inner[0], projection=proj),
        fig.add_subplot(gs_inner[1], projection=proj),
        fig.add_subplot(gs_outer[1])
    ]

    l_rcp = np.unique(df["rcp"].values)

    data_crs = ccrs.PlateCarree()
    for i, (ax, rcp) in enumerate(zip(axes, l_rcp)):
        df_rcp = df[df["rcp"] == rcp]
        ssp = df_rcp["SSP"].values[0]
        world_new = world.merge(df_rcp, on="ISO", how="left")
        invalid_mask = world_new["decpc50"].isna()
        ax.add_geometries(
            world_new.geometry[invalid_mask],
            color="#aaaaaa", linewidth=0,
            crs=data_crs, zorder=10)
        for idx, row in world_new[~invalid_mask].iterrows():
            ax.add_geometries(
                [row.geometry],
                color=cmap(norm(row["decpc50"])),
                linewidth=0, crs=data_crs, zorder=10)
        ax.text(0.5, 1.03, f"RCP{float(rcp[-2:]) / 10:.1f}-{ssp}",
                ha="center", va="bottom", transform=ax.transAxes)

        ax.set_xticks([])
        ax.set_yticks([])
        ax.set_xlim(-160, 130)
        ax.set_ylim(-45, 80)

        u_plot.map_add_gridlines(
            ax, [90., 180., -90.], [0.],
            (25, 315) + ax.get_extent()[2:],
            data_crs,
        )

    ax = axes[0]
    ax.text(0.5, 1.15, "Ricke's discounting choice",
            # f"$\\rho$={100 * dr[0]:.1f}%, $\\eta$={dr[1]}",
            ha="center", va="bottom", fontsize=8, transform=ax.transAxes)

    cb = plt.colorbar(
        plt.cm.ScalarMappable(cmap=cmap),
        cax=axes[2], orientation="horizontal")

    cx = [v for v in mticker.LogLocator(10).tick_values(vmin, vmax) if vmin <= v <= vmax]
    cb.set_ticks(norm(cx))
    cb.set_ticklabels([f"{n:.0f}" for n in cx])
    cb.ax.minorticks_on()
    cx = mticker.LogLocator(10, subs=range(0, 10, 2)).tick_values(vmin, vmax)
    cx = cx[(cx > vmin) & (cx < vmax)]
    cb.ax.xaxis.set_ticks(norm(cx), minor=True)

    cb.set_label("Average DADpc (US$)")


def main():
    u_plot.main_setup_mpl()

    fig = plt.figure(figsize=(u_const.PLOT_WIDTH_IN, 4))
    outer = mgridspec.GridSpec(1, 2, width_ratios=[1, 1.1], wspace=0.1)

    data = pd.read_csv(u_const.SOURCEDATA_DIR / "figs12a.csv")
    plot_dad_timeseries(outer[0], data)
    ax = fig.add_subplot(outer[0], frameon=False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.text(-0.05, 1.015, "a", ha="right", va="bottom", fontweight="bold", fontsize=10,
            transform=ax.transAxes)

    data = pd.read_csv(u_const.SOURCEDATA_DIR / "figs12b.csv")
    plot_dadpc_maps(outer[1], data)
    ax = fig.add_subplot(outer[1], frameon=False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.text(-0.01, 1.015, "b", ha="left", va="bottom", fontweight="bold", fontsize=10,
            transform=ax.transAxes)

    path = u_const.PLOTS_DIR / f"FigureS12.pdf"
    print(f"Writing to {path} ...")
    fig.savefig(path, bbox_inches="tight")


if __name__ == "__main__":
    main()
