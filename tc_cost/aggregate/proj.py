
import itertools
import os
import sys

import numpy as np
import xarray as xr

import tc_cost.bootstrap.df as bs_df
import tc_cost.util.constants as u_const
import tc_cost.util.io as u_io


def aggregate_growth_losses(timehorizon, iso, use_ols, use_temp):
    base_path = u_const.BOOTSTRAP_DIR / "growth_losses"

    suffix = f"{'_ols' if use_ols else ''}{'_temp' if use_temp else ''}"
    ds = bs_df.load_loss_data(timehorizon, iso, suffix).compute()
    ds['gmt'] = xr.open_dataset(u_const.GMT_NC)['gmt']

    cov_data = []
    mean_data = []
    for rcp in ds["rcp"].values:
        ds_sel = ds.sel(rcp=rcp)
        gmt_x = (
            ds_sel["gmt"]
            .broadcast_like(ds_sel["gdppc_growth_cost"])
            .values.ravel()
        )
        cost_y = ds_sel["gdppc_growth_cost"].values.ravel()
        cov_data.append(np.cov(gmt_x, cost_y))
        mean_data.append([np.mean(gmt_x), np.mean(cost_y)])
    ds["covs"] = (["rcp", "v", "w"], cov_data)
    ds["means"] = (["rcp", "v"], mean_data)
    ds["v"] = ("v", ["gmt", "cost"])
    ds["w"] = ("w", ["gmt", "cost"])

    qdims = ["SSP", "realisation", "elt"]
    qs = [0.05, 0.167, 0.5, 0.833, 0.95]
    for q in qs:
        ds[f"cost_{100 * q:.0f}"] = (
            ds["gdppc_growth_cost"]
            .quantile(q=q, dim=qdims)
        )
    rngs = [(5, 95), (17, 83)]
    for q1, q2 in rngs:
        ds[f"cost_rng_{q2 - q1}"] = ds[f"cost_{q2}"] - ds[f"cost_{q1}"]
        ds[f"cost_mid_{q2 - q1}"] = 0.5 * (ds[f"cost_{q1}"] + ds[f"cost_{q2}"])

    return ds[
        ["covs", "means"]
        + [f"cost_{100 * q:.0f}" for q in qs]
        + [f"cost_rng_{q2 - q1}" for q1, q2 in rngs]
        + [f"cost_mid_{q2 - q1}" for q1, q2 in rngs]
    ].compute()


def main():
    try:
        task = int(os.environ["SLURM_ARRAY_TASK_ID"])
    except:
        task = int(sys.argv[-1])

    timehorizon = 8
    use_ols = "ols" in sys.argv
    use_temp = "temp" in sys.argv
    suffix = f"{'_ols' if use_ols else ''}{'_temp' if use_temp else ''}"

    # 42 configurations
    iso = (["GLB"] + u_const.L_COUNTRIES)[task]

    nc_path = u_const.BOOTSTRAP_DIR / "growth_losses" / (
        f"aggregate_{timehorizon}-{iso}{suffix}.nc"
    )
    if u_io.path_exists_safely(nc_path):
        return

    ds = aggregate_growth_losses(timehorizon, iso, use_ols, use_temp)
    u_io.write_safely(ds, nc_path)


if __name__ == "__main__":
    main()
