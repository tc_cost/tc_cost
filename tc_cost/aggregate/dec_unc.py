
import itertools
import os
import sys

import numpy as np
import xarray as xr

import tc_cost.util.constants as u_const
import tc_cost.util.data as u_data
import tc_cost.util.io as u_io


def aggregate_gdad_uncertainty(timehorizon, ssp, rcp, dr_name, disc_growth, real, suffix):
    dr = u_const.L_DR['ramsey'][dr_name]

    l_countries = u_const.L_COUNTRIES

    ds = xr.open_mfdataset([
        u_const.BOOTSTRAP_DIR / "dec" / f"{ssp}-{timehorizon}-{iso}_{disc_growth}{suffix}.nc"
        for iso in l_countries
    ], combine="nested", concat_dim=["ISO"])
    ds['ISO'] = ("ISO", l_countries)

    for v in ["rho", "eta"]:
        ds[v] = ds[v].sel(ISO=ds['ISO'].values[0])
    dr_indexer = ((ds.rho == dr[0]) & (ds.eta == dr[1])).compute()
    ds = ds.sel(realisation=real, rcp=rcp, dr=dr_indexer).squeeze()

    ds['pop'] = u_data.load_ssp_data().sel(SSP=ssp)['pop']

    ds["gdad"] = (ds["marginal_dec_pc"] * ds["pop"]).sum(dim="ISO")

    return ds[["gdad"]]


def main():
    try:
        task = int(os.environ["SLURM_ARRAY_TASK_ID"])
    except:
        task = int(sys.argv[-1])

    timehorizon = 8
    disc_growth = "baseline"
    dr_name = "Ricke"
    use_ols = "ols" in sys.argv
    use_temp = "temp" in sys.argv
    suffix = f"{'_ols' if use_ols else ''}{'_temp' if use_temp else ''}"

    ssp, rcp, real = list(itertools.product(
        # 600 configurations
        u_const.L_SSP,
        u_const.L_RCP_LOWER,
        np.arange(100),
    ))[task]

    path = u_const.BOOTSTRAP_DIR / "dec" / (
        f"aggregate_unc_{ssp}-{timehorizon}-{rcp}_{disc_growth}_{dr_name}-{real}{suffix}.nc"
    )
    if u_io.path_exists_safely(path):
        return

    ds = aggregate_gdad_uncertainty(timehorizon, ssp, rcp, dr_name, disc_growth, real, suffix)
    u_io.write_safely(ds, path)


if __name__ == "__main__":
    main()
