
import pandas as pd
import matplotlib.gridspec as mgridspec
import matplotlib.lines as mlines
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy as np
import xarray as xr

import tc_cost.util.constants as u_const
import tc_cost.util.plot as u_plot


def plot_gscc_dots(gs, df):
    SCC_dict = {ssp: df[df.SSP == ssp].copy() for ssp in ["SSP2", "SSP5"]}

    fig = plt.gcf()
    gs_outer = gs
    gs0 = mgridspec.GridSpecFromSubplotSpec(
        2, 1, subplot_spec=gs_outer, height_ratios=[1, 10], hspace=0.2)
    gs = mgridspec.GridSpecFromSubplotSpec(2, 1, subplot_spec=gs0[1])
    axes = [fig.add_subplot(gs[0])]
    axes += [fig.add_subplot(gs[1], sharex=axes[0], sharey=axes[0])]

    y_rcp = np.array([2, 1, 0])
    for i_ssp, (ax, ssp) in enumerate(zip(axes, u_const.L_SSP)):
        SCC_ssp = SCC_dict[ssp]
        for suffix in ["", "_temp"]:
            for i_dr, (dr_name, dr) in enumerate(u_const.L_DR['ramsey'].items()):
                dr_color = u_const.L_DR_COLORS[dr_name]
                SCC_ssp_dr = SCC_ssp[(SCC_ssp.rho == dr[0]) & (SCC_ssp.eta == dr[1])].copy()
                SCC_ssp_dr_50 = SCC_ssp_dr[f"scc_wld{suffix}50"].values
                SCC_ssp_dr_83 = SCC_ssp_dr[f"scc_wld{suffix}83"].values
                SCC_ssp_dr_17 = SCC_ssp_dr[f"scc_wld{suffix}17"].values
                SCC_ssp_dr_err = np.stack(
                    [SCC_ssp_dr_50 - SCC_ssp_dr_17, SCC_ssp_dr_83 - SCC_ssp_dr_50], axis=1)
                y = y_rcp + (
                    0.15 if i_dr == 1 else -0.15 if i_dr == 2 else 0.0
                ) + (1 if suffix == "" else -1) * 0.07
                ax.errorbar(
                    SCC_ssp_dr_50, y, xerr=SCC_ssp_dr_err.transpose(),
                    fmt="o", markersize=3,
                    linewidth=1,
                    markeredgecolor=dr_color,
                    markeredgewidth=1,
                    markerfacecolor=dr_color if suffix == "" else "white",
                    color=dr_color, label=dr_name if suffix == "" else None,
                )
        ax.text(0.995, 0.95, f"{ssp}", va="top", ha="right", transform=ax.transAxes)
        ax.set_yticks([0, 1, 2])
        ax.set_yticklabels([r[-3:] for r in u_const.L_RCP[::-1]])
        if i_ssp == 0:
            plt.setp(ax.get_xticklabels(), visible=False)
        else:
            ax.set_xlabel("Global SCC (US$/tCO2)")

    ax = fig.add_subplot(gs0[1], frameon=False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_ylabel("RCP", labelpad=20)

    ax = fig.add_subplot(gs0[0], frameon=False)
    ax.set_xticks([])
    ax.set_yticks([])
    handles, labels = axes[0].get_legend_handles_labels()
    handles = [
        mpatches.Patch(facecolor=h.lines[0].get_color(), edgecolor='none')
        for h in handles
    ] + [
        mlines.Line2D(
            [0], [0], marker='o', color="black", markeredgecolor="black",
            markeredgewidth=1, markersize=3, markerfacecolor=mfc,
        ) for mfc in ["black", "white"]
    ]
    labels = labels + ["w/ TCs", "w/o TCs"]
    leg_order = [0, 3, 1, 4, 2]
    handles = ['Discounting choice:', ''] + [handles[i] for i in leg_order]
    labels = ['                      ', ''] + [labels[i] for i in leg_order]
    ax.legend(
        handles,
        labels,
        loc="center",
        ncol=4,
        frameon=False,
        handler_map={str: u_plot.LegendTitle()},
    )


def plot_cscc_distribution(gs, df):
    # plot the distribution of TC-SCC for countries with the highest TC-SCC
    l_countries, inds = np.unique(df["ISO"].values, return_index=True)
    l_countries = l_countries[np.argsort(inds)]
    iso_name = pd.read_csv(u_const.ISO_NAMES_CSV)

    da_scc_hist = df.set_index(["ISO", "bins"])["scc_tc_hist"].to_xarray()
    bin_width = np.unique(da_scc_hist["bins"].diff(dim="bins").values)
    bin_width = bin_width[bin_width > 0][0]
    bin_centers = da_scc_hist["bins"] + 0.5 * bin_width
    da_scc_means = (da_scc_hist * bin_centers).sum(dim="bins") * bin_width
    da_hist_plot = da_scc_hist.sel(bins=(da_scc_hist > 0.05 * da_scc_hist.max()).any(dim="ISO"))

    plot_bins = da_hist_plot["bins"].values
    plot_bins = np.concatenate([plot_bins, [2 * plot_bins[-1] - plot_bins[-2]]])

    fig = plt.gcf()
    rows, columns = 1, len(l_countries)
    outer = mgridspec.GridSpecFromSubplotSpec(
        2, 1, subplot_spec=gs, height_ratios=[4, 1], hspace=0.4,
    )
    inner = mgridspec.GridSpecFromSubplotSpec(rows, columns, subplot_spec=outer[0], wspace=0.12)

    # sharex, sharey
    axes = [fig.add_subplot(inner[0, 0])]
    axes += [
        fig.add_subplot(inner[0, i], sharex=axes[0], sharey=axes[0])
        for i in range(1, columns)
    ]

    dr_name = df["dr_name"].values[0]
    dr_color = u_const.L_DR_COLORS[dr_name]

    for ax, iso in zip(axes, l_countries):
        name_ctry = iso_name.loc[iso_name.iso3 == iso, "name"].values[0]
        ax.set_title(f"{name_ctry}")

        # histogram (main plot part)
        hist = da_hist_plot.sel(ISO=iso).values
        ax.stairs(hist, plot_bins, color=dr_color, fill=True, zorder=1)

        # overlay by mean, median and CI
        threshs = (da_scc_hist + 1e-10).sel(ISO=iso).cumsum(dim="bins").values * bin_width
        qvalues = xr.DataArray(
            data=bin_centers.values,
            coords={"q": threshs},
            name="values"
        ).interp(coords={"q": [0.167, 0.5, 0.833]}, method="linear", assume_sorted=True)
        ax.axvline(
            qvalues.sel(q=0.5).item(),
            linewidth=0.5, color="black", zorder=100, label="Median",
        )
        ax.axvline(
            da_scc_means.sel(ISO=iso).item(),
            linewidth=0.5, linestyle=":", color="black", zorder=100, label="Mean",
        )
        ax.axvspan(
            qvalues.sel(q=0.167).item(), qvalues.sel(q=0.833).item(),
            facecolor="black", edgecolor="none", alpha=0.3, zorder=10, label="66% CI",
        )


    for ax in axes[1:]:
        plt.setp(ax.get_yticklabels(), visible=False)

    ax = axes[0]
    ax.set_ylabel("Probability density")
    xlim = ax.get_xlim()
    ax.set_xlim(min(0, xlim[0]), xlim[1])
    ax.set_xticks([0, 5, 10, 15])

    ax = fig.add_subplot(outer[0], frameon=False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_xlabel(f"Country-level TC-SCC (US$/tCO2)", labelpad=18)

    ax = fig.add_subplot(outer[1], frameon=False)
    ax.set_xticks([])
    ax.set_yticks([])
    handles, labels = axes[-1].get_legend_handles_labels()
    ax.legend(
        handles,
        labels,
        ncol=3,
        loc="lower right",
        bbox_to_anchor=(1.0, 0.0),
        frameon=False,
        borderpad=0,
        borderaxespad=0,
        columnspacing=1.3,
        labelspacing=0,
        handletextpad=0.6,
        handlelength=1.5,
    )


def plot_cscc_dots(gs, df):
    def_dr_name = "Ricke"
    def_dr = u_const.L_DR['ramsey'][def_dr_name]
    def_dr_color = u_const.L_DR_COLORS[def_dr_name]
    iso_name = pd.read_csv(u_const.ISO_NAMES_CSV).set_index("iso3")["name"].to_dict()
    l_countries = df["ISO"].values
    l_ctry_names = [iso_name[i] for i in l_countries]

    fig = plt.gcf()
    inner = mgridspec.GridSpecFromSubplotSpec(
        1, 2, wspace=0, width_ratios=[1, 25], subplot_spec=gs)
    ax = fig.add_subplot(inner[1])

    y_countries = np.arange(len(l_countries))[::-1]
    for suffix in ["", "_temp"]:
        df_50 = df[f"scc{suffix}50"].values
        df_83 = df[f"scc{suffix}83"].values
        df_17 = df[f"scc{suffix}17"].values
        df_err = np.stack([df_50 - df_17, df_83 - df_50], axis=1)
        y = y_countries + (1 if suffix == "" else -1) * 0.12
        ax.errorbar(
            df_50, y, xerr=df_err.transpose(),
            fmt="o", markersize=3, linewidth=1,
            markeredgewidth=1,
            markeredgecolor=def_dr_color,
            markerfacecolor=def_dr_color if suffix == "" else "white",
            color=def_dr_color,
        )
    for y in y_countries:
        ax.axhspan(
            y - 0.5, y + 0.5,
            color="#f0f0f0",
            alpha=0.7 if y % 2 == 0 else 0.3,
            linewidth=0,
        )
    ax.axvline(0, lw=0.5, ls=":", c="silver")
    ax.set_ylim(min(y_countries) - 0.5, max(y_countries) + 0.5)
    ax.set_yticks(y_countries[::-1])
    ax.set_yticklabels(l_ctry_names[::-1])
    ax.set_xlabel("Country-level SCC (US$/tCO2)")
    xlim = ax.get_xlim()
    ax.set_xlim(min(0, xlim[0]), xlim[1])


def main():
    u_plot.main_setup_mpl()
    fig = plt.figure(figsize=(u_const.PLOT_WIDTH_IN, 7.0))
    outer = mgridspec.GridSpec(5, 1, hspace=0.1, height_ratios=[8, 2.0, 8, 2.5, 6])

    data = pd.read_csv(u_const.SOURCEDATA_DIR / "fig4a.csv")
    plot_gscc_dots(outer[0], data)

    ax = fig.add_subplot(outer[0], frameon=False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.text(-0.055, 0.97, "a", va="bottom", ha="right", fontweight="bold", fontsize=10,
            transform=ax.transAxes)

    data = pd.read_csv(u_const.SOURCEDATA_DIR / "fig4b.csv")
    plot_cscc_dots(outer[2], data)

    ax = fig.add_subplot(outer[2], frameon=False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.text(-0.055, 1.03, "b", va="bottom", ha="right", fontweight="bold", fontsize=10,
            transform=ax.transAxes)

    data = pd.read_csv(u_const.SOURCEDATA_DIR / "fig4c.csv")
    plot_cscc_distribution(outer[4], data)

    ax = fig.add_subplot(outer[4], frameon=False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.text(-0.055, 1.15, "c", va="bottom", ha="right", fontweight="bold", fontsize=10,
            transform=ax.transAxes)

    path = u_const.PLOTS_DIR / f"Figure4.pdf"
    print(f"Writing to {path} ...")
    fig.savefig(path, bbox_inches="tight")



if __name__ == "__main__":
    main()
