
import numpy as np
import pandas as pd
import xarray as xr

import tc_cost.util.constants as u_const
import tc_cost.util.data as u_data


def get_data(timehorizon, glb, nobootstrap, average_annual_aff, use_ols, use_temp):
    print(f"Reading {'global' if glb else 'country'}-level data for {timehorizon} lags ...")

    beta_ds = u_data.read_betas(
        timehorizon,
        subdir="bootstrap" if glb else None,
        group_type="glb" if glb else None,
        nobootstrap=nobootstrap,
        use_ols=use_ols,
        use_temp=use_temp,
    ).rename({"elt": "boot"})

    if average_annual_aff:
        beta_ds['coeff'] = beta_ds['coeff'] * u_data.load_avg_aff(timehorizon)['avg_aff']

    if "ISO" not in beta_ds.dims:
        beta_ds['coeff'] = xr.broadcast(beta_ds['coeff'], beta_ds['ISO'])[0]

    return beta_ds.sel(ISO=u_const.L_COUNTRIES)


def get_data_cum(max_timehorizon, glb, nobootstrap, average_annual_aff, use_ols, use_temp):
    # There is no explicit conversion to % units happening here, but the conversion is
    # implicit in the summing operation, which is equal to multiplying by 1 (100%) so that the
    # result is the effect in % units.
    suffix = f"{'_ols' if use_ols else ''}{'_temp' if use_temp else ''}"
    fname = (
        f"cum_{'glb' if glb else 'ctry'}{'_nb' if nobootstrap else ''}"
        f"{'_avgaff' if average_annual_aff else ''}"
        f"{suffix}-{max_timehorizon}.nc"
    )
    cache_path = u_const.CACHE_DIR / "impacts" / fname
    if cache_path.exists():
        print(f"Reading from {cache_path} ...")
        beta_ds = xr.open_dataset(cache_path)
    else:
        beta_ds = xr.combine_nested([
            get_data(L, glb, nobootstrap, average_annual_aff, use_ols, use_temp)
            for L in range(max_timehorizon + 1)
        ], "persistence")
        print(f"Aggregating {'global' if glb else 'country'}-level data ...")
        beta_ds = beta_ds.sum(dim="aff")
        print(f"Writing to {cache_path} ...")
        encoding = {v: {'zlib': True} for v in beta_ds.data_vars}
        beta_ds.to_netcdf(cache_path, encoding=encoding)
    return beta_ds


def get_data_qs(timehorizon, glb, nobootstrap, average_annual_aff, use_ols, use_temp):
    # The data is converted to % units (multiplied by 100)!
    suffix = f"{'_ols' if use_ols else ''}{'_temp' if use_temp else ''}"
    fname = (
        f"qs_{'glb' if glb else 'ctry'}{'_nb' if nobootstrap else ''}"
        f"{'_avgaff' if average_annual_aff else ''}"
        f"{suffix}-{timehorizon}.nc"
    )
    cache_path = u_const.CACHE_DIR / "impacts" / fname
    if cache_path.exists():
        print(f"Reading from {cache_path} ...")
        beta_ds_q = xr.open_dataset(cache_path)
    else:
        beta_ds = (
            get_data(timehorizon, glb, nobootstrap, average_annual_aff, use_ols, use_temp)
            .reindex(aff=np.arange(0, timehorizon + 1))
        )
        print(f"Aggregating {'global' if glb else 'country'}-level data in quantiles ...")

        # Alternatively, select only the main period for nobootstrap-runs
        # if nobootstrap:
        #     beta_ds = beta_ds.sel(boot=beta_ds["boot"].str.contains("1981-2015"))

        beta_ds['coeff_cum'] = beta_ds['coeff'].cumsum(dim="aff")

        suffices = ["", "_cum"]
        for s in suffices:
            beta_ds[f"coeff{s}"] = 100 * (np.exp(beta_ds[f"coeff{s}"]) - 1)

        beta_ds_q = xr.concat([
            beta_ds.quantile(dim="boot", q=[0.5, 0.167, 0.833, 0.05, 0.95]),
            beta_ds.quantile(dim=["ISO", "boot"], q=[0.5, 0.167, 0.833, 0.05, 0.95])
            .expand_dims(dim={"ISO": ["GLB"]}),
        ], "ISO")

        print(f"Writing to {cache_path} ...")
        encoding = {v: {'zlib': True} for v in beta_ds_q.data_vars}
        beta_ds_q.to_netcdf(cache_path, encoding=encoding)
    return beta_ds_q


def data_fig1a(beta_ctry):
    df = beta_ctry.to_dataframe().reset_index()
    df["affected"] = ~df["coeff"].isna()
    return df[["ISO", "affected"]]


def data_fig1b(timehorizon, glb, nobootstrap, use_ols, use_temp):
    beta_ctry_qs = get_data_qs(timehorizon, glb, nobootstrap, True, use_ols, use_temp)
    df = (
        beta_ctry_qs[["coeff_cum"]]
        .sel(quantile=[0.167, 0.5, 0.833])
        .sel(ISO=["GLB", "USA", "JPN", "PHL"])
        .to_dataframe()
        .reset_index()
        .rename(columns={"aff": "lag"})
        .set_index(["ISO", "lag", "quantile"])["coeff_cum"]
        .unstack()
        .rename(columns={0.167: "low", 0.5: "med", 0.833: "high"})
        .reset_index()
    )
    df.columns.name = None
    return df


def data_fig1c(beta_ctry):
    beta_ctry = beta_ctry.rename(persistence="lag").rename_vars(coeff="value")
    max_timehorizon = beta_ctry["lag"].values.max()
    bins = np.linspace(-0.100, 0.022, 50)
    hist_timehorizons = [2, 5, 8, 12, 15]

    medians = beta_ctry["value"].median(dim="boot").to_dataframe().reset_index()
    medians["variable"] = "median"

    hists = []
    for l in hist_timehorizons:
        hist = np.histogram(
            beta_ctry.sel(lag=l)["value"].values,
            bins=bins,
            density=True,
        )[0]
        hist = pd.DataFrame({"lag": l, "variable": bins[:-1], "value": hist})
        hist["variable"] = (
            "bin_"
            + hist.index.to_series().apply(lambda v: f"{v:03d}_")
            + hist["variable"].astype(str)
        )
        hists.append(hist)

    return pd.concat(hists + [medians]).sort_values(by=["lag", "variable"])


def main():
    timehorizon = 8
    glb = True
    nobootstrap = False
    use_ols = True
    use_temp = True
    average_annual_aff = False
    beta_ctry_cum = get_data_cum(15, glb, nobootstrap, average_annual_aff, use_ols, use_temp)

    path = u_const.SOURCEDATA_DIR / "fig1a.csv"
    df = data_fig1a(beta_ctry_cum.sel(persistence=timehorizon))
    print(f"Writing to {path} ...")
    df.to_csv(path, index=None)

    path = u_const.SOURCEDATA_DIR / "fig1b.csv"
    df = data_fig1b(timehorizon, glb, nobootstrap, use_ols, use_temp)
    print(f"Writing to {path} ...")
    df.to_csv(path, index=None)

    path = u_const.SOURCEDATA_DIR / "fig1c.csv"
    df = data_fig1c(beta_ctry_cum)
    print(f"Writing to {path} ...")
    df.to_csv(path, index=None)


if __name__ == "__main__":
    main()
