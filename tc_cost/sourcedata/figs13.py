
from tc_cost.sourcedata.fig2 import data_fig2c
import tc_cost.util.constants as u_const


def main():
    use_ols = True
    use_temp = True
    timehorizon = 8
    rcp = "rcp60"
    ssp = "SSP2"
    disc_growth = "baseline"

    panels = {"a": "Stern", "b": "Nordhaus"}
    for key, dr_name in panels.items():
        path = u_const.SOURCEDATA_DIR / f"figs13{key}.csv"
        df = data_fig2c(ssp, rcp, timehorizon, dr_name, disc_growth, use_ols, use_temp)
        print(f"Writing to {path} ...")
        df.to_csv(path, index=None)


if __name__ == "__main__":
    main()
