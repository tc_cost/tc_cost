#!/bin/bash

#SBATCH --qos=priority
#SBATCH --job-name=tcc_run_script
#SBATCH --account=ebm
#SBATCH --chdir=/home/tovogt/code/tc_cost/log
#SBATCH --output=run_script-%A_%a.out
#SBATCH --error=run_script-%A_%a.err
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16

module load intel/2019.0-nopython

export CONDA_HOME=/home/tovogt/.local/share/miniforge3
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

source $CONDA_HOME/etc/profile.d/conda.sh
conda activate tc_cost

cd /home/tovogt/code/tc_cost
echo $@ > log/run_script-${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}.args
SCRIPT_NAME=$1
shift
srun python -u -m "tc_cost.$SCRIPT_NAME" $@
