
import matplotlib.gridspec as mgridspec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import tc_cost.util.constants as u_const
import tc_cost.util.plot as u_plot


def main():
    u_plot.main_setup_mpl()

    fig = plt.figure(figsize=(u_const.PLOT_WIDTH_IN, 8))

    gs0 = mgridspec.GridSpec(2, 1, height_ratios=[5, 1], hspace=0.1)

    inner = mgridspec.GridSpecFromSubplotSpec(
        8, 2, subplot_spec=gs0[0], hspace=0, wspace=0)
    base_path = u_const.BOOTSTRAP_DIR / "estimates"

    axs = [fig.add_subplot(inner[0, 0])]
    axs += [fig.add_subplot(inner[i % 8, i // 8], sharex=axs[0], sharey=axs[0])
            for i in range(1, 16)]

    data = pd.read_csv(u_const.SOURCEDATA_DIR / "figs02.csv")
    l_timehorizons = np.unique(data["L"].values)

    for ax, timehorizon in zip(axs, l_timehorizons):
        df = data[data["L"] == timehorizon].sort_values(by=["l"], ascending=True)
        pvalues_bs = np.clip(df["p_value"].values, 0.01, 1.0)

        x = df["l"]
        ax.plot(x, pvalues_bs, c="black", linestyle="-")
        ax.axhline(0.05, c="grey", linestyle=":", linewidth=0.75)
        ax.text(0.5, 0.95, f"L={timehorizon}", ha="center", va="top", transform=ax.transAxes)

        ax.set_xlim(0, max(l_timehorizons))
        ax.set_xticks(x)
        ax.semilogy()
        ax.set_ylim(0.01, 1)

    for i, ax in enumerate(axs):
        if i == 7:
            plt.setp(ax.get_xticklabels()[-1:], visible=False)
        elif i != 15:
            plt.setp(ax.get_xticklabels(), visible=False)

        if i // 8 != 0 or i % 2 != 0:
            plt.setp(ax.get_yticklabels(), visible=False)

    ax = fig.add_subplot(gs0[0], frameon=False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_ylabel("p-value across 1,200 bootstraps", labelpad=25)

    ax = fig.add_subplot(gs0[1])
    for timehorizon in np.arange(1, 16):
        df = data[data["L"] == timehorizon].sort_values(by=["l"], ascending=True)
        ax.plot(df["l"], df["median"], c="k", linewidth=0.5)
        ax.fill_between(
            df["l"], df["17"], df["83"],
            alpha=0.1, facecolor="k",
        )
    ax.axhline(0.0, c="grey", linestyle=":", linewidth=0.5)
    ax.set_xlabel("Lag year")
    ax.set_ylabel("Cumulative impact on\ngrowth per 1% change in\nexposed people (%)")

    outpath = u_const.PLOTS_DIR / f"FigureS02.pdf"
    print(f"Writing to {outpath} ...")
    fig.savefig(outpath, bbox_inches="tight")


if __name__ == "__main__":
    main()
