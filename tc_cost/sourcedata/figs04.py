
import numpy as np
import pandas as pd
import statsmodels.formula.api as smf
import xarray as xr

import tc_cost.util.constants as u_const
import tc_cost.util.data as u_data


def read_aff_people_proj(ssp, iso, baseline=False):
    ds_aff = (
        u_data.load_ssp_data()
        .sel(SSP=ssp, ISO=[iso])
        .reindex(
            year=np.arange(u_const.SSP_PERIOD[0], u_const.SSP_PERIOD[1] + 1)
        ).squeeze()
    )

    l_rcp = u_const.L_RCP_LOWER
    if baseline:
        l_rcp = ["rcp26_baseline"] + l_rcp

    ds_aff['aff_share'] = xr.combine_nested([
        u_data.load_affected_people(
            rcp[:5], ssp, "baseline" if "baseline" in rcp else "full", shares=True,
        ).sel(ISO=iso)
        for rcp in l_rcp
    ], concat_dim="rcp")
    ds_aff['rcp'] = ("rcp", l_rcp)

    ds_aff['gmt'] = xr.open_dataset(u_const.GMT_NC)['gmt']

    return ds_aff


def aggregate_gdppc_projections(ssp, timehorizon, iso, use_ols, use_temp):
    suffix = f"{'_ols' if use_ols else ''}{'_temp' if use_temp else ''}"
    cache_path = u_const.CACHE_DIR / f"gdppc_pathway-{ssp}-{timehorizon}-{iso}{suffix}.csv"
    if cache_path.exists():
        print(f"Reading cached data from {cache_path} ...")
        return pd.read_csv(cache_path)

    base_path = u_const.BOOTSTRAP_DIR / "growth_losses"
    print(f"Reading raw data from {base_path} ...")
    ds_losses = xr.open_mfdataset([
        base_path / f"{ssp}-{timehorizon}-{rcp}-{iso}{suffix}.nc"
        for rcp in u_const.L_RCP_LOWER
    ], combine="nested", concat_dim=["rcp"])
    ds_losses['rcp'] = ("rcp", u_const.L_RCP_LOWER)

    q_dims = ["realisation", "elt", "gcm"]
    for sc in ["full", "baseline"]:
        for q in [0.167, 0.50, 0.833]:
            q_col = f'gdppc_{sc}_damage{100 * q:.0f}'
            ds_losses[q_col] = ds_losses[f'gdppc_{sc}_damage'].quantile(q=q, dim=q_dims)

    df = ds_losses[[
        f'gdppc_{sc}_damage{q}' for sc in ["full", "baseline"] for q in [17, 50, 83]
    ]].squeeze().to_dataframe().reset_index().drop(columns=["ISO", "SSP", "quantile"])

    print(f"Writing to {cache_path} ...")
    df.to_csv(cache_path, index=None)

    return df



def data_figs04ab(ssp, iso):
    ds = read_aff_people_proj(ssp, iso, baseline=True)
    for q in [0.167, 0.50, 0.833]:
        ds[f'aff_{100 * q:.0f}'] = ds['aff_share'].quantile(
            q=q, dim=["realisation", "gcm"],
        )
    df = (
        ds[[f"aff_{q}" for q in [17, 50, 83]]]
        .squeeze()
        .to_dataframe()
        .reset_index()
        .drop(columns=["quantile"])
        .rename(columns={"rcp": "scenario"})
    )
    df.loc[df["scenario"].str.endswith("baseline"), "scenario"] = "baseline"
    return df


def data_figs04cd(ssp, iso):
    ds = read_aff_people_proj(ssp, iso)
    ds['aff_avg'] = ds['aff_share'].mean(dim="realisation")
    ds = ds[["gmt", "aff_avg"]].squeeze()

    df = (
        ds.mean(dim="gcm").squeeze()
        .to_dataframe().reset_index()
        .drop(columns=["SSP", "ISO"])
    )

    res = smf.ols(formula="aff_avg ~ gmt", data=df).fit()
    res_pred = res.get_prediction()
    df["fit"] = res_pred.predicted_mean
    pred_ci = res_pred.conf_int(alpha=0.1)
    df["fit_min"] = pred_ci[:, 0]
    df["fit_max"] = pred_ci[:, 1]
    df = df.sort_values(by=["gmt"]).reset_index(drop=True)
    df["SSP"] = ssp

    return df


def data_figs04ef(timehorizon, use_ols, use_temp, ssp, iso):
    qs = ["50", "17", "83"]
    df = (
        aggregate_gdppc_projections(ssp, timehorizon, iso, use_ols, use_temp)
        .rename(columns={f"gdppc_full_damage{q}": f"gdppc{q}" for q in qs})
    )
    df["ISO"] = iso
    df["SSP"] = ssp

    bl_cols = ["year"] + [f"gdppc_baseline_damage{q}" for q in qs]
    df_bl = (
        df.loc[df["rcp"] == "rcp26", bl_cols].copy()
        .rename(columns={f"gdppc_baseline_damage{q}": f"gdppc{q}" for q in qs})
    )
    df_bl["rcp"] = "baseline"

    df_ssp = (
        u_data.load_ssp_data()["gdppc"]
        .sel(
            SSP=ssp, ISO=iso,
            year=np.arange(u_const.SSP_PERIOD[0], u_const.SSP_PERIOD[1] + 1)
        )
        .to_dataframe()
        .reset_index()
        .rename(columns={"gdppc": "gdppc50"})
    )
    df_ssp["rcp"] = "notc"

    df = df[["rcp", "ISO", "SSP", "year", "gdppc50", "gdppc17", "gdppc83"]]
    return pd.concat([df, df_bl, df_ssp]).rename(columns={"rcp": "scenario"})


def main():
    timehorizon = 8
    use_ols = True
    use_temp = True
    iso = "USA"

    for i_ssp, ssp in enumerate(u_const.L_SSP):
        path = u_const.SOURCEDATA_DIR / f"figs04{'ab'[i_ssp]}.csv"
        df = data_figs04ab(ssp, iso)
        print(f"Writing to {path} ...")
        df.to_csv(path, index=None)

        path = u_const.SOURCEDATA_DIR / f"figs04{'cd'[i_ssp]}.csv"
        df = data_figs04cd(ssp, iso)
        print(f"Writing to {path} ...")
        df.to_csv(path, index=None)

        path = u_const.SOURCEDATA_DIR / f"figs04{'ef'[i_ssp]}.csv"
        df = data_figs04ef(timehorizon, use_ols, use_temp, ssp, iso)
        print(f"Writing to {path} ...")
        df.to_csv(path, index=None)


if __name__ == "__main__":
    main()
