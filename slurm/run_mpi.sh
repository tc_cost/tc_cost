#!/bin/bash

#SBATCH --qos=short
#SBATCH --job-name=run_mpi
#SBATCH --account=ebm
#SBATCH --chdir=/home/tovogt/code/tc_cost/log
#SBATCH --output=run_mpi-%A_%a.out
#SBATCH --error=run_mpi-%A_%a.err
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=15
#SBATCH --cpus-per-task=1
#SBATCH --array=0-63

module load intel/2019.0-nopython

export CONDA_HOME=/home/tovogt/.local/share/miniforge3
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

source $CONDA_HOME/etc/profile.d/conda.sh
conda activate kriPy

export PATH=$CONDA_PREFIX/bin/libfabric:$PATH

unset I_MPI_DAPL_UD
unset I_MPI_DAPL_UD_PROVIDER

export I_MPI_FABRICS=shm:ofi
export I_MPI_PMI_LIBRARY=/p/system/slurm/lib/libpmi.so

cd /home/tovogt/code/tc_cost/
echo $@ > log/run_mpi-${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}.args
SCRIPT_NAME=$1
shift
srun -n $SLURM_NTASKS python -m "tc_cost.$SCRIPT_NAME" $@
