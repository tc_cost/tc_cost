
import os
import pickle
import re
import sys

import numpy as np
import pandas as pd
import statsmodels.formula.api as smf
import xarray as xr

import tc_cost.util.constants as u_const
import tc_cost.util.io as u_io


# suppress all output from R, since robustbase is very verbose
import rpy2.rinterface_lib.callbacks
rpy2.rinterface_lib.callbacks.consolewrite_print = lambda *args: None
rpy2.rinterface_lib.callbacks.consolewrite_warnerror = lambda *args: None
rpy2.rinterface_lib.callbacks.showmessage = lambda *args: None
import rpy2.robjects
import rpy2.robjects.packages
import rpy2.robjects.pandas2ri


rpy2.robjects.pandas2ri.activate()
r_base = rpy2.robjects.packages.importr("base")
r_rbs = rpy2.robjects.packages.importr("robustbase")
r_stat = rpy2.robjects.packages.importr("stats")


OUTPUT_PATH = u_const.BOOTSTRAP_DIR / "estimates"
OUTPUT_PATH.mkdir(parents=True, exist_ok=True)


def regress(df_fit, lags, glb, use_ols):
    fitrd = df_fit.copy()
    lags = lags if "aff0" in df_fit else -1
    formula = compose_formula(use_ols, lags, glb, use_temp="temp" in fitrd.columns)

    if use_ols:
        mod = smf.ols(formula=formula, data=fitrd)
        try:
            mod = mod.fit(method="pinv")
        except np.linalg.LinAlgError:
            mod = mod.fit(method="qr")
        fitrd["predicted"] = mod.fittedvalues

        summary = pd.DataFrame({
            'Estimate': mod.params,
            'pvalues': mod.pvalues,
            'tvalues': mod.tvalues,
            'rsquared': mod.rsquared,
            'exog_mean': pd.DataFrame(mod.model.exog, columns=mod.model.exog_names).mean(),
        })
        summary.index = rename_sm2r_params(summary.index)
    else:
        mod = r_rbs.glmrob(formula, data=fitrd, family="gaussian", maxit=10000)
        fitrd["predicted"] = r_stat.predict(mod)

        r_coeffs = r_base.summary(mod).rx2("coefficients")
        summary = pd.DataFrame(
            data=np.asarray(r_coeffs),
            index=np.asarray(r_coeffs.names.rx(1)).ravel(),
            columns=np.asarray(r_coeffs.names.rx(2)).ravel(),
        )
    summary.index.name = "coeff"

    ds = summary.to_xarray()
    ds['predicted'] = fitrd.set_index(["year", "ISO"])['predicted'].to_xarray()
    return ds, mod


def compose_formula(sm, lags, glb, use_temp=False):
    aff_cols = []
    if lags >= 0:
        aff_cols = [f"aff{l}" for l in range(lags + 1)]
    C = "C" if sm else "factor"
    return (
        # GDPpc growth
        "gdppc_ch ~ "
        + " + ".join(
            # interaction between share of people exposed and the country-specific effect:
            (aff_cols if glb else [f"{col}:{C}(ISO)" for col in aff_cols])
            # population-weighted temperature
            + (["temp", "temp2"] if use_temp else [])
            # three-way fixed effects:
            + [f"{C}(year)", f"{C}(ISO)", f"{C}(ISO):year", f"{C}(ISO):np.power(year, 2)"]
        )
    )


def rename_sm2r_params(ser):
    ser = ser.str.replace("C(", "factor(", regex=False)
    ser = ser.str.replace(r"\[(T\.)?(.*)\]", r"\2", regex=True)
    ser = ser.str.replace(r"^Intercept$", "(Intercept)", regex=True)
    return ser


def rename_r2sm_params(ser):
    ser = ser.str.replace("factor(", "C(", regex=False)
    ser = ser.str.replace(r"\)([^\)\(\[\]:]+)", r"[T.\1]", regex=True)
    ser = ser.str.replace(r"^(Intercept)$", "Intercept", regex=True)
    return ser


def add_aff_years_to_ds(ds_aff, timehorizon, period):
    return (
        ds_aff
        .rolling(year=timehorizon + 1)
        .construct(year="aff")
        .isel(aff=slice(None, None, -1))
        .sel(year=np.arange(period[0], period[1] + 1))
    )


def fitdf_from_ds(ds_growth, elt, ds_aff=None, ds_temp=None):
    all_dfs = []

    if ds_aff is not None:
        # one column for each lag
        df = ds_aff.sel(elt=elt).to_dataframe().drop(columns='elt').reset_index()
        df['aff'] = "aff" + df['aff'].astype(str)
        df = df.pivot(index=["year", "ISO"], columns="aff", values="affected")
        all_dfs.append(df.reset_index())

    all_dfs.extend([
        ds.sel(elt=elt).to_dataframe().drop(columns='elt').reset_index()
        for ds in [ds_growth] + ([] if ds_temp is None else [ds_temp])
    ])

    df = all_dfs[0]
    for _df in all_dfs[1:]:
        df = df.merge(_df, on=["year", "ISO"], how="outer")

    if ds_aff is not None:
        # non-TC-affected countries get 0 affected people for all lag years
        aff_cols = [c for c in df.columns if c.startswith("aff")]
        df[aff_cols] = df[aff_cols].fillna(0)

    if ds_temp is None:
        # regression only across the 41 TC-affected countries
        df = df[df["ISO"].isin(u_const.L_COUNTRIES)].reset_index(drop=True)
    else:
        df = df.dropna(subset=["temp"])
        df["temp2"] = df["temp"]**2

    # start counting at year 1
    df["year"] -= df["year"].values.min() - 1

    return df


def run_regressions(ds_growth, timehorizon, period, glb, use_ols=False, ds_aff=None, ds_temp=None):
    if ds_aff is not None:
        ds_aff = add_aff_years_to_ds(ds_aff, timehorizon, period)

    original_ols_res = None

    results = []
    for elt in ds_growth['elt'].values:
        df_fit = fitdf_from_ds(ds_growth, elt, ds_aff=ds_aff, ds_temp=ds_temp)
        ds, res_obj = regress(df_fit, timehorizon, glb, use_ols)

        if use_ols and elt == "original":
            original_ols_res = res_obj

        ds['elt'] = elt
        results.append(ds)
    ds = xr.combine_nested(results, "elt")

    if use_ols:
        return ds, original_ols_res
    else:
        return ds


def main():
    try:
        task = int(os.environ["SLURM_ARRAY_TASK_ID"])
    except:
        task = int(sys.argv[-1])

    use_ols = "ols" in sys.argv
    use_temp = "temp" in sys.argv
    use_notc = "notc" in sys.argv
    suffix = (
        f"{'_ols' if use_ols else ''}"
        f"{'_temp' if use_temp else ''}"
        f"{'_notc' if use_notc else ''}"
    )

    input_dir = u_const.BOOTSTRAP_DIR / "bootstrapped"
    period_args = [re.match(r"^([0-9]{4})-([0-9]{4})$", a) for a in sys.argv[1:]]
    period_args = [(int(m.group(1)), int(m.group(2))) for m in period_args if m is not None]
    if len(period_args) > 0:
        period = period_args[0]
        max_timehorizon = min(20, period[0] - u_const.HIST_PERIOD_TC[0])
        input_dir /= f"{period[0]}-{period[1]}"
        suffix = f"{suffix}_{period[0]}-{period[1]}"
    else:
        period = u_const.HIST_PERIOD_REF
        max_timehorizon = u_const.MAX_TIMEHORIZON

    # ~10 minutes per parameter set, 1 set per job
    # 16 configurations
    timehorizon = np.arange(0, max_timehorizon + 1)[task]

    ds_aff = None if use_notc else (
        xr.open_dataset(input_dir / "affected.nc")
        .sel(ISO=u_const.L_COUNTRIES)
    )

    input_suffixes = [""]
    if (input_dir / "gdppc_ch_bhm.nc").exists():
        input_suffixes.append("_bhm")

    for in_suffix in input_suffixes:
        ds_growth = xr.open_dataset(input_dir / f"gdppc_ch{in_suffix}.nc")
        ds_temp = xr.open_dataset(input_dir / f"temp{in_suffix}.nc") if use_temp else None

        if in_suffix != "":
            ds_growth = ds_growth.rename_vars({f"gdppc_ch{in_suffix}": "gdppc_ch"})
            ds_temp = ds_temp.rename_vars({f"temp{in_suffix}": "temp"})

        path = OUTPUT_PATH / f"coeffs_{timehorizon}{suffix}{in_suffix}.nc"
        if u_io.path_exists_safely(path):
            continue

        ds = run_regressions(
            ds_growth, timehorizon, period, True,
            use_ols=use_ols, ds_aff=ds_aff, ds_temp=ds_temp,
        )

        if use_ols:
            # discard the OLS result object
            ds = ds[0]

        u_io.write_safely(ds, path)


if __name__ == "__main__":
    main()
