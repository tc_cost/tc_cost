
import itertools
import os
import sys

import numpy as np
import pandas as pd
from statsmodels.sandbox.regression.onewaygls import OneWayLS
import xarray as xr

from tc_cost.bootstrap.df import load_loss_data, prepare_fit_data
import tc_cost.util.constants as u_const
import tc_cost.util.data as u_data
import tc_cost.util.io as u_io


OUTPUT_PATH = u_const.BOOTSTRAP_DIR / "damage_functions"
OUTPUT_PATH.mkdir(parents=True, exist_ok=True)


def test_breaks(endog, exog, groups):
    """F-tests for structural breaks in each of the regressor variables

    Parameters
    ----------
    endog : Series
        Observations of the endogenous response variable (dependent variable).
    exog : DataFrame
        Columns are the regressor variables, rows are the observations.
    groups : Series
        Assigns each of the observations to a group.

    Returns
    -------
    DataFrame
        Columns "variable", "groups", "f_pvalue" containing the name of the
        tested regressor variable, the combination of groups, and the corresponding
        F-test's p-value.
    """
    owls = OneWayLS(endog, exog, groups)
    _, summarytable = owls.ftest_summary()

    nvars = exog.shape[1]
    nparams = owls.contrasts['all'].shape[1]
    ngroups = owls.unique.size

    results = []
    for group, fres in summarytable:
        group_str = "__all__" if group == "all" else "_".join(group)
        results.append(pd.DataFrame({
            "variable": ["__all__"],
            "groups": group_str,
            "f_pvalue": fres[1],
        }))
        for iv, v in enumerate(exog.columns):
            contrasts = owls.contrasts[group].copy()
            if group == "all":
                contrasts = contrasts[[ig * nvars + iv for ig in range(ngroups - 1)], :]
            else:
                contrasts = contrasts[[iv], :]
            fres = owls.lsjoint.f_test(contrasts)
            results.append(pd.DataFrame({
                "variable": [v],
                "groups": group_str,
                "f_pvalue": fres.pvalue,
            }))
    return pd.concat(results)


def run_tests(ds_gmt, timehorizon, gcm, iso, use_ols, use_temp):
    suffix = f"{'_ols' if use_ols else ''}{'_temp' if use_temp else ''}"
    nc_path = OUTPUT_PATH / f"ftest_{timehorizon}-{gcm}-{iso}{suffix}.nc"
    if u_io.path_exists_safely(nc_path):
        return

    ds_losses = load_loss_data(timehorizon, iso, suffix).compute()
    ds_losses['gmt'] = ds_gmt['gmt']

    all_ftests = {"rcp": [], "SSP": []}
    for boot in ds_losses['elt'].values:
        boot_s = boot.replace('/', 'b')
        ftest_path = {
            key: OUTPUT_PATH / f"ftest_{key}-{timehorizon}-{gcm}-{iso}_{boot_s}{suffix}.csv"
            for key in all_ftests.keys()
        }
        if not all(u_io.path_exists_safely(p) for p in ftest_path.values()):
            boot_ftests = {k: [] for k in all_ftests.keys()}
            for real in ds_losses['realisation'].values:
                fit_df, endog, exog, groups = prepare_fit_data(ds_losses, gcm, boot, real)
                for key in all_ftests.keys():
                    df = test_breaks(endog, exog, fit_df[key]).rename(columns={
                        "groups": f"groups_{key}",
                        "f_pvalue": f"f_pvalue_{key}",
                    })
                    df["elt"] = boot
                    df["realisation"] = real
                    df["gcm"] = gcm
                    boot_ftests[key].append(df)
            for key, path in ftest_path.items():
                u_io.write_safely(pd.concat(boot_ftests[key]), path)
        for key, path in ftest_path.items():
            print(f"Reading from {path} ...")
            all_ftests[key].append(pd.read_csv(path))

    ds = xr.merge([
        pd.concat(dfs)
        .set_index(['realisation', 'elt', 'gcm', "variable", f"groups_{key}"])
        .to_xarray()
        for key, dfs in all_ftests.items()
    ])
    ds["realisation"] = ds["realisation"].astype(str)

    if ds.dims["realisation"] != 100:
        print("Skipped (realisation!=100):", nc_path, file=sys.stderr)
        return

    u_io.write_safely(
        ds, nc_path,
        unlink=list(OUTPUT_PATH.glob(f"ftest_*{timehorizon}-{gcm}-{iso}_*{suffix}.csv")),
    )


def main():
    try:
        task = int(os.environ["SLURM_ARRAY_TASK_ID"])
    except:
        task = int(sys.argv[-1])

    use_ols = "ols" in sys.argv
    use_temp = "temp" in sys.argv

    # to reduce number of jobs, split countries into 8 groups
    l_countries = np.array_split(u_const.L_COUNTRIES + ["GLB"], 8)

    # 4h per job
    timehorizon, gcm, countries = list(itertools.product(
        # 640 configurations
        np.arange(0, u_const.MAX_TIMEHORIZON + 1),
        u_const.L_GCM + ["allgcms"],
        l_countries,
    ))[task]

    ds_gmt = xr.open_dataset(u_const.GMT_NC)

    for iso in countries:
        print(f"{gcm} timehorizon={timehorizon} country={iso}")
        run_tests(ds_gmt, timehorizon, gcm, iso, use_ols, use_temp)



if __name__ == "__main__":
    main()
