
import re

import matplotlib.pyplot as plt
import pandas as pd
import xarray as xr

from tc_cost.util.pygsl_div import gsl_div
import tc_cost.util.constants as u_const
import tc_cost.util.data as u_data
import tc_cost.nobootstrap.fit as nb_fit

"""Aggregate across all periods of this length within this period"""
MAX_PERIOD = (1971, 2015)
YEARCOUNT = 35


def compute_div(predict_df, b):
    gsl_div_kwargs = dict(weights='add-progressive', b=b, L=6, min_per=1, max_per=99)
    result = predict_df.groupby("ISO").apply(
        lambda df: gsl_div(
            df['gdppc_ch'].values[None],
            df['predicted'].values[None],
            **gsl_div_kwargs,
        )
    )
    result.name = "gsl_div"
    return result.reset_index()


def plot_results(dfs, plot_ci=False):
    fig = plt.figure(figsize=(13, 7))
    ax = fig.add_subplot(1, 1, 1)
    colors = ['tab:blue', 'tab:orange']
    for i, (l, df) in enumerate(dfs.items()):
        color = colors[i]
        ax.plot(df['mean'], c=color, linestyle="--", label=f"{l} (mean)")
        ax.plot(df['median'], c=color, label=f"{l} (median)")
        print(f"{l}: argmin of median={df['median'].argmin()}, mean={df['mean'].argmin()}")
        if plot_ci:
            ax.fill_between(
                df.index, df['q17'], df['q83'], facecolor=color, alpha=0.3, edgecolor="none")
        if i == 0:
            ax.set_xticks(df.index)
    ax.set_xlabel("number of lags used")
    ax.set_ylabel("generalized subtracted L-divergence")
    ax.legend()

    path = u_const.PLOTS_DIR / "gsl_div.pdf"
    print(f"Writing to {path} ...")
    fig.savefig(path, bbox_inches="tight")


def main():
    ds_aff = u_data.load_tce_dat_annual_shares()
    ds_gdppc = u_data.load_hist_gdppc()

    all_divs = []
    all_paths = list((u_const.NOBOOTSTRAP_DIR / "coefficients").glob("*_glmrob.nc"))
    for i, coeffpath in enumerate(all_paths):
        m = re.match(r"(glb|ctry)_34-peop_([0-9]+)-([0-9]+)_([0-9]+)_glmrob.nc", coeffpath.name)
        if m is None:
            continue

        progress = f"{i + 1}/{len(all_paths)}"

        glb = m.group(1) == "glb"
        minyear = int(m.group(2))
        maxyear = int(m.group(3))
        timehorizon = int(m.group(4))

        if maxyear - minyear + 1 != YEARCOUNT:
            continue

        divpath = u_const.NOBOOTSTRAP_DIR / "gsl_div" / coeffpath.name.replace(".nc", ".csv")
        if divpath.exists():
            print(f"[{progress}] Reading cached GSL-div data from {divpath.name} ...", end="\r")
            divs = pd.read_csv(divpath)
        else:
            predict_ds = xr.open_dataset(coeffpath)[['predicted']]
            countries = predict_ds['ISO'].values
            ds_growth, _ = nb_fit.load_fit_data(
                minyear, maxyear, timehorizon, countries, ds_aff=ds_aff, ds_gdppc=ds_gdppc)
            ds_growth = ds_growth.assign_coords(
                year=ds_growth["year"] - ds_growth["year"].min() + 1)
            predict_ds['gdppc_ch'] = ds_growth['gdppc_ch']
            predict_df = predict_ds.to_dataframe().reset_index()
            for b in range(5, 9):
                divs = compute_div(predict_df, b)
                divs['gsl_div-b'] = b

            print(f"[{progress}] Writing GSL-div data to {divpath.name} ...", end="\r")
            divs.to_csv(divpath, index=None)

        divs['glb'] = m.group(1) == "glb"
        divs['minyear'] = minyear
        divs['maxyear'] = maxyear
        divs['timehorizon'] = timehorizon
        all_divs.append(divs)
    print("")

    df = pd.concat(all_divs).drop_duplicates().reset_index(drop=True)
    df["yearcount"] = df['maxyear'] - df['minyear'] + 1
    df = df[df['minyear'] >= MAX_PERIOD[0]]
    df = df[df['maxyear'] <= MAX_PERIOD[1]]
    df['agg_count'] = 1

    agg = df.groupby(by=["glb", "timehorizon", "yearcount"]).agg({
        "gsl_div": [
            lambda x: x.mean(),
            lambda x: x.quantile(q=0.5),
            lambda x: x.quantile(q=0.17),
            lambda x: x.quantile(q=0.83),
        ],
        "agg_count": "sum",
    })
    agg.columns = ["mean", "median", "q17", "q83", "agg_count"]
    agg = agg.reset_index()
    agg = agg[agg["yearcount"] == YEARCOUNT]
    drop_cols = ["yearcount", "glb"]

    plot_data = {
        'global': agg[agg['glb']].drop(columns=drop_cols).set_index("timehorizon"),
        # 'country-specific': agg[~agg['glb']].drop(columns=drop_cols).set_index("timehorizon"),
    }
    plot_results(plot_data, plot_ci=False)


if __name__ == "__main__":
    main()
