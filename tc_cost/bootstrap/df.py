
import itertools
import os
import sys
import warnings

import numpy as np
import pandas as pd
import statsmodels.api as sm
from statsmodels.tools.sm_exceptions import ConvergenceWarning
import xarray as xr

import tc_cost.util.constants as u_const
import tc_cost.util.data as u_data
import tc_cost.util.io as u_io


OUTPUT_PATH = u_const.BOOTSTRAP_DIR / "damage_functions"
OUTPUT_PATH.mkdir(parents=True, exist_ok=True)


def mixed_fit(endog, exog, groups):
    """Run a mixed-effects linear model on the given data

    Try several combinations of random/fixed effects assumptions. Take the first one that
    does not fail due to a singular covariance matrix. If all fail, fall back to an OLS with
    only fixed effects.

    Parameters
    ----------
    endog : Series
        Observations of the endogenous response variable (dependent variable).
    exog : DataFrame
        Columns are the regressor variables, rows are the observations.
        Currently, the column names "const" and "gmt" are hardcoded.
    groups : Series
        Assigns each of the observations to a group.

    Returns
    -------
    DataFrame
        Columns "coeff", "group", "estimates" containing the estimated coefficients for
        each fixed and random effect. The estimate over the total population is stored
        under the group name "__all__", and NaNs are inserted in the random effects that
        weren't active in the regression.
    """
    g_params = None
    for re_vars in [["const", "gmt"], ["gmt"], ["const"]]:
        try:
            exog_re = exog[re_vars]
            with warnings.catch_warnings():
                warnings.simplefilter("ignore", category=(
                    ConvergenceWarning,
                    UserWarning,
                    RuntimeWarning,
                ))
                # This ignores warnings about failing convergence and about singular covariance
                # matrices. In case of an Exception, we still react (see below).
                res = sm.MixedLM(endog, exog, groups, exog_re=exog_re).fit()
            g_params = {
                "__all__": res.fe_params,
                **res.random_effects
            }
        except (ValueError, np.linalg.LinAlgError):
            # "Cannot predict random effects from singular covariance structure."
            continue
        break

    if g_params is None:
        res = sm.OLS(endog, exog).fit()
        groups_uniq = np.unique(groups)
        nan_params = res.params.copy()
        nan_params.values[:] = np.nan
        g_params = {
            "__all__": res.params,
            **{g: nan_params for g in groups_uniq}
        }

    all_dfs = []
    for g, params in g_params.items():
        df = params.to_frame().reset_index()
        df.columns = ["coeff", "estimates"]
        df["group"] = g
        all_dfs.append(df)
    return pd.concat(all_dfs)


def aggregate_glb_loss_data(timehorizon, ssp, rcp, suffix):
    nc_path = u_const.BOOTSTRAP_DIR / "growth_losses" / (
        f"{ssp}-{timehorizon}-{rcp}-GLB{suffix}.nc"
    )
    if u_io.path_exists_safely(nc_path):
        return

    ds = xr.open_mfdataset([
        u_const.BOOTSTRAP_DIR / "growth_losses" / (
            f"{ssp}-{timehorizon}-{rcp}-{iso}{suffix}.nc"
        ) for iso in u_const.L_COUNTRIES
    ], combine="nested", concat_dim=["iso"], parallel=True)
    ds['iso'] = ("iso", u_const.L_COUNTRIES)
    if "gdppc_growth_cost" not in ds.variables:
        ds['gdppc_growth_cost'] = ds["full_growth_losses"] - ds["baseline_growth_losses"]
    ds = ds[['gdppc_growth_cost']].mean(dim="iso").compute()
    u_io.write_safely(ds, nc_path)


def load_loss_data(timehorizon, iso, suffix):
    if iso == "GLB":
        # the global loss data needs to be aggregated first since it is the multi-country average
        for ssp in u_const.L_SSP:
            for rcp in u_const.L_RCP_LOWER:
                aggregate_glb_loss_data(timehorizon, ssp, rcp, suffix)

    ds = xr.open_mfdataset([
        [
            u_const.BOOTSTRAP_DIR / "growth_losses" / (
                f"{ssp}-{timehorizon}-{rcp}-{iso}{suffix}.nc"
            ) for rcp in u_const.L_RCP_LOWER
        ] for ssp in u_const.L_SSP
    ], combine="nested", concat_dim=["SSP", "rcp"], parallel=True)
    ds['SSP'] = ("SSP", u_const.L_SSP)
    ds['rcp'] = ("rcp", u_const.L_RCP_LOWER)
    if "gdppc_growth_cost" not in ds.variables:
        ds['gdppc_growth_cost'] = ds["full_growth_losses"] - ds["baseline_growth_losses"]
    return ds[['gdppc_growth_cost']]


def prepare_fit_data(ds_losses, gcm, boot, real):
    # the fitting is across SSPs (2), RCPs (3), years (80), and, optionally, across GCMs (3)
    # => 480 (1440) values per damage function
    if gcm != "allgcms":
        ds_losses = ds_losses.sel(gcm=gcm)
    fit_df = (
        ds_losses
        .sel(elt=boot, realisation=real)
        .to_dataframe()
        .reset_index()[
            ["gdppc_growth_cost", "gmt", "SSP", "rcp"]
        ].copy()
    )
    endog = fit_df["gdppc_growth_cost"]
    exog = sm.add_constant(fit_df[["gmt"]])
    groups = fit_df["rcp"]
    return fit_df, endog, exog, groups


def compute_damage_functions(ds_gmt, timehorizon, gcm, iso, use_ols, use_temp):
    suffix = f"{'_ols' if use_ols else ''}{'_temp' if use_temp else ''}"
    nc_path = OUTPUT_PATH / f"{timehorizon}-{gcm}-{iso}{suffix}.nc"
    if u_io.path_exists_safely(nc_path):
        return

    ds_losses = load_loss_data(timehorizon, iso, suffix).compute()
    ds_losses['gmt'] = ds_gmt['gmt']

    all_fits = []
    for boot in ds_losses['elt'].values:
        boot_s = boot.replace('/', 'b')
        fit_path = OUTPUT_PATH / f"{timehorizon}-{gcm}-{iso}_{boot_s}{suffix}.csv"
        if not u_io.path_exists_safely(fit_path):
            boot_fits = []
            for real in ds_losses['realisation'].values:
                fit_df, endog, exog, groups = prepare_fit_data(ds_losses, gcm, boot, real)
                df = mixed_fit(endog, exog, groups).rename(columns={"group": "rcp"})
                df["elt"] = boot
                df["realisation"] = real
                df["gcm"] = gcm
                boot_fits.append(df)
            u_io.write_safely(pd.concat(boot_fits), fit_path)

        print(f"Reading from {fit_path} ...")
        all_fits.append(pd.read_csv(fit_path))

    ds = pd.concat(all_fits).set_index(['realisation', 'elt', 'gcm', 'coeff', 'rcp']).to_xarray()
    ds["realisation"] = ds["realisation"].astype(str)
    ds["estimates"] = ds["estimates"].fillna(0)

    if ds.dims["realisation"] != 100:
        print("Skipped (realisation!=100):", nc_path, file=sys.stderr)
        return

    u_io.write_safely(
        ds, nc_path,
        unlink=list(OUTPUT_PATH.glob(f"{timehorizon}-{gcm}-{iso}_*{suffix}.csv")),
    )


def main():
    try:
        task = int(os.environ["SLURM_ARRAY_TASK_ID"])
    except:
        task = int(sys.argv[-1])

    use_ols = "ols" in sys.argv
    use_temp = "temp" in sys.argv

    # 3h per job
    timehorizon, gcm, iso = list(itertools.product(
        # 3360 configurations
        np.arange(0, u_const.MAX_TIMEHORIZON + 1),
        u_const.L_GCM + ["allgcms"],
        u_const.L_COUNTRIES + ["GLB"],
    ))[task]

    ds_gmt = xr.open_dataset(u_const.GMT_NC)

    print(f"{gcm} timehorizon={timehorizon} country={iso}")
    compute_damage_functions(ds_gmt, timehorizon, gcm, iso, use_ols, use_temp)



if __name__ == "__main__":
    main()
