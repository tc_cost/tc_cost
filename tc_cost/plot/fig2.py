
import numpy as np
import matplotlib.colors as mcolors
import matplotlib.gridspec as mgridspec
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import pandas as pd

import tc_cost.util.constants as u_const
import tc_cost.util.data as u_data
import tc_cost.util.plot as u_plot


def plot_gdad_boxes(gs, df):
    fig = plt.gcf()
    outer = mgridspec.GridSpecFromSubplotSpec(
        2, 1, subplot_spec=gs, height_ratios=[1, 10], hspace=0.05)
    inner = mgridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec=outer[1])

    ax = fig.add_subplot(inner[0])

    l_dr = np.array(["Stern", "Nordhaus", "Ricke"])
    l_rcp = np.unique(df["rcp"].values)
    ssp = df["SSP"].values[0]
    ymax = 1.05 * (df["median"] + df["err_high"]).max()
    ymin = 1.2 * min(-200e9, (df["median"] - df["err_low"]).min())

    x = np.arange(l_rcp.size)
    n_boxes = l_dr.size
    box_width = 0.8 / n_boxes
    box_pad = box_width / 7
    box_width_w_pad = box_width + box_pad

    for i_dr, dr_name in enumerate(l_dr):
        df_dr = df[df["dr_name"] == dr_name]
        pos_dr = x + (i_dr - 0.5 * (n_boxes  - 1)) * box_width_w_pad
        for i_rcp, pos in enumerate(pos_dr):
            median = df_dr["median"].values[i_rcp]
            low, high = df_dr['err_low'].values[i_rcp], df_dr['err_high'].values[i_rcp]
            ax.add_patch(
                mpatches.Rectangle(
                    (pos - 0.5 * box_width, median - low),
                    box_width,
                    high + low,
                    fc=u_const.L_DR_COLORS[dr_name],
                    ec="black",
                    linewidth=0.2,
                )
            )
            ax.plot(
                [pos - 0.5 * box_width, pos + 0.5 * box_width],
                [median, median],
                linewidth=0.2,
                color="black",
            )

    labels = l_dr
    handles = [
        mpatches.Patch(facecolor=u_const.L_DR_COLORS[dr_name])
        for dr_name in l_dr
    ]
    ax.yaxis.set_major_formatter(lambda v, pos: f"{v / 1e12:.1f}")
    ax.set_ylim(ymin, ymax)
    ax.set_xticks(x)
    ax.set_xticklabels(u_const.L_RCP)
    ax.text(0.5, 0.96, f"{ssp}", va="top", ha="center", transform=ax.transAxes)
    ax.axhline(0, color="black", linestyle=":", linewidth=0.75)

    ax2 = ax.twinx()
    gwp_2021 = u_data.load_ssp_data().sel(year=2021)['gdp'].sum(dim="ISO")
    ax2.set_ylim(*[100 * y / gwp_2021.sel(SSP=ssp) for y in (ymin, ymax)])

    ax = fig.add_subplot(outer[0], frameon=False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_title("Discounting choice")
    ax.legend(handles, labels, loc="center", ncol=3, frameon=False)

    ax = fig.add_subplot(outer[1], frameon=False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_ylabel("Average DAD\n(Trillion US$)", labelpad=20)
    ax2 = ax.twinx()
    for side in ["left", "right", "top", "bottom"]:
        ax2.spines[side].set_visible(False)
    ax2.set_yticks([])
    ax2.set_ylabel("Average DAD\n(% of 2021 gwp)", labelpad=20)


def plot_cdadpc_to_income(ax, df, annotate_kwargs=None, ypad=0.1, med_fontsize=9):
    rcp = df["rcp"].values[0]
    ssp = df["SSP"].values[0]
    dr_name = df["dr_name"].values[0]

    df["income_ppp_log10"] = np.log10(df["income_ppp"])

    x = df["income_ppp_log10"].tolist()
    x_median = np.log10(df["income_ppp"].median())
    x_min = df["income_ppp_log10"].min()
    x_max = df["income_ppp_log10"].max()
    y = df["perc_of_ahi"].tolist()
    y_median = df["perc_of_ahi"].median()
    y_min = df["perc_of_ahi"].min()
    y_max = df["perc_of_ahi"].max()
    z = df["days_of_income"].tolist()
    ctry_name = df["ISO"].tolist()

    if dr_name == "Stern":
        ctry_pos = {
            "AUS": ((0, -3), "center", "top"),
            "CAN": ((0, 1), "center", "bottom"),
            "KHM": ((-1, -3), "right", "top"),
            "MOZ": ((-3, 0), "right", "top"),
            "MEX": ((0, 1), "center", "bottom"),
            "CUB": ((3, -2), "left", "bottom"),
            "CHN": ((0, -4), "left", "top"),
            "DOM": ((3, -1), "left", "top"),
            "LKA": ((-3, 0), "right", "bottom"),
            "THA": ((3, 0), "left", "top"),
            "GTM": ((-2, -1), "right", "top"),
            "LAO": ((-3, 0), "right", "center"),
            "PNG": ((0, -3), "center", "top"),
            "JAM": ((-3, 0), "right", "center"),
            "NIC": ((0, -3), "left", "top"),
            "HND": ((3, -2), "left", "center"),
            "HTI": ((-2, 0), "right", "top"),
            "BGD": ((3, 0), "left", "center"),
            "MMR": ((0, 3), "right", "bottom"),
        }
    else:
        ctry_pos = {
            "AUS": ((-1, -3), "center", "top"),
            "CAN": ((1, 3), "center", "bottom"),
            "KHM": ((-1, -3), "right", "top"),
            "MOZ": ((-3, 0), "right", "top"),
            "MEX": ((0, 1), "center", "bottom"),
            "CHN": ((2, -1), "left", "top"),
            "DOM": ((3, -1), "left", "top"),
            "LKA": ((0, 2), "center", "bottom"),
            "THA": ((0, -3), "center", "top"),
            "GTM": ((-1, -2), "left", "top"),
            "LAO": ((2, -2), "right", "top"),
            "PNG": ((0, -3), "center", "top"),
            "JAM": ((0, 2), "right", "bottom"),
            "NIC": ((-3, -2), "left", "top"),
            "HND": ((1, -1), "left", "top"),
            "HTI": ((-2, 0), "right", "center"),
            "BGD": ((2, 1), "left", "center"),
            "MMR": ((3, 1), "right", "bottom"),
            "BLZ": ((0, 0), "center", "bottom"),
        }

    ax2 = ax.twinx()

    ax.scatter(x, y, s=5, c='k')
    annotate_kwargs = {} if annotate_kwargs is None else annotate_kwargs
    for cx, cy, ctxt in zip(x, y, ctry_name):
        xytext, ha, va = ctry_pos.get(ctxt, ((3, 0), "left", "bottom"))
        ax.annotate(
            ctxt, (cx, cy), xytext=xytext, ha=ha, va=va, textcoords="offset points",
            **annotate_kwargs,
        )

    ax.set_xlim(*np.log10([3000, 100000]))
    ax.set_xticks(np.log10([5000, 10000, 50000, 100000]))
    ax.xaxis.set_major_formatter(lambda v, pos: f"{10**v / 1000:.0f}")
    ax.set_xticks(np.log10(np.concatenate([
        np.arange(3e3, 1e4, 1e3),
        np.arange(1e4, 1e5, 1e4),
    ])), minor=True)
    ax2.scatter(x, z, s=1, c='k')

    xmin, xmax = ax.get_xlim()
    xlen = xmax - xmin
    ax.set_xlim(xmin - 0.1 * xlen, xmax + 0.03 * xlen)
    for tmp_ax in [ax2, ax]:
        ymin, ymax = tmp_ax.get_ylim()
        ylen = ymax - ymin
        tmp_ax.set_ylim(ymin - ypad * ylen, ymax + ypad * ylen)

    ax.axhline(y_median, color="black", linestyle=":", linewidth=0.75)
    ax.text(ax.get_xlim()[1] - 0.005 * xlen, y_median - 0.005 * ylen, "Median impact",
            ha="right", va="top", fontsize=med_fontsize)
    ax.axvline(x_median, color="black", linestyle=":", linewidth=0.75)
    ax.text(x_median - 0.002 * xlen, ax.get_ylim()[1] - 0.01 * ylen, "Median income",
            rotation=90, ha="right", va="top", fontsize=med_fontsize)

    ax.set_xlabel("2019 average household income ($10^3$ US\$)")

    ax.set_ylabel("Average DAD per capita\n (% of 2019 average household income)")
    ax2.set_ylabel("Average DAD per capita\n (days of 2019 average household income)")

    title = f"RCP{float(rcp[-2:]) / 10:.1f}-{ssp}, {dr_name}'s discounting choice"
    ax.set_title(title)
    ax.set_facecolor(
        0.6 * np.ones(3)
        + 0.4 * np.array(mcolors.to_rgb(u_const.L_DR_COLORS[dr_name]))
    )

    trans = ax.transAxes
    for in_s, in_al, in_co in [("low", "left", 0.005), ("high", "right", 0.995)]:
        for im_s, im_al, im_co in [("low", "bottom", 0.01), ("high", "top", 0.99)]:
            ax.text(in_co, im_co, f"{in_s}-income/{im_s}-impact", ha=in_al, va=im_al,
                    transform=trans)


def plot_cdadpc_boxes(ax, df):
    rcp = df["rcp"].values[0]
    ssp = df["SSP"].values[0]
    dr_name = df["dr_name"].values[0]

    l_countries = df["ISO"].values
    dad50 = df["dad50"].values
    dad17 = df["dad17"].values
    dad83 = df["dad83"].values

    color = u_const.L_DR_COLORS[dr_name]

    box_width = 0.8
    for i_ctry, _ in enumerate(l_countries):
        pos = i_ctry - 0.5
        median = dad50[i_ctry]
        low, high = dad17[i_ctry], dad83[i_ctry]
        ax.add_patch(
            mpatches.Rectangle(
                (pos - 0.5 * box_width, low),
                box_width,
                high - low,
                fc=color,
                ec="black",
                linewidth=0.2,
            )
        )
        ax.plot(
            [pos - 0.5 * box_width, pos + 0.5 * box_width],
            [median, median],
            linewidth=0.2,
            color="black",
        )

    ax.set_xticks(np.arange(len(l_countries)))
    ax.set_xticklabels(l_countries, rotation=90, ha="center")
    ax.axhline(0, color="black", linestyle=":", linewidth=0.75)

    ymin = 1.2 * min(-500, dad17.min())
    ymax = 1.05 * dad83.max()
    ax.set_ylim(ymin, ymax)
    ax.yaxis.set_major_formatter(lambda v, pos: f"{v / 1:.0f}")
    ax.set_ylabel("Average DAD per capita (US\$)")
    ax.yaxis.set_label_position("right")
    ax.yaxis.tick_right()

    title = f"RCP{float(rcp[-2:]) / 10:.1f}-{ssp}, {dr_name}'s discounting choice"
    ax.set_title(title)


def plot_uncertainty_dimensions(gs, df):
    rcp = df["rcp"].values[0]
    ssp = df["SSP"].values[0]
    dr_name = df["dr_name"].values[0]

    fig = plt.gcf()

    inner = mgridspec.GridSpecFromSubplotSpec(1, 2, subplot_spec=gs, width_ratios=[1, 2.75])
    ax = fig.add_subplot(inner[1])

    y_offsets = {
        "year": -1,
        "realisation,gcm": 1,
        "elt": 3,
    }

    labels = {
        "year": f"DAD variation\nacross {u_const.SSP_PERIOD[0]}-{u_const.SSP_PERIOD[1]}",
        "realisation,gcm": "100 realisations of\nTC futures for each\nof 4 GCMs $\\mathbf{(UD5)}$",
        "elt": "1200 bootstraps of\n1981-2015 $\\mathbf{(UD4)}$",
    }

    all_agg_dims = ["year", "realisation", "elt", "gcm"]

    x_scale = 1e12
    for i_dim, (exdims, y) in enumerate(y_offsets.items()):
        l_exdims = exdims.split(",")
        agg_dims = [d for d in all_agg_dims if d not in l_exdims]
        xdata_all = (
            df.loc[df["exclude_dims"] == exdims, l_exdims + ["gdad"]]
            .set_index(l_exdims)["gdad"]
            .to_xarray()
        )
        l_gcms = xdata_all['gcm'].values[[1, 2, 3, 0]] if "gcm" in xdata_all.dims else [None]
        dy = 0.5 if len(l_gcms) == 1 else 0.4
        ypad = 0.2 * dy
        for i_gcm, gcm in enumerate(l_gcms):
            xdata = xdata_all if gcm is None else xdata_all.sel(gcm=gcm)
            ymin = (
                y - (0.5 * dy if len(l_gcms) == 1 else 2 * dy + 4.5 * ypad)
                + i_gcm * (dy + 3 * ypad)
            )
            ymax = ymin + dy

            ax.vlines(
                xdata.values.ravel() / x_scale, ymin, ymax,
                colors='k', alpha=0.2, linewidth=0.5, zorder=10,
            )

            xs = xdata.quantile(q=[0.5, 0.17, 0.83]).values / x_scale
            for zorder, alpha in [(1, 0.8), (20, 0.2)]:
                ax.fill_between(
                    xs[1:], 2 * (ymin - ypad,), 2 * (ymax + ypad,),
                    facecolor=u_const.L_DR_COLORS[dr_name], edgecolor='none',
                    alpha=alpha, zorder=zorder,
                )
            ax.vlines(
                xs[:1], ymin - ypad, ymax + ypad,
                colors='tab:red', linewidth=0.7, zorder=100
            )

    ax.set_ylabel("Uncertainty source")
    ax.set_yticks(list(y_offsets.values()))
    ax.set_yticklabels(list(labels.values()))
    ax.set_xlabel("Average DAD (Trillion US$)")

    ax = ax.twinx()
    ax.set_yticks([])
    ax.set_ylabel(f"RCP{float(rcp[3:]) / 10:.1f}-{ssp}\n{dr_name}'s discounting")


def main():
    u_plot.main_setup_mpl()

    fig = plt.figure(figsize=(u_const.PLOT_WIDTH_IN, 8.5))
    outer = mgridspec.GridSpec(2, 1, hspace=0.2, height_ratios=[9, 10])

    inner = mgridspec.GridSpecFromSubplotSpec(1, 2, subplot_spec=outer[0], wspace=0.27)

    inner2 = mgridspec.GridSpecFromSubplotSpec(
        2, 1, subplot_spec=inner[0], height_ratios=[11, 10], hspace=0.17)

    data = pd.read_csv(u_const.SOURCEDATA_DIR / "fig2a_upper.csv")
    plot_gdad_boxes(inner2[0], data)

    data = pd.read_csv(u_const.SOURCEDATA_DIR / "fig2a_lower.csv")
    plot_uncertainty_dimensions(inner2[1], data)

    ax = fig.add_subplot(inner[0], frameon=False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.text(-0.11, 1.01, "a", va="bottom", ha="right", fontweight="bold", fontsize=10,
            transform=ax.transAxes)

    ax = fig.add_subplot(inner[1])
    data = pd.read_csv(u_const.SOURCEDATA_DIR / "fig2b.csv")
    plot_cdadpc_boxes(ax, data)
    ax.text(-0.01, 1.01, "b", va="bottom", ha="right", fontweight="bold", fontsize=10,
            transform=ax.transAxes)

    ax = fig.add_subplot(outer[1])
    data = pd.read_csv(u_const.SOURCEDATA_DIR / "fig2c.csv")
    plot_cdadpc_to_income(ax, data)
    ax.text(-0.05, 1.03, "c", va="bottom", ha="right", fontweight="bold", fontsize=10,
            transform=ax.transAxes)

    path = u_const.PLOTS_DIR / f"Figure2.pdf"
    print(f"Writing to {path} ...")
    fig.savefig(path, bbox_inches="tight")


if __name__ == "__main__":
    main()
