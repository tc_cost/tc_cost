
import itertools
import os
import sys

import numpy as np
import pandas as pd
import xarray as xr

import tc_cost.util.constants as u_const
import tc_cost.util.data as u_data
import tc_cost.util.io as u_io


def aggregate_dec(ds_ssp, timehorizon, ssp, iso, disc_growth, suffix):
    ds = xr.open_dataset(
        u_const.BOOTSTRAP_DIR / "dec" / f"{ssp}-{timehorizon}-{iso}_{disc_growth}{suffix}.nc"
    )
    dr_df = ds[["rho", "eta"]].to_dataframe()[["rho", "eta"]].reset_index()
    l_quantiles = [0.167, 0.5, 0.833]

    # convert GDP per capita to total GDP (in USD) by multiplying with pop count
    ds['pop'] = ds_ssp.sel(ISO=iso, year=ds['year'].values)['pop']
    ds["marginal_dec"] = ds["marginal_dec_pc"] * ds["pop"]

    dfs = []
    for pc in ["_pc", ""]:
        yr_dfs = []
        for avg_years in [True, False]:
            index_cols = ["dr", "rcp"] + ([] if avg_years else ["year"])
            ds_var = f"marginal_dec{pc}"
            df = (
                (ds[ds_var].mean(dim="year") if avg_years else ds[ds_var])
                .quantile(q=l_quantiles, dim=["realisation", "elt", "gcm"])
                .to_dataframe().reset_index()
                .pivot(index=index_cols, columns="quantile", values=ds_var)
            )
            df.columns = [f"{ds_var}_{100 * q:.0f}" for q in l_quantiles]

            if avg_years:
                df['year'] = np.nan

            df = df.reset_index().set_index(["dr", "rcp", "year"])

            yr_dfs.append(df)
        dfs.append(pd.concat(yr_dfs))

    df = (
        pd.concat(dfs, axis=1)
        .reset_index()
        .merge(dr_df, on="dr", how="left")
        .drop(columns="dr")
    )
    return df


def main():
    try:
        task = int(os.environ["SLURM_ARRAY_TASK_ID"])
    except:
        task = int(sys.argv[-1])

    use_ols = "ols" in sys.argv
    use_temp = "temp" in sys.argv
    suffix = f"{'_ols' if use_ols else ''}{'_temp' if use_temp else ''}"

    # to reduce number of jobs, split countries into 8 groups
    l_countries = np.array_split(u_const.L_COUNTRIES, 8)

    timehorizon, ssp, countries = list(itertools.product(
        # 256 configurations
        np.arange(0, u_const.MAX_TIMEHORIZON + 1),
        u_const.L_SSP,
        l_countries,
    ))[task]

    ds_ssp = u_data.load_ssp_data().sel(SSP=ssp, ISO=countries)

    for iso in countries:
        for disc_growth in ["baseline"]:
            path = u_const.BOOTSTRAP_DIR / "dec" / (
                f"aggregate_{ssp}-{timehorizon}-{iso}_{disc_growth}{suffix}.csv"
            )
            if u_io.path_exists_safely(path):
                continue

            df = aggregate_dec(ds_ssp, timehorizon, ssp, iso, disc_growth, suffix)
            u_io.write_safely(df, path)


if __name__ == "__main__":
    main()
