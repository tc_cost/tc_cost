
import numpy as np

from tc_cost.sourcedata.fig1 import get_data_qs
import tc_cost.util.constants as u_const
import tc_cost.util.data as u_data


def data_figs03(timehorizon, nobootstrap, use_ols, use_temp):
    l_countries = u_const.L_COUNTRIES
    average_annual_aff = True
    glb = True
    ds = get_data_qs(timehorizon, glb, nobootstrap, average_annual_aff, use_ols, use_temp)
    ds['growth_losses'] = -ds['coeff_cum'].sel(aff=timehorizon, quantile=0.5)
    ds['income'] = (
        u_data.load_wid_income(l_countries, 2019, "p0p100")
        .set_index("ISO").to_xarray()['income_ppp']
    )
    return (
        ds
        .sel(ISO=~np.isnan(ds['income']))
        [["growth_losses", "income"]]
        .to_dataframe()
        .reset_index()
    )


def main():
    timehorizon = 8
    nobootstrap = False
    use_ols = True
    use_temp = True

    path = u_const.SOURCEDATA_DIR / "figs03.csv"
    df = data_figs03(timehorizon, nobootstrap, use_ols, use_temp)
    print(f"Writing to {path} ...")
    df.to_csv(path, index=None)


if __name__ == "__main__":
    main()
