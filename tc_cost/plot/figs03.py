
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import tc_cost.util.constants as u_const
import tc_cost.util.plot as u_plot


def main():
    u_plot.main_setup_mpl()

    df = pd.read_csv(u_const.SOURCEDATA_DIR / "figs03.csv")
    df['income_log10'] = np.log10(df['income'])

    x = df['income_log10'].values
    x_median = df['income_log10'].median()
    x_min = df['income_log10'].min()
    x_max = df['income_log10'].max()
    y = df['growth_losses'].values
    y_median = df['growth_losses'].median()
    y_min = df['growth_losses'].min()
    y_max = df['growth_losses'].max()
    ctry_name = df['ISO'].values

    fig = plt.figure(figsize=(u_const.PLOT_WIDTH_IN, 5))
    ax = fig.gca()

    ctry_pos = {
        "AUS": ((-1, -3), "center", "top"),
        "CAN": ((1, 3), "center", "bottom"),
        "MOZ": ((-3, 0), "right", "top"),
        "MEX": ((0, -3), "center", "top"),
        "CHN": ((0, 1), "center", "bottom"),
        "KOR": ((3, 0), "left", "top"),
        "CUB": ((-3, 0), "right", "center"),
        "DOM": ((3, 0), "left", "top"),
        "THA": ((-3, 0), "right", "top"),
        "GTM": ((-3, 0), "right", "bottom"),
        "IND": ((3, 0), "left", "top"),
        "LAO": ((-3, 0), "right", "bottom"),
        "PNG": ((0, -3), "center", "top"),
        "JAM": ((-3, 0), "right", "bottom"),
        "NIC": ((1, -1), "left", "top"),
        "HND": ((2, -1), "left", "bottom"),
        "MMR": ((-3, 0), "right", "center"),
        "HTI": ((-3, 0), "right", "top"),
        "BGD": ((-3, 0), "right", "center"),
        "KHM": ((-3, 0), "right", "top"),
    }

    ax.scatter(x, y, s=5, c='k')
    for cx, cy, ctxt in zip(x, y, ctry_name):
        xytext, ha, va = ctry_pos.get(ctxt, ((3, 0), "left", "bottom"))
        ax.annotate(ctxt, (cx, cy), xytext=xytext, ha=ha, va=va, textcoords="offset points")

    ax.set_xlim(*np.log10([3000, 100000]))
    ax.set_xticks(np.log10([5000, 10000, 50000, 100000]))
    ax.xaxis.set_major_formatter(lambda v, pos: f"{10**v / 1000:.0f}")
    ax.set_xticks(np.log10(np.concatenate([
        np.arange(3e3, 1e4, 1e3),
        np.arange(1e4, 1e5, 1e4),
    ])), minor=True)

    xmin, xmax = ax.get_xlim()
    xlen = xmax - xmin
    ax.set_xlim(xmin - 0.1 * xlen, xmax + 0.03 * xlen)
    ymin, ymax = ax.get_ylim()
    ylen = ymax - ymin
    ax.set_ylim(ymin - 0.1 * ylen, ymax + 0.1 * ylen)

    ax.axhline(y_median, color="black", linestyle=":", linewidth=0.75)
    ax.text(ax.get_xlim()[1] - 0.005 * xlen, y_median + 0.005 * ylen, "Median impact",
            ha="right", va="bottom")
    ax.axvline(x_median, color="black", linestyle=":", linewidth=0.75)
    ax.text(x_median - 0.002 * xlen, ax.get_ylim()[1] - 0.01 * ylen, "Median income",
            rotation=90, ha="right", va="top")

    ax.set_xlabel("2019 average household income ($10^3$ US\$)")
    ax.set_ylabel("Average annual growth losses due to TCs in 1981-2015 (%)")

    trans = ax.transAxes
    for in_s, in_al, in_co in [("low", "left", 0.005), ("high", "right", 0.995)]:
        for im_s, im_al, im_co in [("low", "bottom", 0.01), ("high", "top", 0.99)]:
            ax.text(in_co, im_co, f"{in_s}-income/{im_s}-impact", ha=in_al, va=im_al,
                    transform=trans)

    outpath = u_const.PLOTS_DIR / f"FigureS03.pdf"
    print(f"Writing to {outpath} ...")
    fig.savefig(outpath, bbox_inches="tight")


if __name__ == "__main__":
    main()
