
import itertools
import os
import sys

import numpy as np
import xarray as xr

import tc_cost.util.constants as u_const
import tc_cost.util.io as u_io


def aggregate_scc(timehorizon, rcp, ssp, suffix):
    # aggregates the SCC per country, SSP, rcp, discount rate, and time lags
    print(f"{rcp} {ssp} {timehorizon}")
    scc_dir = u_const.BOOTSTRAP_DIR / "scc"
    all_scc_paths = scc_dir.glob(f"{ssp}-{timehorizon}-*-{rcp}-*{suffix}.nc")
    l_countries = sorted(set(p.stem.split("-")[-1].strip(suffix) for p in all_scc_paths))

    ds_scc = xr.open_mfdataset([
        [
            scc_dir / f"{ssp}-{timehorizon}-{gcm}-{rcp}-{iso}{suffix}.nc"
            for iso in l_countries
        ] for gcm in u_const.L_GCM
    ], combine="nested", concat_dim=["gcm", "ISO"], parallel=True)
    ds_scc['ISO'] = ("ISO", l_countries)
    for v in ["rho", "eta"]:
        ds_scc[v] = ds_scc[v].isel({d: 0 for d in ds_scc[v].dims if d != "dr"})

    ds_scc = ds_scc.chunk(dict(gcm=-1))

    # fix legacy pipeline naming convention
    if "scc_bhm" in ds_scc.variables:
        ds_scc = ds_scc.rename({"scc_bhm": "scc_temp"})

    for s in ["", "_temp"]:
        ds_scc[f"scc_wld{s}"] = ds_scc[f"scc{s}"].sum(dim="ISO")

    tc_countries = u_const.L_COUNTRIES
    for s in ["", "_temp"]:
        ds_scc[f"scc_aff{s}"] = ds_scc[f"scc{s}"].sel(ISO=tc_countries).sum(dim="ISO")
        ds_scc[f"scc_affrelwld{s}"] = ds_scc[f"scc_aff{s}"] / ds_scc[f"scc_wld{s}"]

    for s in ["_wld", "_aff", ""]:
        ds_scc[f"scc{s}_tc"] = ds_scc[f"scc{s}"] - ds_scc[f"scc{s}_temp"]
        ds_scc[f"scc{s}_tcreltc"] = ds_scc[f"scc{s}_tc"] / ds_scc[f"scc_wld_tc"]
        ds_scc[f"scc{s}_tcreltemp"] = ds_scc[f"scc{s}_tc"] / ds_scc[f"scc{s}_temp"]
        ds_scc[f"scc{s}_tcrelall"] = ds_scc[f"scc{s}_tc"] / ds_scc[f"scc{s}"]

    nonqdims = ["dr", "SSP", "rcp", "ISO"]
    quantiles = [0.5, 0.167, 0.833, 0.01, 0.99]
    all_qvars = []
    for s1 in ["", "_aff", "_wld", "_affrelwld"]:
        for s2 in ["", "_temp", "_tc", "_tcreltc", "_tcreltemp", "_tcrelall"]:
            var = f"scc{s1}{s2}"
            if var not in ds_scc.variables:
                continue
            qdims = [d for d in ds_scc[var].dims if d not in nonqdims]
            qvars = [f"{var}{100 * q:02.0f}" for q in quantiles]
            for v, q in zip(qvars, quantiles):
                ds_scc[v] = ds_scc[var].quantile(q=q, dim=qdims)
            all_qvars += qvars

    bin_min = ds_scc["scc_tc01"].min().compute()
    bin_max = ds_scc["scc_tc99"].max().compute()
    bins = np.linspace(bin_min, bin_max, 200)
    ds_scc["bins"] = ("bins", bins[:-1])
    ds_scc["scc_tc_hist"] = (["ISO", "dr", "bins"], np.array([
        [
            np.histogram(
                ds_scc.sel(ISO=iso, dr=dr)["scc_tc"].values.ravel(),
                bins=bins, density=True,
            )[0]
            for dr in ds_scc["dr"].values
        ] for iso in ds_scc["ISO"].values
    ]))
    all_qvars.append("scc_tc_hist")

    return ds_scc[all_qvars + ["rho", "eta"]].squeeze()


def main():
    try:
        task = int(os.environ["SLURM_ARRAY_TASK_ID"])
    except:
        task = int(sys.argv[-1])

    use_ols = "ols" in sys.argv
    use_temp = "temp" in sys.argv
    suffix = f"{'_ols' if use_ols else ''}{'_temp' if use_temp else ''}"

    timehorizon, ssp, rcp = list(itertools.product(
        # 96 configurations
        np.arange(0, u_const.MAX_TIMEHORIZON + 1),
        u_const.L_SSP,
        u_const.L_RCP_LOWER,
    ))[task]

    outpath = (
        u_const.BOOTSTRAP_DIR / "scc"
        / f"aggregate_{timehorizon}-{rcp}-{ssp}{suffix}.nc"
    )

    if u_io.path_exists_safely(outpath):
        return

    ds = aggregate_scc(timehorizon, rcp, ssp, suffix)
    ds.load()
    u_io.write_safely(ds, outpath)


if __name__ == "__main__":
    main()
