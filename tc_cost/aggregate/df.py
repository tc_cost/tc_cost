
import itertools
import os
import sys

import numpy as np
import xarray as xr

import tc_cost.util.constants as u_const
import tc_cost.util.io as u_io


def aggregate_damfuns(timehorizon, iso, use_ols, use_temp):
    base_path = u_const.BOOTSTRAP_DIR / "damage_functions"
    gmt_bounds = (0.0, 8.0)

    q_dims = ["realisation", "elt", "gcm"]
    suffix = f"{'_ols' if use_ols else ''}{'_temp' if use_temp else ''}"
    ds = xr.open_dataset(
        u_const.BOOTSTRAP_DIR / "damage_functions" / (
            f"{timehorizon}-allgcms-{iso}{suffix}.nc"
        )
    )
    ds["realisation"] = ds["realisation"].astype(str)
    ds["estimates"] = ds["estimates"].fillna(0)
    ds["gmt"] = ("gmt", np.linspace(*gmt_bounds, 100))

    ds["damfun"] = (
        ds['estimates'].sel(coeff="const")
        + ds['estimates'].sel(coeff="gmt") * ds['gmt']
    )
    ds["damfun"] += (
        ds["damfun"]
        .sel(rcp="__all__")
        .copy()
        .expand_dims(rcp=[v for v in ds["rcp"].values if v != "__all__"])
        .reindex(rcp=ds["rcp"].values)
        .fillna(0)
    )

    return ds[["damfun"]].quantile(q=[0.05, 0.167, 0.5, 0.833, 0.95], dim=q_dims)


def main():
    try:
        task = int(os.environ["SLURM_ARRAY_TASK_ID"])
    except:
        task = int(sys.argv[-1])

    timehorizon = 8
    use_ols = "ols" in sys.argv
    use_temp = "temp" in sys.argv
    suffix = f"{'_ols' if use_ols else ''}{'_temp' if use_temp else ''}"

    # 42 configurations
    iso = (["GLB"] + u_const.L_COUNTRIES)[task]

    nc_path = u_const.BOOTSTRAP_DIR / "damage_functions" / (
        f"aggregate_{timehorizon}-{iso}{suffix}.nc"
    )
    if u_io.path_exists_safely(nc_path):
        return

    ds = aggregate_damfuns(timehorizon, iso, use_ols, use_temp)
    u_io.write_safely(ds, nc_path)


if __name__ == "__main__":
    main()
