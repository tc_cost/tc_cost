
import colorsys

import geopandas as gpd
import matplotlib
import matplotlib.colors as mcolors
import matplotlib.lines as mlines
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import matplotlib.text as mtext
import shapely.affinity
import shapely.geometry
import shapely.ops


def main_setup_mpl(agg=True):
    if agg:
        matplotlib.use("Agg")
    plt.rcParams.update(
        {
            "lines.markeredgewidth": 0,
            "lines.markersize": 6,
            "lines.linewidth": 1.0,
            "axes.labelpad": 5,
            "xtick.minor.size": 2,
            "ytick.minor.size": 2,
            "xtick.major.size": 2,
            "ytick.major.size": 2,
            "font.size": 7,
            "legend.fontsize": 7,
            "xtick.labelsize": 7,
            "ytick.labelsize": 7,
            "axes.labelsize": 7,
            "axes.titlesize": 7,
            "font.family": "sans-serif",
        }
    )


class LegendTitle:
    def __init__(self, text_props=None):
        self.text_props = text_props or {}
        super(LegendTitle, self).__init__()

    def legend_artist(self, legend, orig_handle, fontsize, handlebox):
        x0, y0 = handlebox.xdescent, handlebox.ydescent
        text_props = self.text_props.copy()
        text_props['fontsize'] = text_props.get("fontsize", fontsize)
        title = mtext.Text(x0, y0, orig_handle, **text_props)
        handlebox.add_artist(title)
        return title


class LineCIHandler:
    def __init__(self, plot_scatter):
        self.plot_scatter = plot_scatter.lower()

    def legend_artist(self, legend, orig_handle, fontsize, handlebox):
        orig_handle, ci_color = orig_handle
        x0, y0 = handlebox.xdescent, handlebox.ydescent
        width, height = handlebox.width, handlebox.height
        if ci_color != "#333333" and self.plot_scatter in ["ellipse", "circle"]:
            if self.plot_scatter == "ellipse":
                radius = 0.95 * height
                patch = mpatches.Ellipse([x0 + width, y0 + 0.5 * height],
                                        width=0.6 * radius, height=radius,
                                        facecolor=mcolors.to_rgb(ci_color) + (0.5,))
                width -= 0.6 * radius
            else:
                radius = 0.42 * height
                patch = mpatches.Circle([x0 + width - radius, y0 + 0.5 * height],
                                        radius=radius,
                                        facecolor=ci_color)
                width -= 2.3 * radius
            handlebox.add_artist(patch)
        patch = mpatches.Rectangle([x0, y0], width, height, facecolor=ci_color, alpha=0.3)
        handlebox.add_artist(patch)
        patch = mlines.Line2D([x0, x0 + width], [y0 + 0.5 * height, y0 + 0.5 * height],
                              ls=orig_handle.get_linestyle(),
                              c=orig_handle.get_color(),
                              lw=orig_handle.get_linewidth(),
                              transform=handlebox.get_transform())
        handlebox.add_artist(patch)
        return patch


def lighten_mcolor_hls(color, factor):
    hls = colorsys.rgb_to_hls(*mcolors.to_rgb(color))
    hls = (hls[0], np.clip(1 - factor * (1 - hls[1]), 0, 1), hls[2])
    return colorsys.hls_to_rgb(*hls)


def lighten_mcolor(color, factor):
    # interpolate with white to achieve same effect as alpha over white background
    return tuple([factor * v + (1 - factor) * 1 for v in mcolors.to_rgb(color)])


def map_add_gridlines(ax, xlocs, ylocs, extent, transform):
    xmin, xmax, ymin, ymax = extent
    width, height = xmax - xmin, ymax - ymin
    color = (0.1, 0.1, 0.1, 0.6)
    ax.gridlines(
        ylocs=ylocs, xlocs=xlocs, crs=transform,
        linewidth=0.5, linestyle=":", color=color, zorder=20,
    )
    for loc in xlocs:
        ax.text(
            loc + 0.01 * width, ymin + 0.012 * height,
            f"{abs(loc):.0f}°{'' if loc == 0 else 'W' if loc < 0 else 'E'}",
            rotation="vertical", ha="left", va="bottom",
            fontsize=8, color=color,
            zorder=20, transform=transform,
        )
    for loc in ylocs:
        ax.text(
            xmin + 0.005 * width, loc,
            f"{abs(loc):.0f}°{'' if loc == 0 else 'S' if loc < 0 else 'N'}",
            ha="left", va="bottom",
            fontsize=8, color=color,
            zorder=20, transform=transform,
        )
