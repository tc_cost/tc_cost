
import numpy as np
import pandas as pd
import xarray as xr

import tc_cost.util.constants as u_const
import tc_cost.util.data as u_data


def concat_dad_ssp(timehorizon, ssp, disc_growth, use_ols, use_temp):
    # concatenate DAD results into one dataframe
    base_path = u_const.BOOTSTRAP_DIR / "dec"
    print(f"Reading DEC data for {ssp} from {base_path} ...")

    suffix = f"{'_ols' if use_ols else ''}{'_temp' if use_temp else ''}"

    l_countries = u_const.L_COUNTRIES

    df = []
    for iso in l_countries:
        c_df = pd.read_csv(
            base_path / f"aggregate_{ssp}-{timehorizon}-{iso}_{disc_growth}{suffix}.csv"
        )
        c_df["ISO"] = iso
        df.append(c_df)
    df = pd.concat(df)
    return df


def load_gdad(timehorizon, disc_growth, use_ols, use_temp):
    dec = []
    for ssp in u_const.L_SSP:
        df_ssp = concat_dad_ssp(timehorizon, ssp, disc_growth, use_ols, use_temp)
        df_ssp = df_ssp.loc[
            np.isnan(df_ssp['year']),
            ["ISO", "rcp", "rho", "eta", "marginal_dec_50", "marginal_dec_83", "marginal_dec_17"],
        ].copy()
        for dr_name, dr in u_const.L_DR['ramsey'].items():
            df_dr = df_ssp[(df_ssp.rho == dr[0]) & (df_ssp.eta == dr[1])].copy()
            df_dr = df_dr.groupby("rcp").sum().reset_index()
            data = pd.DataFrame({p: df_dr[f'marginal_dec_{p}'].values for p in ["50", "83", "17"]})
            data["rcp"] = u_const.L_RCP
            data['err_low'] = data['50'] - data['17']
            data['err_high'] = data['83'] - data['50']
            data["SSP"] = ssp
            data["dr_name"] = dr_name
            dec.append(data)
    return pd.concat(dec).set_index(["SSP", "rcp", "dr_name"]).reset_index()


def load_dadpc(ssp, rcp, timehorizon, dr, disc_growth, use_ols, use_temp):
    # DAD per capita and per-capita income
    EC_df = concat_dad_ssp(timehorizon, ssp, disc_growth, use_ols, use_temp)
    EC_df = EC_df[np.isnan(EC_df['year'])].copy()
    EC_df = EC_df[
        (EC_df.rho == dr[0]) & (EC_df.eta == dr[1]) & (EC_df.rcp == rcp)
    ].copy()
    l_countries = EC_df.ISO.drop_duplicates().values
    return EC_df


def data_fig2a_upper(timehorizon, disc_growth, use_ols, use_temp, ssp):
    df = (
        load_gdad(timehorizon, disc_growth, use_ols, use_temp)
        .rename(columns={"50": "median"})
        [["SSP", "rcp", "dr_name", "median", "err_low", "err_high"]]
    )
    return df[df["SSP"] == ssp].reset_index(drop=True)


def data_fig2a_lower(ssp, rcp, timehorizon, dr_name, disc_growth, use_ols, use_temp):
    suffix = f"{'_ols' if use_ols else ''}{'_temp' if use_temp else ''}"
    ds = xr.open_dataset(
        u_const.BOOTSTRAP_DIR / "dec"
        / f"aggregate_unc_{ssp}-{timehorizon}-{rcp}_{disc_growth}_{dr_name}{suffix}.nc"
    )

    all_agg_dims = ["year", "realisation", "elt", "gcm"]
    exclude_dims = [["year"], ["realisation", "gcm"], ["elt"]]

    print("Computing averages across subsets of the uncertainty dimensions...")
    averages = [
        ds["gdad"]
        .mean(dim=[d for d in all_agg_dims if d not in exdims])
        .compute()
        .to_dataframe()
        .drop(columns=["SSP", "rcp"])
        .reset_index()
        for exdims in exclude_dims
    ]
    for av, exdims in zip(averages, exclude_dims):
        av["exclude_dims"] = ",".join(exdims)

    df = pd.concat(averages)
    df["SSP"] = ssp
    df["rcp"] = rcp
    df["dr_name"] = dr_name

    return df


def data_fig2b(ssp, rcp, timehorizon, dr_name, disc_growth, use_ols, use_temp):
    dr = u_const.L_DR['ramsey'][dr_name]
    df = concat_dad_ssp(timehorizon, ssp, disc_growth, use_ols, use_temp)
    df = df[np.isnan(df['year'])].copy().drop(columns=['year'])
    df = df[
        (df.rho == dr[0]) & (df.eta == dr[1]) & (df.rcp == rcp)
    ].copy()
    df = (
        df.nlargest(20, "marginal_dec_pc_50")
        .reset_index(drop=True)
        .rename(columns={
            "marginal_dec_pc_50": "dad50",
            "marginal_dec_pc_17": "dad17",
            "marginal_dec_pc_83": "dad83",
        })
        [["ISO", "dad50", "dad17", "dad83"]]
    )
    df["SSP"] = ssp
    df["rcp"] = rcp
    df["dr_name"] = dr_name
    return df


def data_fig2c(ssp, rcp, timehorizon, dr_name, disc_growth, use_ols, use_temp):
    l_countries = u_const.L_COUNTRIES
    dr = u_const.L_DR['ramsey'][dr_name]
    df = (
        load_dadpc(ssp, rcp, timehorizon, dr, disc_growth, use_ols, use_temp)
        .merge(u_data.load_wid_income(l_countries, 2019, "p0p100"), on="ISO", how="left")
    )
    df["perc_of_ahi"] = 100 * df["marginal_dec_pc_50"] / df["income_ppp"]
    df["days_of_income"] = 365 * df["perc_of_ahi"] / 100
    df = (
        df
        .dropna(axis=0, subset=["income_ppp"])
        .drop(columns=["rcp", "year"])
        [["ISO", "perc_of_ahi", "days_of_income", "income_ppp"]]
    )
    df["SSP"] = ssp
    df["rcp"] = rcp
    df["dr_name"] = dr_name
    return df


def main():
    timehorizon = 8
    use_ols = True
    use_temp = True
    disc_growth = "baseline"
    dr_name = "Ricke"
    rcp = "rcp60"
    ssp = "SSP2"

    args = (timehorizon, disc_growth, use_ols, use_temp)
    ext_args = (ssp, rcp, timehorizon, dr_name, disc_growth, use_ols, use_temp)

    path = u_const.SOURCEDATA_DIR / "fig2a_upper.csv"
    df = data_fig2a_upper(*args, ssp)
    print(f"Writing to {path} ...")
    df.to_csv(path, index=None)

    path = u_const.SOURCEDATA_DIR / "fig2a_lower.csv"
    df = data_fig2a_lower(*ext_args)
    print(f"Writing to {path} ...")
    df.to_csv(path, index=None)

    path = u_const.SOURCEDATA_DIR / "fig2b.csv"
    df = data_fig2b(*ext_args)
    print(f"Writing to {path} ...")
    df.to_csv(path, index=None)

    path = u_const.SOURCEDATA_DIR / "fig2c.csv"
    df = data_fig2c(*ext_args)
    print(f"Writing to {path} ...")
    df.to_csv(path, index=None)


if __name__ == "__main__":
    main()
