
import numpy as np
import pandas as pd

from tc_cost.sourcedata.fig2 import load_gdad
import tc_cost.util.constants as u_const
import tc_cost.util.data as u_data


def data_tabs1(timehorizon, disc_growth, use_ols, use_temp):
    gwp_2021 = u_data.load_ssp_data().sel(year=2021)['gdp'].sum(dim="ISO")

    df_all = load_gdad(timehorizon, disc_growth, use_ols, use_temp)
    l_dr_names = np.unique(df_all["dr_name"].values)
    l_percs = ["50", "83", "17"]

    df = []
    for ssp in u_const.L_SSP:
        for dr_name in l_dr_names:
            dr = u_const.L_DR['ramsey'][dr_name]
            tmp_df = df_all[
                (df_all["SSP"] == ssp) & (df_all["dr_name"] == dr_name)
            ].copy()
            tmp_df['SSP'] = ssp
            tmp_df['dr'] = f"rho={dr[0] * 100:.1f}%_eta={dr[1]}"
            for p in l_percs:
                tmp_df[f'{p}_share'] = tmp_df[p] / gwp_2021.sel(SSP=ssp).item() * 100
            columns = ['SSP', 'dr', "rcp"] + l_percs + [f'{p}_share' for p in l_percs]
            df.append(tmp_df.loc[:, columns])
    return pd.concat(df)


def tex_tabs1(df):
    table_str = ""
    for i_ssp, ssp in enumerate(u_const.L_SSP):
        for i_rcp, rcp in enumerate(u_const.L_RCP):
            for suffix in ['', '_share']:
                row_str = ("\\multirow{6}{*}{\\rotatebox[origin=c]{90}{%s}}" % (ssp)
                           if i_rcp == 0 and suffix == "" else "")
                row_str += "&"
                if suffix == "":
                    row_str += ("\\multirow{2}{*}{\\rotatebox[origin=c]{0}{%s}}" % (rcp))
                for dr in np.unique(df['dr'].values):
                    row_data = df[
                        (df['dr'] == dr) & (df['SSP'] == ssp) & (df["rcp"] == rcp)
                    ].iloc[0]
                    med = row_data[f'50{suffix}']
                    low = row_data[f'17{suffix}']
                    high = row_data[f'83{suffix}']
                    row_str += " & "
                    if suffix == "":
                        row_str += f"{med / 1e12:.2f} ({low / 1e12:.2f}--{high / 1e12:.2f})"
                    else:
                        row_str += f"{med:.2f}\\% ({low:.2f}\\%--{high:.2f}\\%)"
                tline = "\\midrule" if suffix != "" and i_rcp == 2 and i_ssp == 0 else ""
                table_str += f"{row_str}\\\\{tline}\n"
    return table_str


def main():
    timehorizon = 8
    use_ols = True
    use_temp = True
    disc_growth = "baseline"

    path = u_const.SOURCEDATA_DIR / "tabs1.csv"
    df = data_tabs1(timehorizon, disc_growth, use_ols, use_temp)
    print(f"Writing to {path} ...")
    df.to_csv(path, index=None)

    path = u_const.TABLES_DIR / "tabs1.tex"
    s_tex = tex_tabs1(df)
    print(f"Writing to {path} ...")
    with open(path, "w") as fp:
        fp.write(s_tex)


if __name__ == "__main__":
    main()
