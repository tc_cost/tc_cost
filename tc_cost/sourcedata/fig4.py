
import numpy as np
import xarray as xr

import tc_cost.util.constants as u_const


def load_aggregate_scc(timehorizon, use_ols, use_temp):
    suffix = f"{'_ols' if use_ols else ''}{'_temp' if use_temp else ''}"
    ds = xr.open_mfdataset([
        [
            u_const.BOOTSTRAP_DIR / "scc"
            / f"aggregate_{timehorizon}-{rcp}-{ssp}{suffix}.nc"
            for ssp in u_const.L_SSP
        ] for rcp in u_const.L_RCP_LOWER
    ], combine="nested", concat_dim=["rcp", "SSP"])
    ds['SSP'] = ("SSP", u_const.L_SSP)
    ds['rcp'] = ("rcp", u_const.L_RCP_LOWER)
    for v in ["rho", "eta"]:
        ds[v] = ds[v].isel({d: 0 for d in ds[v].dims if d != "dr"})
    ds = ds[[v for v in ds.variables if "hist" not in v and "bins" not in v]].squeeze()
    return ds


def data_fig4a(timehorizon, use_ols, use_temp):
    qcols = [f"scc_wld{s}{q}" for q in [17, 50, 83] for s in ["", "_temp"]]
    return (
        load_aggregate_scc(timehorizon, use_ols, use_temp)[qcols + ["rho", "eta"]]
        .to_dataframe()
        .reset_index()
        .drop(columns=["dr", "quantile"])
    )


def data_fig4b(ssp, rcp, timehorizon, dr_name, use_ols, use_temp):
    dr = u_const.L_DR['ramsey'][dr_name]
    ds = load_aggregate_scc(timehorizon, use_ols, use_temp).sel(SSP=ssp, rcp=rcp)
    def_dr_indexer = ((ds["rho"] == dr[0]) & (ds["eta"] == dr[1])).compute()
    ds = ds.sel(dr=def_dr_indexer).squeeze()
    l_countries = ds.sortby("scc_tcreltc50", ascending=False)["ISO"].values[:10]
    df = (
        ds.sel(ISO=l_countries)
        .to_dataframe()
        .reset_index()
    )
    df["dr_name"] = dr_name
    return df[
        ["ISO", "rcp", "SSP", "dr_name"]
        + [f"scc{s}{q}" for s in ["", "_temp"] for q in [50, 17, 83]]
    ]


def data_fig4c(ssp, rcp, timehorizon, dr_name, use_ols, use_temp):
    suffix = f"{'_ols' if use_ols else ''}{'_temp' if use_temp else ''}"
    dr = u_const.L_DR['ramsey'][dr_name]
    ds_scc_agg = xr.open_dataset(
        u_const.BOOTSTRAP_DIR / "scc"
        / f"aggregate_{timehorizon}-{rcp}-{ssp}{suffix}.nc"
    )
    dr_indexer = ((ds_scc_agg.rho == dr[0]) & (ds_scc_agg.eta == dr[1])).compute()
    ds_scc_agg = ds_scc_agg.sel(dr=dr_indexer).squeeze()
    l_countries = ds_scc_agg.sortby("scc_tcreltc50", ascending=False)["ISO"].values[:5]
    df = (
        ds_scc_agg
        .sel(ISO=l_countries)["scc_tc_hist"]
        .dropna(dim="bins")
        .to_dataframe()
        .reset_index()
        .drop(columns=["quantile"])
    )
    df["dr_name"] = dr_name
    return df


def main():
    use_ols = True
    use_temp = True
    timehorizon = 8
    rcp = "rcp60"
    ssp = "SSP2"
    dr_name = "Ricke"

    path = u_const.SOURCEDATA_DIR / "fig4a.csv"
    df = data_fig4a(timehorizon, use_ols, use_temp)
    print(f"Writing to {path} ...")
    df.to_csv(path, index=None)

    path = u_const.SOURCEDATA_DIR / "fig4b.csv"
    df = data_fig4b(ssp, rcp, timehorizon, dr_name, use_ols, use_temp)
    print(f"Writing to {path} ...")
    df.to_csv(path, index=None)

    path = u_const.SOURCEDATA_DIR / "fig4c.csv"
    df = data_fig4c(ssp, rcp, timehorizon, dr_name, use_ols, use_temp)
    print(f"Writing to {path} ...")
    df.to_csv(path, index=None)


if __name__ == "__main__":
    main()
