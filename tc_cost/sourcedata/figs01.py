
import numpy as np
import pandas as pd
import statsmodels.formula.api as smf
import xarray as xr

import tc_cost.bootstrap.fit as bs_fit
import tc_cost.util.constants as u_const
import tc_cost.util.data as u_data


def load_fitdata_all(timehorizon, exclude_sources=None):
    exclude_sources = [] if exclude_sources is None else exclude_sources
    ds_bhm = u_data.load_bhm_data()
    ds_aff = read_aff_people_hist(timehorizon)
    ds_gdppc = u_data.load_hist_gdppc()
    ds_all = {}
    for source in ["burke", "hazem", "gswp3-w5e5"]:
        if source in exclude_sources:
            continue
        if source == "burke":
            ds = ds_bhm
        else:
            ds = u_data.load_hist_temp(source=source)
            ds = ds.merge(ds_gdppc)
            ds["time"] = ds["year"] - 1960
            ds["time2"] = ds["time"]**2
            ds["temp2"] = ds["temp"]**2
        ds = ds.merge(
            ds_aff
            .reindex(ISO=ds["ISO"].values)
            .fillna(0)
        )
        ds_all[source] = ds
    return ds_all


def read_aff_people_hist(timehorizon):
    da = (
        bs_fit.add_aff_years_to_ds(
            u_data.load_tce_dat_annual_shares(),
            timehorizon,
            (1960, 2015),
        )["affected"]
        .sel(ISO=u_const.L_COUNTRIES)
    )
    da["aff"] = [f"aff{a:.0f}" for a in da["aff"].values]
    return da.to_dataset(dim="aff")


def load_bootstrap_with_pred(timehorizon, x_vals, suffix):
    path = u_const.BOOTSTRAP_DIR / "estimates" / f"coeffs_{timehorizon}{suffix}.nc"
    ds = xr.open_dataset(path)
    ds["temp"] = ("x", x_vals)
    ds["temp2"] = ds["temp"]**2
    ds["exog_pred"] = ds[["temp", "temp2"]].to_array(dim="coeff").broadcast_like(ds["elt"])
    ds["exog_pred"] = ds["exog_pred"].fillna(ds["exog_mean"])
    ds = ds.drop_vars(["temp", "temp2"])
    return ds


def ols_fit_model(data, tc_effect=False, temp=True, precip=False, time_sq=True):
    aff_cols = [c for c in data.columns if c.startswith("aff")]
    formula = "gdppc_ch ~ " + " + ".join(
        (aff_cols if tc_effect else [])
        + (["temp", "temp2"] if temp else [])
        + (["precip", "precip2"] if precip else [])
        + (["C(ISO):time2"] if time_sq else [])
        + ["C(ISO)", "C(time)", "C(ISO):time"]
    )
    return smf.ols(formula=formula, data=data).fit()


def predict_temp_from_ols(res, x_vals):
    mod = res.model
    exog_mean = pd.DataFrame(mod.exog, columns=mod.exog_names).mean()
    exog_at = pd.DataFrame({
        **exog_mean.to_dict(),
        "temp": x_vals,
    })
    exog_at["temp2"] = exog_at["temp"]**2
    res_pred = res.get_prediction(exog=exog_at, transform=False)
    predicted = res_pred.predicted_mean
    # obs=False means confidence intervals (change to obs=True for prediction intervals)
    pred_ci = res_pred.conf_int(alpha=0.1, obs=False)
    return predicted, pred_ci


def predict_tc_from_ols(res):
    mod = res.model
    exog_zero = 0 * pd.DataFrame(mod.exog, columns=mod.exog_names).mean()
    timehorizon = len([v for v in mod.exog_names if v.startswith("aff")]) - 1
    exog_at = pd.DataFrame({
        **exog_zero.to_dict(),
        **{
            f"aff{L}": [1 if L <= l else 0 for l in range(timehorizon + 1)]
            for L in range(timehorizon + 1)
        }
    })
    res_pred = res.get_prediction(exog=exog_at, transform=False)
    predicted = res_pred.predicted_mean
    # obs=False means confidence intervals (change to obs=True for prediction intervals)
    pred_ci = res_pred.conf_int(alpha=0.34, obs=False)
    return predicted, pred_ci


def predict_temp_from_bs(ds):
    values = (ds["exog_pred"] * ds["Estimate"]).sum(dim="coeff")
    predicted = values.mean(dim="elt").values
    pred_ci = np.stack([
        values.quantile(q=0.05, dim="elt").values,
        values.quantile(q=0.95, dim="elt").values,
    ], axis=1)
    return predicted, pred_ci


def predict_tc_from_bs(ds):
    aff_coeffs = [c for c in ds["coeff"].values if c.startswith("aff")]
    values = ds["Estimate"].sel(coeff=aff_coeffs).cumsum(dim="coeff")
    predicted = values.mean(dim="elt").values
    pred_ci = np.stack([
        values.quantile(q=0.05, dim="elt").values,
        values.quantile(q=0.95, dim="elt").values,
    ], axis=1)
    return predicted, pred_ci


def prepare_tdf_comparison(
    source, period, bhm_ctry, color,
    temp_pred_vals=None,
    ds_fitdata=None,
    bhm_countries=None,
):
    res = None
    temp_predicted = temp_pred_ci = None
    tc_predicted = tc_pred_ci = None

    if temp_pred_vals is None:
        temp_pred_vals = np.linspace(-3, 30.5, 100)

    if source == "ricke_bhm_notc":
        # load reference from Ricke repository
        period = (1961, 2010)
        temp_predicted = (
            temp_pred_vals * ds_fitdata["temp"].item()
            + temp_pred_vals**2 * ds_fitdata["temp2"].item()
            # manually adjust constant term since it's not in Ricke's repository
            - 0.021
        )
    elif source.startswith("bootstrap"):
        period = (1981, 2015) if period is None else period
        source = f"{source}_bhm" if bhm_ctry else source
        if "_temp" in source:
            temp_predicted, temp_pred_ci = predict_temp_from_bs(ds_fitdata)
        if "_notc" not in source:
            tc_predicted, tc_pred_ci = predict_tc_from_bs(ds_fitdata)
    else:
        # fit an OLS model and predict on the fly
        aff_cols = [v for v in ds_fitdata.data_vars if v.startswith("aff")]
        fitrd = (
            ds_fitdata
            .to_dataframe()
            .dropna(subset=["gdppc_ch", "temp", "temp2", "time"] + aff_cols, how="any")
            .reset_index()
        )
        if bhm_ctry:
            fitrd = fitrd[fitrd["ISO"].isin(bhm_countries)].copy()
        if period is not None:
            fitrd = fitrd[
                (fitrd["year"] >= period[0])
                & (fitrd["year"] <= period[1])
            ].copy()
        period = (fitrd["year"].min(), fitrd["year"].max())
        res = ols_fit_model(
            fitrd,
            tc_effect="notc" not in source,
            temp=source != "notemp")
        if source != "notemp":
            temp_predicted, temp_pred_ci = predict_temp_from_ols(res, temp_pred_vals)
        if "notc" not in source:
            tc_predicted, tc_pred_ci = predict_tc_from_ols(res)

    label = f"{source} {period[0]}-{period[1]}"

    notemp = source.startswith("bootstrap") and "_temp" not in source
    plot_kwargs = dict(
        lw=(
            3 if source == "ricke_bhm_notc"
            else 2 if "notc" in source
            else 1 if notemp
            else 0.5
        ),
        ls="--" if notemp else ":" if "notc" in source else None,
        alpha=0.3 if source == "ricke_bhm_notc" else None,
    )

    return (
        (label, color, plot_kwargs),
        (temp_predicted, temp_pred_ci),
        (tc_predicted, tc_pred_ci),
        res,
    )


def print_tdf_comparison(l_plotdata, l_fitdata, ref_i, check_vars):
    # check significance of difference between estimates (w/ or w/o bootstraping)
    def _dist_coeff(d1, d2):
        return np.sqrt(((d1 - d2)**2).sum(dim="coeff"))

    def _hypothesis_test(ref_data, test_mean):
        if hasattr(ref_data, "f_test"):
            test_mean = test_mean.to_series().to_dict()
            return ref_data.f_test(", ".join(
                f"({var} = {val})"
                for var, val in test_mean.items()
            )).pvalue
        else:
            ref_mean = ref_data.mean(dim="elt")
            dist = _dist_coeff(test_mean, ref_mean)
            return (_dist_coeff(ref_data, ref_mean) >= dist).mean(dim="elt").item()

    def _res_from_pldata(pldata, fitdata):
        return (
            fitdata["Estimate"].sel(elt=fitdata["elt"] != "original")
            if pldata[3] is None else pldata[3]
        )

    def _is_ols(res):
        return not isinstance(res, xr.DataArray)

    ref_pldata = l_plotdata[ref_i]
    ref_res = _res_from_pldata(ref_pldata, l_fitdata[ref_i])
    print("Reference:", ref_pldata[0][0])
    for i, test_pldata in enumerate(l_plotdata):
        if i == ref_i:
            continue
        print(test_pldata[0][0])
        test_res = _res_from_pldata(test_pldata, l_fitdata[i])
        test_coeffs = test_res.params.index if _is_ols(test_res) else test_res["coeff"].values
        for l_vars in check_vars:
            if any(v not in test_coeffs for v in l_vars):
                continue
            pvalues = [
                _hypothesis_test(
                    d1 if _is_ols(d1) else d1.sel(coeff=list(l_vars)),
                    (
                        d2.params.to_xarray().rename(index="coeff").sel(coeff=list(l_vars))
                        if _is_ols(d2) else
                        d2.sel(coeff=list(l_vars)).mean(dim="elt")

                    ),
                )
                for d1, d2 in [(ref_res, test_res), (test_res, ref_res)]
            ]
            print(f"{l_vars[0]:6s}: {pvalues[0]:.2f}, {pvalues[1]:.2f}")


def data_figs01(timehorizon):
    temp_pred_vals = np.linspace(-3, 30.5, 100)
    tc_pred_vals = np.arange(timehorizon + 1)

    ds_all = load_fitdata_all(timehorizon, exclude_sources=["hazem", "gswp3-w5e5"])
    ds_bhm = ds_all["burke"]
    bhm_countries = ds_bhm["ISO"].values
    non_bhm_countries = np.setdiff1d(u_const.L_COUNTRIES, bhm_countries)

    l_plotconfig = [
        ("bootstrap_ols_temp", (1961, 2010), True, "tab:blue"),
        ("bootstrap_ols_temp_notc", (1961, 2010), True, "tab:blue"),
        ("bootstrap_ols_temp", (1961, 2010), False, "tab:orange"),
        ("bootstrap_ols_temp_notc", (1961, 2010), False, "tab:orange"),
        ("bootstrap_ols_temp", None, False, "tab:green"),
        ("bootstrap_ols_temp_notc", None, False, "tab:green"),
        ("bootstrap_ols", None, False, "tab:purple"),
    ]

    l_fitdata = [
        (
            load_bootstrap_with_pred(
                timehorizon, temp_pred_vals,
                source.lstrip("bootstrap") + (
                    "" if period is None else f"_{period[0]}-{period[1]}"
                ) + ("_bhm" if bhm_ctry else "")
            )
            if source.startswith("bootstrap") else
            ds_all[
                "gswp3-w5e5" if source == "notemp"
                else "burke" if source.startswith("burke")
                else source.rstrip("_notc")
            ]
        )
        for source, period, bhm_ctry, color in l_plotconfig
    ]

    l_plotdata = {
        "": [
            prepare_tdf_comparison(
                source, period, bhm_ctry, color,
                ds_fitdata=ds_fitdata,
                temp_pred_vals=temp_pred_vals,
                bhm_countries=bhm_countries,
            )
            for (source, period, bhm_ctry, color), ds_fitdata
            in zip(l_plotconfig, l_fitdata)
        ]
    }

    check_vars = [("temp", "temp2"), tuple(f"aff{l}" for l in range(timehorizon + 1))]
    for def_i in [0, 2, 4]:
        print_tdf_comparison(l_plotdata[""], l_fitdata, def_i, check_vars)
        print()

    df = []
    for (label, _, _), temp_pred, tc_pred, _ in l_plotdata[""]:
        source, period = label.split(" ")
        data = {
            "data": "Burke et al. 2015" if "bhm" in source else "own",
            "period": period,
            "tc_effects": "_notc" not in source,
            "temp_effects": "_temp" in source,
        }
        if temp_pred[0] is not None:
            data["variable"] = "temp"
            data["x"] = temp_pred_vals
            data["y_mean"] = temp_pred[0]
            data["y_5"] = temp_pred[1][:, 0]
            data["y_95"] = temp_pred[1][:, 1]
            df.append(pd.DataFrame(data))
        if tc_pred[0] is not None:
            data["variable"] = "tc"
            data["x"] = tc_pred_vals
            data["y_mean"] = tc_pred[0]
            data["y_5"] = tc_pred[1][:, 0]
            data["y_95"] = tc_pred[1][:, 1]
            df.append(pd.DataFrame(data))
    return pd.concat(df)


def main():
    timehorizon = 8

    path = u_const.SOURCEDATA_DIR / "figs01.csv"
    df = data_figs01(timehorizon)
    print(f"Writing to {path} ...")
    df.to_csv(path, index=None)


if __name__ == "__main__":
    main()
