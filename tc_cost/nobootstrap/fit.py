
import pickle

import numpy as np
import pandas as pd

import tc_cost.util.constants as u_const
import tc_cost.util.data as u_data
import tc_cost.bootstrap.fit as bs_fit
import tc_cost.bootstrap.bootstrap as bs_bootstrap

"""The period should never start before this year"""
MIN_MINYEAR = u_const.HIST_PERIOD_GDPPC[0]

"""The length of the considered period should be one of these"""
YEARCOUNTS = [35, 40, 45]


def load_fit_data(
    minyear, maxyear, timehorizon, countries,
    ds_aff=None, ds_gdppc=None, ds_temp=None,
):
    input_data = {
        "affected": bs_bootstrap.get_aff_df(minyear - timehorizon, maxyear, ds=ds_aff),
        "gdppc_ch": bs_bootstrap.get_gdppc_df(minyear, maxyear, ds=ds_gdppc),
        "temp": bs_bootstrap.get_temp_df(minyear, maxyear, ds=ds_temp),
    }

    elt = "original"
    for col, df in input_data.items():
        df = (df[df["ISO"].isin(countries)] if col == "affected" else df).copy()
        df['elt'] = elt
        input_data[col] = df.set_index(["elt", "year", "ISO"])[[col]].to_xarray()

    return [input_data[k] for k in ['gdppc_ch', 'affected', "temp"]]


def all_periods(timehorizon):
    # make sure that each period has enough years to accommodate the lags
    minyear_range = (
        max(MIN_MINYEAR, u_const.HIST_PERIOD_TC[0] + timehorizon),
        u_const.HIST_PERIOD_GDPPC[1] - min(YEARCOUNTS) + 2,
    )
    return [
        (minyear, maxyear)
        for minyear in range(*minyear_range)
        for maxyear in [
            minyear + cnt - 1
            for cnt in YEARCOUNTS
            if minyear + cnt - 1 <= u_const.HIST_PERIOD_GDPPC[1]
        ] + [u_const.HIST_PERIOD_GDPPC[1]]
    ]


def main():
    use_temp = "temp" in sys.argv

    # leave at least 15 years extra because the robust regression might fail otherwise
    max_timehorizon = min(YEARCOUNTS) - 15

    countries = u_const.L_COUNTRIES

    ds_aff = u_data.load_tce_dat_annual_shares()
    ds_gdppc = u_data.load_hist_gdppc()
    ds_temp = u_data.load_hist_temp() if use_temp else None

    for timehorizon in range(max_timehorizon + 1):
        for period in all_periods(timehorizon):
            minyear, maxyear = period

            fnames = [
                (
                    (f"{'glb' if glb else 'ctry'}_{u_const.INDICATOR}_{minyear}-{maxyear}"
                     f"_{timehorizon}_{'ols' if use_ols else 'glmrob'}"
                     f"{'_temp' if use_temp else ''}.nc"),
                    glb,
                    use_ols,
                )
                for glb in [False, True]
                for use_ols in [False]
            ]
            coeffpaths = [(u_const.NOBOOTSTRAP_DIR / "coefficients" / n) for n, _, _ in fnames]
            if all(p.exists() for p in coeffpaths):
                continue

            ds_fit_growth, ds_fit_aff = load_fit_data(
                minyear, maxyear, timehorizon, countries,
                ds_aff=ds_aff, ds_gdppc=ds_gdppc, ds_temp=ds_temp,
            )

            for coeffpath, (fname, glb, use_ols) in zip(coeffpaths, fnames):
                if coeffpath.exists():
                    continue

                ds = bs_fit.run_regressions(
                    ds_fit_growth, timehorizon, period, glb,
                    use_ols=use_ols, ds_aff=ds_fit_aff, ds_temp=ds_temp,
                )

                if use_ols:
                    ds, res = ds
                    picklepath = coeffpath.parent / fname.replace(".nc", ".pickle")
                    print(f"Writing to {picklepath} ...")
                    with open(picklepath, "wb") as fp:
                        pickle.dump(res, fp)

                print(f"Writing to {coeffpath} ...")
                encoding = {v: {'zlib': True} for v in ds.data_vars}
                ds.to_netcdf(coeffpath, encoding=encoding)


if __name__ == "__main__":
    main()
