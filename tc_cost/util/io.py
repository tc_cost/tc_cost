
import os
import sys

import pandas as pd
import xarray as xr

from tc_cost.util import GracefulInterruptHandler


def path_exists_safely(path):
    """Check if file exists and is not corrupt

    For data that has been written using `write_safely`, this will check whether the file and
    its checksum file exist and whether the checksum matches. If not, the data is considered as
    corrupted and the file and its checksum file are removed (if they exist).

    An empty file (size 0) is considered to be non-existent, even if the checksum file matches.

    Parameters
    ----------
    path : Path
        The path to the file to check.

    Returns
    -------
    bool
    """
    checksum_path = path.parent / f"{path.name}.sha256sum"
    if not path.exists() or path.stat().st_size == 0:
        exists = False
    elif not checksum_path.exists():
        exists = False
    else:
        exit_code = os.system(f"sha256sum --quiet -c {checksum_path}")
        exists = (exit_code == 0)
    if not exists:
        path.unlink(missing_ok=True)
        checksum_path.unlink(missing_ok=True)
    return exists


def write_safely(obj, path, unlink=None):
    """Write NetCDF/CSV data together with checksum file

    If you later find that the file exists, but the checksum file doesn't exist or doesn't match,
    then the data is probably corrupted, e.g. because the process was killed during writing.

    Parameters
    ----------
    obj : pd.DataFrame or xr.Dataset
        The data to store in the given location. A DataFrame is written as CSV,
        a Dataset is written as NetCDF.
    path : Path
        The path to the destination file.
    unlink : list of Path, optional
        If given, remove these (temporary) files after the data has been
        successfully written to the specified location.

    See also
    --------
    path_exists_safely
    """
    with GracefulInterruptHandler() as h:
        print(f"Writing to {path.name} ... ", end="")
        if isinstance(obj, pd.DataFrame):
            obj.to_csv(path, index=None)
        else:
            encoding = {v: {'zlib': True} for v in obj.data_vars}
            obj.to_netcdf(path, encoding=encoding)
        checksum_path = path.parent / f"{path.name}.sha256sum"
        os.system(f"sha256sum {path} > {checksum_path}")
        print("done.")

        if unlink is not None:
            print(f"Deleting temporary files ... ", end="")
            for p in unlink:
                p.unlink(missing_ok=True)
                (p.parent / f"{p.name}.sha256sum").unlink(missing_ok=True)
            print("done.")

        if h.interrupted:
            print("Quitting gracefully after SIGTERM ...")
            sys.exit()
