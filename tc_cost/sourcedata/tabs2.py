
import numpy as np
import pandas as pd

from tc_cost.sourcedata.fig2 import load_dadpc
import tc_cost.util.constants as u_const
import tc_cost.util.data as u_data


def data_tabs2(ssp, rcp, timehorizon, dr_name, disc_growth, use_ols, use_temp):
    l_countries = u_const.L_COUNTRIES
    iso_name = pd.read_csv(u_const.ISO_NAMES_CSV).rename(columns={"iso3": "ISO"})
    dr = u_const.L_DR['ramsey'][dr_name]
    df = (
        load_dadpc(ssp, rcp, timehorizon, dr, disc_growth, use_ols, use_temp)
        .merge(u_data.load_wid_income(l_countries, 2019, "p0p100"), on="ISO", how="left")
        .merge(iso_name, on="ISO", how="left")
    )
    for p in ["50", "17", "83"]:
        df[f"perc_of_ahi{p}"] = 100 * df[f"marginal_dec_pc_{p}"] / df["income_ppp"]
    return df


def tex_tabs2(df):
    table_str = ""
    for i_ctry, ctry in enumerate(df["name"].values):
        iso = df["ISO"].values[i_ctry]
        row_str = f"{ctry} & {iso}"
        income = df["income_ppp"].values[i_ctry]
        row_str += " & --" if np.isnan(income) else f" & {income:.0f} "
        for p in ["50", "17", "83"]:
            val = df[f"marginal_dec_pc_{p}"].values[i_ctry]
            val_rel = df[f"perc_of_ahi{p}"].values[i_ctry]
            val_rel_str = "--" if np.isnan(val_rel) else f"{val_rel:.2f}\\%"
            row_str += f" & {val:.0f} ({val_rel_str})"
        table_str += f"{row_str}\\\\\n"
    return table_str


def main():
    timehorizon = 8
    use_ols = True
    use_temp = True
    disc_growth = "baseline"
    dr_name = "Ricke"
    rcp = "rcp60"
    ssp = "SSP2"

    path = u_const.SOURCEDATA_DIR / "tabs2.csv"
    df = data_tabs2(ssp, rcp, timehorizon, dr_name, disc_growth, use_ols, use_temp)
    print(f"Writing to {path} ...")
    df.to_csv(path, index=None)

    path = u_const.TABLES_DIR / "tabs2.tex"
    s_tex = tex_tabs2(df)
    print(f"Writing to {path} ...")
    with open(path, "w") as fp:
        fp.write(s_tex)


if __name__ == "__main__":
    main()
