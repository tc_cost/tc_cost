
import numpy as np
import pandas as pd
import xarray as xr

import tc_cost.util.constants as u_const


def data_tabs3(timehorizon, use_ols, use_temp):
    l_countries = u_const.L_COUNTRIES
    l_gcm = ["allgcms"]  # alternatively, include GCMs in uncertainty: u_const.L_GCM

    path = u_const.BOOTSTRAP_DIR / "damage_functions"
    print(f"Reading country-level damage functions from {path} ...")

    suffix = f"{'_ols' if use_ols else ''}{'_temp' if use_temp else ''}"
    ds = xr.open_mfdataset([
        [
            path / f"{timehorizon}-{gcm}-{iso}{suffix}.nc"
            for iso in l_countries
        ] for gcm in l_gcm
    ], combine="nested", concat_dim=["gcm", "ISO"]).sel(rcp="__all__").compute()
    ds['ISO'] = ("ISO", l_countries)
    ds['gcm'] = ("gcm", l_gcm)

    qdims = ["realisation", "elt", "gcm"]
    for q in [0.167, 0.833]:
        ds[f"{100 * q:.0f}"] = ds['estimates'].quantile(q=q, dim=qdims)
    ds["mean"] = ds['estimates'].mean(dim=qdims)

    ds = ds[["mean", "17", "83"]]
    df = ds.to_dataframe().drop(columns=["quantile", "rcp"])
    df *= 100

    return (
        df
        .reset_index()
        .merge(
            pd.read_csv(u_const.ISO_NAMES_CSV).rename(columns={"iso3": "ISO"}),
            on="ISO", how="left",
        )
    )


def tex_tabs3(df):
    l_countries = np.unique(df["name"].values)
    df = df.set_index(["name", "coeff"])[["mean", "17", "83"]]
    tex_str = ""
    for name in l_countries:
        tex_str += f"{name}"
        for coeff in ["const", "gmt"]:
            vmean, v17, v83 = df.loc[(name, coeff), :]
            tex_str += f" & {vmean:.3f} & {v17:.3f} & {v83:.3f}"
        interc_mean, interc_17, interc_83 = df.loc[(name, "const"), :]
        tex_str += " \\\\\n"
    return tex_str


def main():
    timehorizon = 8
    use_ols = True
    use_temp = True

    path = u_const.SOURCEDATA_DIR / "tabs3.csv"
    df = data_tabs3(timehorizon, use_ols, use_temp)
    print(f"Writing to {path} ...")
    df.to_csv(path, index=None)

    path = u_const.TABLES_DIR / "tabs3.tex"
    s_tex = tex_tabs3(df)
    print(f"Writing to {path} ...")
    with open(path, "w") as fp:
        fp.write(s_tex)


if __name__ == "__main__":
    main()
