
from tc_cost.sourcedata.fig3 import data_fig3a_markers, data_fig3a_lines
import tc_cost.util.constants as u_const


def main():
    timehorizon = 8
    use_ols = True
    use_temp = True

    l_countries = [
        u_const.L_COUNTRIES[:21],
        u_const.L_COUNTRIES[21:],
    ]

    for i, countries in enumerate(l_countries):
        path = u_const.SOURCEDATA_DIR / f"figs{14 + i}_markers.csv"
        df = data_fig3a_markers(timehorizon, use_ols, use_temp, countries=countries)
        print(f"Writing to {path} ...")
        df.to_csv(path, index=None)

        path = u_const.SOURCEDATA_DIR / f"figs{14 + i}_lines.csv"
        df = data_fig3a_lines(timehorizon, use_ols, use_temp, countries=countries)
        print(f"Writing to {path} ...")
        df.to_csv(path, index=None)


if __name__ == "__main__":
    main()
