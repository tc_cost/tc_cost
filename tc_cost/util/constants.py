
import pathlib

L_SSP = ["SSP2", "SSP5"]

L_RCP =  ["RCP2.6", "RCP6.0", "RCP8.5"]

L_RCP_LOWER = [rcp.lower().replace('.', '') for rcp in L_RCP]

L_GCM = ["GFDL-ESM2M", "HadGEM2-ES", "IPSL-CM5A-LR", "MIROC5"]

L_DR = {
    # Comparison EPA (SC-CO2) [http://www3.epa.gov/climatechange/EPAactivities/economics/scc.html]
    "fixed": {
        # don't use the fixed discounting for now (to safe disk space)
        # "Stern": (0.001, 0),
        # "Nordhaus": (0.015, 0),
    },

    # elasticity of marginal utility of consumption = 1
    # based on Table 3.2 in IPCC AR5 WG2 Chapter 3
    # added 3% rho to be compatible with EPA
    "ramsey": {
        "Stern": (0.001, 1.01),
        "Nordhaus": (0.015, 1.45),
        "Ricke": (0.02, 1.5),
    },
}

L_DR_COLORS = {
    "Stern": "#DC267F",
    "Nordhaus": "#648FFF",
    "Ricke": "#FFB000",
}

L_RCP_COLORS = {
    "all": "#333333",
    "rcp26": "#7570b3",
    "rcp60": "#1b9e77",
    "rcp85": "#d95f02",
    "baseline": "#e377c2",
}

L_COUNTRIES = [
    "AUS", "BGD", "BHS", "BLZ", "BRB", "CAN", "CHN", "CUB", "DOM", "FJI", "GTM", "HKG", "HND",
    "HTI", "IND", "JAM", "JPN", "KHM", "KOR", "LAO", "LCA", "LKA", "MDG", "MEX", "MMR", "MOZ",
    "MUS", "NCL", "NIC", "PHL", "PNG", "PRI", "SLB", "THA", "TON", "TWN", "USA", "VCT", "VNM",
    "VUT", "WSM",
]
"""List of TC-affected countries that we selected for this study."""

MINWIND = 34
INDICATOR = f"{MINWIND}-peop"
"""Damage indicator: share of people affected by winds of at least 34 knots

This used to be a parameter in earlier versions.
Most of the data and code is lost for the alternative indicators, so we make it a constant.
"""

HIST_PERIOD_GDPPC = (1961, 2015)
"""The maximum period covered by the historical GDP per capita data"""

HIST_PERIOD_TC = (1950, 2015)
"""The maximum period covered by TCE-DAT"""

HIST_PERIOD_REF = (1981, 2015)
"""The reference period of historical economic activity.

Please note: Due to the lag structure in the model, TC activity from years preceeding this period
will also be included in the analysis.
"""

MAX_TIMEHORIZON = 15
"""The maximum timehorizon considered in the analysis.

This determines the TC reference period used in the bootstrapping."""

BOOT_NUMBER = 4
BOOT_SIZE = 30
BOOT_REPLICATIONS = 10
"""For historical reasons, the 1200 bootstraps are subdivided into 4*30 chunks of 10 samples"""

SSP_PERIOD = (2010, 2100)
"""The period used when projecting GDP pathways along SSP scenarios"""

PULSE_YEAR = 2025
"""Year of addition CO2-pulse for SCC computation"""

VERY_LAST_YEAR = 2200
"""Last year after 2100 to consider in SCC computation"""

PULSE_SCALE = 1e9 * 44 / 12
"""multiplication converts GtC to tCO2"""

BASE_DIR = pathlib.Path(__file__).resolve().parent.parent.parent
"""The location of the git repository's root"""

RICKE_DATA_DIR = BASE_DIR / "cscc-paper-2018" / "data"

DATA_DIR = BASE_DIR / "data"

CACHE_DIR = DATA_DIR / "cache"

INPUT_DIR = DATA_DIR / "input"

PLOTS_DIR = DATA_DIR / "plots"

TABLES_DIR = DATA_DIR / "tables"

SOURCEDATA_DIR = DATA_DIR / "sourcedata"

BOOTSTRAP_DIR = DATA_DIR / "bootstrap"

NOBOOTSTRAP_DIR = DATA_DIR / "nobootstrap"

HIST_GDP_CSV = INPUT_DIR / "IHME_GLOBAL_GDP_ESTIMATES_1950_2015.csv"
IHME_GDPPC_COL = " IHME GDP estimate (2005 international dollars) "
HIST_GDP_NC = CACHE_DIR / "IHME_GLOBAL_GDP_ESTIMATES_1950_2015.nc"
"""Historical GDP per capita data from James et al. 2012 (https://doi.org/10.1186/1478-7954-10-12)

Downloaded from:
https://ghdx.healthdata.org/record/ihme-data/gross-domestic-product-gdp-estimates-country-1950-2015
"""

GMT_NC = INPUT_DIR / "GMT.nc"
"""The GMT time series are extracted by the emulator scripts"""

ISO_NAMES_CSV = INPUT_DIR / "iso_names.csv"
"""Mapping from ISO 3166 alpha3 codes to human readable country names"""

ISO_NUMERIC_CSV = INPUT_DIR / "iso_numeric.csv"
"""Mapping from ISO 3166 alpha3 codes to numeric values"""

SSP_FILE_CSV = RICKE_DATA_DIR / "SspDb_country_data_2013-06-12.csv"
SSP_FILE_NC = CACHE_DIR / "SspDb_GDP_POP_OECD_Env-Growth.nc"
"""The SSP csv-file from the Ricke repository"""

PULSE_RESPONSE_CSV = INPUT_DIR / "pulse_response_ricke_caldeira_2014.csv"
PULSE_RESPONSE_CMIP5_MODELS = [
    "BCC-CSM1.1", "BCC-CSM1.1(m)", "CanESM2", "CSIRO-Mk3.6.0", "FGOALS-g2", "FGOALS-s2",
    "GFDL-CM3", "GFDL-ESM2G", "GFDL-ESM2M", "INM-CM4", "IPSL-CM5A-LR", "IPSL-CM5A-MR",
    "IPSL-CM5B-LR", "MIROC5", "MIROC-ESM", "MPI-ESM-LR", "MPI-ESM-MR", "MPI-ESM-P", "MRI-CGCM3",
    "NorESM1-M",
]
PULSE_RESPONSE_CARB_CYCLE_MODELS = [
    "NCAR CSM1.4", "HadGEM2-ES", "MPI-ESM", "Bern3D-LPJ (ensemble median)",
    "Bern2.5D-LPJ", "CLIMBER2-LPJ", "DCESS", "GENIE (ensemble median)", "LOVECLIM",
    "MESMO", "UVic2.9", "ACC2", "Bern-SAR", "MAGICC6 (ensemble median)", "TOTEM2",
]
"""The data behind Figure 1 in Ricke & Caldeira 2014"""

COUNTRY_GROUP_CSV = INPUT_DIR / "wb_income_classes_fy2024.csv"
"""The World Bank classification of economies by GNI per capita for the fiscal year 2024:
https://datahelpdesk.worldbank.org/knowledgebase/articles/906519"""

COUNTRY_GROUP_ORDER = ["H", "UM", "LM", "L"]

TCDATA_DIR = INPUT_DIR / "tcdata"

TCE_DAT_FILE = TCDATA_DIR / "TCE-DAT_historic-exposure_1950-2015.csv"
TCE_DAT_INDICATOR_COL = f"{MINWIND}kn_pop"

TC_EMULATOR_DIR = TCDATA_DIR / "emulator"

WID_ALL_DATA = INPUT_DIR / "wid_all_data.zip"
"""Bulk data, as downloaded from the World Inequality Database:
https://wid.world/bulk_download/wid_all_data.zip"""

WID_INCOME_DIR = CACHE_DIR / "aptinc992j_ppp"
WID_INCOME_CSV = CACHE_DIR / "wid_income_data.csv"

WID_VARIABLES = {
    # average pre-tax national income (992: adults) (j: equal-split)
    "income_lc": ["aptinc992j", "aptincj992"],
    # PPP conversion factor (LCU per USD)
    "ppp": ["xlcusp999i", "xlcuspi999"],
}

TAB20_COLORS = {
    "black": "#333333",
    "yellow": "#f3f781",
    "blue": "#1f77b4",
    "orange": "#ff7f0e",
    "green": "#2ca02c",
    "red": "#d62728",
    "purple": "#9467bd",
    "brown": "#8c564b",
    "magenta": "#e377c2",
    "gray": "#7f7f7f",
    "grey": "#7f7f7f",
    "lime": "#bcbd22",
    "cyan": "#17becf",
    "lightblue": "#aec7e8",
    "lightorange": "#ffbb78",
    "lightgreen": "#98df8a",
    "lightred": "#ff9896",
    "lightpurple": "#c5b0d5",
    "lightbrown": "#c49c94",
    "lightmagenta": "#f7b6d2",
    "lightgray": "#c7c7c7",
    "lightgrey": "#c7c7c7",
    "lightlime": "#dbdb8d",
    "lightcyan": "#9edae5",
}

CM_TO_INCH = 1.0 / 2.54

PLOT_WIDTH_IN = 19 * CM_TO_INCH
"""The plot width in inches: 9cm (single column), 14cm (1.5 column), 19cm (double column)"""
