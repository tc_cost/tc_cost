
import matplotlib.gridspec as mgridspec
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import numpy as np
import pandas as pd

import tc_cost.util.constants as u_const
import tc_cost.util.data as u_data
import tc_cost.util.plot as u_plot
from tc_cost.plot.fig1 import plot_impact_cum, plot_impact_hist
from tc_cost.plot.fig2 import (
    plot_cdadpc_boxes,
    plot_cdadpc_to_income,
    plot_gdad_boxes,
    plot_uncertainty_dimensions,
)
from tc_cost.plot.fig3 import plot_damfun, plot_damfun_standalone, plot_damfun_slopes
from tc_cost.plot.fig4 import plot_gscc_dots, plot_cscc_dots
from tc_cost.plot.figs01 import plot_tdf_comparison
from tc_cost.plot.figs04 import plot_aff_projection, plot_aff_regression, plot_gdppc_projection
from tc_cost.sourcedata.fig1 import data_fig1b
from tc_cost.sourcedata.fig2 import data_fig2a_lower, data_fig2a_upper


def plot_timeseries(outer, country, aff=False):
    ds_all = u_data.load_hist_gdppc()[["gdppc_ch"]]
    ds_all["temp"] = u_data.load_hist_temp()["temp"]

    if aff:
        ds_all["aff"] = u_data.load_tce_dat_annual_shares()["affected"]

    ds_all = ds_all.sel(year=(ds_all["year"] >= 1981) & (ds_all["year"] <= 2015))

    linewidth = 0.3 if country is None else 1.0
    color = (0, 0, 0, 0.5) if country is None else "black"
    l_country = ds_all["ISO"].values if country is None else [country]

    nrows = 3 if aff else 2
    ncols = 1
    gs = mgridspec.GridSpecFromSubplotSpec(nrows, ncols, subplot_spec=outer, hspace=0.5)
    fig = plt.gcf()

    axs = [fig.add_subplot(gs[0, 0])]
    axs += [fig.add_subplot(gs[i, 0], sharex=axs[0]) for i in range(1, nrows)]

    ax = axs[0]
    ax.axhline(0.0, linestyle=":", linewidth=0.8, color="grey")
    ax.set_title("GDP growth")
    plt.setp(ax.get_xticklabels(), visible=False)

    ax = axs[1]
    ax.set_title("Mean temperature (°C)")

    if aff:
        plt.setp(ax.get_xticklabels(), visible=False)
        ax = axs[2]
        ax.yaxis.set_major_formatter(mticker.PercentFormatter(xmax=1.0, decimals=0))
        ax.set_title("Share of population affected by TCs (%)")
    ax.set_xlabel("Year")

    vnames = ["gdppc_ch", "temp"]
    if aff:
        vnames += ["aff"]

    for ctry in l_country:
        ds = ds_all.sel(ISO=ctry)

        for ax, vname in zip(axs, vnames):
            ax.plot(ds["year"], ds[vname], color=color, linewidth=linewidth)


def plot_damage_functions(outer, tc=False):
    df = pd.read_csv(u_const.SOURCEDATA_DIR / "figs01.csv")
    df = df[df["data"] == "own"]
    df = df[df["period"] == "1981-2015"]

    if tc:
        df = df[df["tc_effects"] & df["temp_effects"]]
    else:
        df = df[~df["tc_effects"] & df["temp_effects"]]

    df_temp = df[df["variable"] == "temp"].copy()
    df_tc = df[df["variable"] == "tc"].copy()

    fig = plt.gcf()
    inner = mgridspec.GridSpecFromSubplotSpec(1, 2, subplot_spec=outer)

    ax = fig.add_subplot(inner[0])
    ax.plot(df_temp["x"].values, df_temp["y_mean"].values, color="tab:blue")
    ax.fill_between(
        df_temp["x"].values, df_temp["y_5"].values, df_temp["y_95"].values,
        facecolor="tab:blue",
        alpha=0.1,
    )
    ax.set_title("Temperature-growth relationship")
    ax.set_ylabel("Δ GDP growth")
    ax.set_yticks([-0.1, 0.0, 0.1])
    ax.set_xlabel("Mean temperature (°C)")
    ax.set_xticks([0, 5, 10, 15, 20, 25, 30])
    ax.set_xlim(-3, 30.5)
    ax.spines[['left', 'right', 'top']].set_visible(False)
    ax.spines['bottom'].set_bounds([0, 30])

    if tc:
        ax = fig.add_subplot(inner[1])
        ax.set_title("TC-growth relationship")
        ax.plot(df_tc["x"].values, df_tc["y_mean"].values, color="tab:orange")
        ax.fill_between(
            df_tc["x"].values, df_tc["y_5"].values, df_tc["y_95"].values,
            facecolor="tab:orange",
            alpha=0.1,
        )
        ax.axhline(0, color="black", linestyle=":", linewidth=0.5, zorder=20)
        ax.yaxis.set_label_position("right")
        ax.yaxis.tick_right()
        ax.set_ylabel("Δ GDP growth")
        ax.set_yticks([0.0, -0.01, -0.02])
        ax.set_xlabel("Lag year")
        pred_vals = df_tc["x"].values
        ax.set_xticks(pred_vals)
        ax.set_xlim(pred_vals[0], pred_vals[-1])
        ax.spines[['left', 'right', 'top']].set_visible(False)


def plot_regression():
    configs = [
        ("USA", False, "no"),
        (None, False, "temp"),
        ("USA", True, "no"),
        (None, True, "both"),
    ]
    for ctry, aff, damfun in configs:
        fig = plt.figure(figsize=(9.0, 5.5))
        outer = mgridspec.GridSpec(2, 1, hspace=0.4, height_ratios=[9 if aff else 5, 4])

        plot_timeseries(outer[0], ctry, aff=aff)

        if damfun != "no":
            plot_damage_functions(outer[1], tc=damfun != "temp")

        outpath = u_const.PLOTS_DIR / "extra" / (
            f"gdp_vs_temp"
            f"-ctry_{'all' if ctry is None else ctry}"
            f"-aff_{'yes' if aff else 'no'}"
            f"-df_{damfun}"
            ".pdf"
        )
        print(f"Writing to {outpath} ...")
        fig.savefig(outpath, bbox_inches="tight")


def plot_cscc_boxes():
    fig = plt.figure(figsize=(9.0, 3.8))
    ax = fig.add_subplot(1, 1, 1)

    df = pd.read_csv(u_const.SOURCEDATA_DIR / "fig4b.csv")

    def_dr_name = "Ricke"
    def_dr = u_const.L_DR['ramsey'][def_dr_name]
    def_dr_color = u_const.L_DR_COLORS[def_dr_name]
    iso_name = pd.read_csv(u_const.ISO_NAMES_CSV).set_index("iso3")["name"].to_dict()
    l_countries = df["ISO"].values
    l_ctry_names = [iso_name[i] for i in l_countries]

    y_countries = np.arange(len(l_countries))[::-1]
    box_width = 0.4
    for suffix in ["", "_temp"]:
        linewidth = 1.2 if suffix == "" else 0.3
        df_50 = df[f"scc{suffix}50"].values
        df_83 = df[f"scc{suffix}83"].values
        df_17 = df[f"scc{suffix}17"].values
        for i, pos in enumerate(y_countries + (1 if suffix == "" else -1) * 0.23):
            ax.add_patch(
                mpatches.Rectangle(
                    (df_17[i], pos - 0.5 * box_width),
                    df_83[i] - df_17[i],
                    box_width,
                    fc=def_dr_color,
                    ec="black",
                    linewidth=linewidth,
                    zorder=2,
                )
            )
            ax.plot(
                [df_50[i], df_50[i]],
                [pos - 0.5 * box_width, pos + 0.5 * box_width],
                linewidth=linewidth,
                color="black",
                zorder=3,
            )
    for y in y_countries:
        ax.axhspan(
            y - 0.5, y + 0.5,
            color="#f0f0f0",
            alpha=0.7 if y % 2 == 0 else 0.3,
            linewidth=0,
            zorder=1,
        )
    ax.axvline(0, lw=0.5, ls=":", c="silver")
    ax.set_ylim(min(y_countries) - 0.5, max(y_countries) + 0.5)
    ax.set_yticks(y_countries[::-1])
    ax.set_yticklabels(l_ctry_names[::-1])
    ax.set_xlabel("Country-level SCC (US$/tCO2)")
    xlim = ax.get_xlim()
    ax.set_xlim(min(0, xlim[0]), xlim[1])

    outpath = u_const.PLOTS_DIR / "extra" / "scc-increase.pdf"
    print(f"Writing to {outpath} ...")
    fig.savefig(outpath, bbox_inches="tight")


def main_setup_mpl_large():
    u_plot.main_setup_mpl()
    plt.rcParams.update(
        {
            "font.size": 9,
            "legend.fontsize": 9,
            "xtick.labelsize": 8,
            "ytick.labelsize": 8,
            "axes.labelsize": 8,
            "axes.titlesize": 8,
        }
    )


def plot_fig1b_large():
    main_setup_mpl_large()
    figsize = (0.4 * u_const.PLOT_WIDTH_IN, 1.8)

    # extra args only needed for timehorizon != 8
    timehorizon = 8
    glb = True
    nobootstrap = False
    use_ols = True
    use_temp = True
    average_annual_aff = False

    for only_usa in [True, False]:
        fig = plt.figure(figsize=figsize)
        gs = mgridspec.GridSpec(1, 1)
        ax = fig.add_subplot(gs[0, 0])
        data = (
            pd.read_csv(u_const.SOURCEDATA_DIR / "fig1b.csv")
            if timehorizon == 8 else
            data_fig1b(timehorizon, glb, nobootstrap, use_ols, use_temp)
        )
        if only_usa:
            data = data[data["ISO"].isin(["GLB", "USA"])]
        plot_impact_cum(ax, data, ylabelsize=8, tick_right=False)

        suffix = "_onlyusa" if only_usa else ""
        outpath = u_const.PLOTS_DIR / "extra" / f"fig1b_large_{timehorizon}lags{suffix}.pdf"
        print(f"Writing to {outpath} ...")
        fig.savefig(outpath, bbox_inches="tight")


def plot_fig1c_large():
    main_setup_mpl_large()
    figsize = (0.95 * u_const.PLOT_WIDTH_IN, 1.8)
    fig = plt.figure(figsize=figsize)
    gs = mgridspec.GridSpec(1, 1)

    data = pd.read_csv(u_const.SOURCEDATA_DIR / "fig1c.csv")
    ax = fig.add_subplot(gs[0, 0])
    plot_impact_hist(ax, data, lims=((-0.105, 0.055), (0, 250.0)))

    outpath = u_const.PLOTS_DIR / "extra" / f"fig1c_large.pdf"
    print(f"Writing to {outpath} ...")
    fig.savefig(outpath, bbox_inches="tight")


def plot_fig2a_large():
    main_setup_mpl_large()
    enlarge_factor = 0.95

    timehorizon = 8
    use_ols = True
    use_temp = True
    disc_growth = "baseline"
    dr_name = "Ricke"
    rcp = "rcp60"

    args = (timehorizon, disc_growth, use_ols, use_temp)
    dr_args = (rcp, timehorizon, dr_name, disc_growth, use_ols, use_temp)

    for ssp in ["SSP2", "SSP5"]:
        fig = plt.figure(figsize=(0.5 * u_const.PLOT_WIDTH_IN * enlarge_factor, 4.0 * enlarge_factor))
        gs = mgridspec.GridSpec(2, 1, height_ratios=[11, 10], hspace=0.17)

        data = data_fig2a_upper(*args, ssp)
        plot_gdad_boxes(gs[0], data)

        data = data_fig2a_lower(ssp, *dr_args)
        plot_uncertainty_dimensions(gs[1], data)

        path = u_const.PLOTS_DIR / "extra" / f"fig2a_large_{ssp.lower()}.pdf"
        print(f"Writing to {path} ...")
        fig.savefig(path, bbox_inches="tight")


def plot_fig2bc_large():
    main_setup_mpl_large()
    enlarge_factor = 0.75
    fig = plt.figure(figsize=(u_const.PLOT_WIDTH_IN * enlarge_factor, 8.5 * enlarge_factor))
    outer = mgridspec.GridSpec(2, 1, hspace=0.33, height_ratios=[9, 10])

    ax = fig.add_subplot(outer[0])
    data = pd.read_csv(u_const.SOURCEDATA_DIR / "fig2b.csv")
    plot_cdadpc_boxes(ax, data)

    ax = fig.add_subplot(outer[1])
    data = pd.read_csv(u_const.SOURCEDATA_DIR / "fig2c.csv")
    plot_cdadpc_to_income(ax, data, annotate_kwargs=dict(fontsize=7), ypad=0.2, med_fontsize=6)

    path = u_const.PLOTS_DIR / "extra" / "fig2bc_large.pdf"
    print(f"Writing to {path} ...")
    fig.savefig(path, bbox_inches="tight")


def plot_fig4ab_large():
    main_setup_mpl_large()
    enlarge_factor = 0.9
    fig = plt.figure(figsize=(u_const.PLOT_WIDTH_IN * enlarge_factor, 5.0 * enlarge_factor))
    outer = mgridspec.GridSpec(3, 1, hspace=0.1, height_ratios=[10, 2.0, 8])

    data = pd.read_csv(u_const.SOURCEDATA_DIR / "fig4a.csv")
    plot_gscc_dots(outer[0], data)

    ax = fig.add_subplot(outer[0], frameon=False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.text(-0.055, 0.97, "a", va="bottom", ha="right", fontweight="bold", fontsize=11,
            transform=ax.transAxes)

    data = pd.read_csv(u_const.SOURCEDATA_DIR / "fig4b.csv")
    plot_cscc_dots(outer[2], data)

    ax = fig.add_subplot(outer[2], frameon=False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.text(-0.055, 1.03, "b", va="bottom", ha="right", fontweight="bold", fontsize=11,
            transform=ax.transAxes)

    path = u_const.PLOTS_DIR / "extra" / f"fig4ab_large.pdf"
    print(f"Writing to {path} ...")
    fig.savefig(path, bbox_inches="tight")


def plot_fig3ab_large():
    main_setup_mpl_large()
    enlarge_factor = 0.9
    fig = plt.figure(figsize=(u_const.PLOT_WIDTH_IN * enlarge_factor, 5.55 * enlarge_factor))
    outer = mgridspec.GridSpec(2, 1, hspace=0.26, height_ratios=[1, 1.3])
    inner = mgridspec.GridSpecFromSubplotSpec(
        1, 2, subplot_spec=outer[0], wspace=0, width_ratios=[1, 13])

    ax = fig.add_subplot(inner[1])
    df_ell = pd.read_csv(u_const.SOURCEDATA_DIR / "fig3a_ellipses.csv")
    df_mrk = pd.read_csv(u_const.SOURCEDATA_DIR / "fig3a_markers.csv")
    df_fun = pd.read_csv(u_const.SOURCEDATA_DIR / "fig3a_lines.csv")
    plot_damfun_standalone(ax, df_ell, df_mrk, df_fun, panel="a")

    data = pd.read_csv(u_const.SOURCEDATA_DIR / "fig3b.csv")
    plot_damfun_slopes(outer[1], data, fontsize=7)

    ax = fig.add_subplot(outer[1], frameon=False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.text(-0.01, 1.01, "b", va="bottom", ha="right", fontweight="bold", fontsize=10,
            transform=ax.transAxes)

    path = u_const.PLOTS_DIR / "extra" / f"fig3_large.pdf"
    print(f"Writing to {path} ...")
    fig.savefig(path, bbox_inches="tight")


def plot_figs01_extra_sub(labels, suffix):
    main_setup_mpl_large()
    plt.rcParams.update(
        {
            "legend.fontsize": 8,
            "xtick.labelsize": 9,
            "ytick.labelsize": 9,
            "axes.labelsize": 9,
            "axes.titlesize": 9,
        }
    )
    enlarge_factor = 0.8
    fig = plt.figure(figsize=(u_const.PLOT_WIDTH_IN * enlarge_factor, 8 * enlarge_factor))
    gs = mgridspec.GridSpec(1, 1)

    colors = {
        "own_True_True 1981-2015": "tab:green",
        "own_True_False 1981-2015": "tab:green",
        "own_False_True 1981-2015": "tab:purple",
    }

    data = pd.read_csv(u_const.SOURCEDATA_DIR / "figs01.csv")
    data["data"] = data["data"].str.replace("Burke et al. 2015", "bhm")
    data["key"] = (
        data["data"]
        + "_" + data["temp_effects"].astype(str)
        + "_" + data["tc_effects"].astype(str)
        + " " + data["period"]
    )

    pred_vals = [
        np.unique(data.loc[data["variable"] == "temp", "x"].values),
        np.unique(data.loc[data["variable"] == "tc", "x"].values),
    ]

    l_plotdata = []
    for key, label in labels.items():
        df = data[data["key"] == key].sort_values(by="x", ascending=True)
        notc = not df["tc_effects"].values[0]
        notemp = not df["temp_effects"].values[0]
        color = colors[key]
        plot_kwargs = dict(
            lw=(
                2 if notc
                else 1 if notemp
                else 0.5
            ),
            ls="--" if notemp else ":" if notc else None,
        )
        pred = {}
        for v in ["temp", "tc"]:
            var_mask = (df["variable"] == v)
            skip = (v == "temp" and notemp) or (v == "tc" and notc)
            pred[v] = (None, None) if skip else (
                df.loc[var_mask, "y_mean"].values,
                np.stack([
                    df.loc[var_mask, "y_5"].values,
                    df.loc[var_mask, "y_95"].values,
                ], axis=1),
            )
        l_plotdata.append((
            (label, color, plot_kwargs),
            pred["temp"], pred["tc"], None,
        ))

    l_plotdata = {"": l_plotdata}

    plot_tdf_comparison(gs[0], pred_vals, l_plotdata, legend_ncol=1)

    outpath = u_const.PLOTS_DIR / "extra" / f"figs01_large{suffix}.pdf"
    print(f"Writing to {outpath} ...")
    fig.savefig(outpath, bbox_inches="tight")


def plot_figs01_extra():
    labels = {
        "own_True_True 1981-2015": "Our main specification (1981-2015)",
        "own_True_False 1981-2015": "Our main specification w/o TC-effects (1981-2015)",
        "own_False_True 1981-2015": "Our main specification w/o temp. effects (1981-2015)",
    }
    plot_figs01_extra_sub({k: labels[k] for k in ["own_True_True 1981-2015"]}, "_a")
    plot_figs01_extra_sub(labels, "_b")


def plot_figs04ae_large():
    main_setup_mpl_large()
    enlarge_factor = 0.9
    for i_ssp, ssp in enumerate(["SSP2", "SSP5"]):
        fig = plt.figure(figsize=(enlarge_factor * u_const.PLOT_WIDTH_IN, enlarge_factor * 1.9))

        gs = mgridspec.GridSpec(1, 2, wspace=0.04)
        axs = [fig.add_subplot(gs[0, 0]), fig.add_subplot(gs[0, 1])]

        data = pd.read_csv(u_const.SOURCEDATA_DIR / f"figs04{'ab'[i_ssp]}.csv")
        plot_aff_projection(axs[0], data, break_ylabel=False)

        ax = axs[1]
        data = pd.read_csv(u_const.SOURCEDATA_DIR / f"figs04{'ef'[i_ssp]}.csv")
        plot_gdppc_projection(
            ax, data, xmax=2125, y_offset=(0.86, 0.06),
            yticks=np.arange(6, 20, 2) if ssp == "SSP5" else np.arange(4, 11, 1)
        )
        ax.legend(
            bbox_to_anchor=(0.0, 1.0),
            loc="lower center",
            ncol=5,
            frameon=False,
        )
        ax.yaxis.tick_right()
        ax.yaxis.set_label_position("right")

        outpath = u_const.PLOTS_DIR / "extra" / f"figs04ae_{ssp.lower()}_large.pdf"
        print(f"Writing to {outpath} ...")
        fig.savefig(outpath, bbox_inches="tight")


def plot_figs04cd_large():
    main_setup_mpl_large()
    enlarge_factor = 0.9
    for i_ssp, ssp in enumerate(["SSP2", "SSP5"]):
        fig = plt.figure(figsize=(enlarge_factor * u_const.PLOT_WIDTH_IN, enlarge_factor * 2.3))
        gs = mgridspec.GridSpec(1, 2, wspace=0.04)
        axs = [fig.add_subplot(gs[0, 0]), fig.add_subplot(gs[0, 1])]

        ax = axs[0]
        data = pd.read_csv(u_const.SOURCEDATA_DIR / f"figs04{'cd'[i_ssp]}.csv")
        plot_aff_regression(ax, data, break_ylabel=False)
        ax.legend(
            loc="lower right",
            ncol=1,
            frameon=False,
            handletextpad=0.4,
        )

        ax = axs[1]
        iso = "USA"
        df_mrk = pd.read_csv(u_const.SOURCEDATA_DIR / f"figs15_markers.csv")
        df_fun = pd.read_csv(u_const.SOURCEDATA_DIR / f"figs15_lines.csv")
        df_mrk_iso = df_mrk[df_mrk["ISO"] == iso]
        df_fun_iso = df_fun[df_fun["ISO"] == iso]
        rcp_colors = plot_damfun(ax, None, df_mrk_iso, df_fun_iso)
        ax.yaxis.tick_right()
        ax.yaxis.set_label_position("right")
        ax.set_ylabel("TC-induced growth changes\n(percentage point)")
        ax.set_xlabel("Global mean temperature change (°C)")
        ax.set_xlim(*axs[0].get_xlim())

        outpath = u_const.PLOTS_DIR / "extra" / f"figs04cd_{ssp.lower()}_large.pdf"
        print(f"Writing to {outpath} ...")
        fig.savefig(outpath, bbox_inches="tight")


def plot_figs05_large():
    main_setup_mpl_large()

    l_countries = ["USA", "JPN", "PHL"]
    data = pd.concat([
        pd.read_csv(u_const.SOURCEDATA_DIR / f"figs{fig_no:02d}.csv")
        for fig_no in range(5, 12)
    ])
    data = data[data["ISO"].isin(l_countries)]

    enlarge_factor = 0.8
    fig = plt.figure(figsize=(
        enlarge_factor * u_const.PLOT_WIDTH_IN,
        enlarge_factor * 1.3 * len(l_countries),
    ))
    gs = mgridspec.GridSpec(len(l_countries), 1, hspace=0.3)

    axes = [fig.add_subplot(gs[0])]
    axes += [
        fig.add_subplot(gs[i + 1], sharex=axes[0])
        for i, _ in enumerate(l_countries[1:])
    ]

    ax_legend = axes[0].inset_axes([0.0, 0.2, 0.75, 0.8], frameon=False)
    ax_legend.set_xticks([])
    ax_legend.set_yticks([])

    for i, c in enumerate(l_countries):
        df = data[data["ISO"] == c].copy()
        ymax = 1.33 * df.loc[df["variable"] != "median", "value"].max()
        ax = axes[i]
        decorate = c == l_countries[-1]
        plot_impact_hist(
            ax, df,
            decorate=decorate,
            legend=(i == 0),
            ax_legend=ax_legend,
            lims=((-0.147, 0.007), (0, ymax)),
        )

    outpath = u_const.PLOTS_DIR / "extra" / f"figs05_large.pdf"
    print(f"Writing to {outpath} ...")
    fig.savefig(outpath, bbox_inches="tight")



def main():
    plot_fig1b_large()
    plot_fig1c_large()
    plot_fig2a_large()
    plot_fig2bc_large()
    plot_fig3ab_large()
    plot_fig4ab_large()
    plot_figs01_extra()
    plot_figs04ae_large()
    plot_figs04cd_large()
    plot_figs05_large()
    plot_regression()
    plot_cscc_boxes()


if __name__ == "__main__":
    main()
