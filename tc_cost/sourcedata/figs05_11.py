
import numpy as np
import pandas as pd

from tc_cost.sourcedata.fig1 import get_data_cum
import tc_cost.util.constants as u_const


def data_figs05_11(beta_ctry, bin_min, bin_max):
    beta_ctry = (
        beta_ctry
        .rename(persistence="lag")
        .rename_vars(coeff="value")
    )
    max_timehorizon = beta_ctry["lag"].values.max()
    bins = np.linspace(bin_min, bin_max, 50)
    hist_timehorizons = [2, 5, 8, 12, 15]

    medians = beta_ctry["value"].median(dim="boot").to_dataframe().reset_index()
    medians["variable"] = "median"

    hists = []
    for iso in beta_ctry["ISO"].values:
        for l in hist_timehorizons:
            hist = np.histogram(
                beta_ctry.sel(lag=l, ISO=iso)["value"].values,
                bins=bins,
                density=True,
            )[0]
            hist = pd.DataFrame({"lag": l, "ISO": iso, "variable": bins[:-1], "value": hist})
            hist["variable"] = (
                "bin_"
                + hist.index.to_series().apply(lambda v: f"{v:03d}_")
                + hist["variable"].astype(str)
            )
            hists.append(hist)

    return pd.concat(hists + [medians]).sort_values(by=["ISO", "lag", "variable"])


def main():
    timehorizon = 8
    glb = True
    nobootstrap = False
    use_ols = True
    use_temp = True
    average_annual_aff = True

    l_countries = u_const.L_COUNTRIES
    beta_ctry = get_data_cum(15, glb, nobootstrap, average_annual_aff, use_ols, use_temp)
    beta_ctry = beta_ctry.sel(ISO=l_countries)

    l_countries = (
        beta_ctry["coeff"].max(dim=["persistence", "boot"])
        - beta_ctry["coeff"].min(dim=["persistence", "boot"])
    ).to_series().sort_values(ascending=False).index

    countries_per_plot = {
        "5": (l_countries[0:5], -0.160, 0.005),
        "6": (l_countries[5:11], -0.054, 0.003),
        "7": (l_countries[11:17], -0.037, 0.0025),
        "8": (l_countries[17:23], -0.032, 0.002),
        "9": (l_countries[23:29], -0.019, 0.001),
        "10": (l_countries[29:35], -0.008, 0.0003),
        "11": (l_countries[35:], -0.004, 0.0001),
    }

    for fig_no, (countries, bin_min, bin_max) in countries_per_plot.items():
        ds = beta_ctry.sel(ISO=countries)
        path = u_const.SOURCEDATA_DIR / f"figs{int(fig_no):02d}.csv"
        df = data_figs05_11(ds, bin_min, bin_max)
        print(f"Writing to {path} ...")
        df.to_csv(path, index=None)


if __name__ == "__main__":
    main()
