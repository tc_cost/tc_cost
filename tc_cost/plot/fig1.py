
import cartopy.crs as ccrs
import cartopy.io.shapereader as shpreader
import geopandas as gpd
import matplotlib.collections as mcollections
import matplotlib.lines as mlines
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import matplotlib.text as mtext
import numpy as np
import pandas as pd

import tc_cost.util.constants as u_const
import tc_cost.util.plot as u_plot


def plot_map_countries(ax, df):
    data_crs = ccrs.PlateCarree()

    world = gpd.read_file(shpreader.natural_earth(
        resolution='110m', category='cultural', name='admin_0_countries',
    ))
    world = world[world["CONTINENT"] != "Antarctica"].copy()
    world = world.rename(columns={"ISO_A3": "ISO"})
    world = world.merge(df, on="ISO", how="left").reset_index(drop=True)
    world["affected"] = world["affected"].fillna(False)
    invalid_mask = ~world['affected']

    geoms = [
        (world.geometry[invalid_mask].unary_union, "#aaaaaa"),
        (world.geometry[~invalid_mask].unary_union, "salmon"),
    ]
    kwargs = dict(crs=data_crs, linewidth=0, zorder=10)
    for geom, color in geoms:
        ax.add_geometries(geom, color=color, **kwargs)

    bounds = world.total_bounds
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_xlim(-160, 130)
    ax.set_ylim(bounds[1], bounds[3])
    ax.add_patch(
        mpatches.Rectangle(
            (0, 0), 1, 1,
            facecolor="#eeeeee",
            edgecolor="none", lw=0,
            transform=ax.transAxes, zorder=1,
        )
    )

    u_plot.map_add_gridlines(
        ax, [90., 180., -90.], [0.],
        (25, 315) + ax.get_extent()[2:],
        data_crs,
    )


def plot_impact_cum(ax, df, ylabelsize=6, tick_right=True):
    timehorizon = df["lag"].max()
    x = np.arange(0, timehorizon + 1)

    names_colors = {
        "GLB": ("Global", "gray"),
        "USA": ("USA", "blue"),
        "JPN": ("Japan", "lightsalmon"),
        "PHL": ("Philippines", "lightblue"),
    }

    # order the list of countries according to `names_colors`
    l_iso = np.unique(df["ISO"].values)
    assert all(c in names_colors for c in l_iso)
    l_iso = [c for c in names_colors.keys() if c in l_iso]

    for iso in l_iso:
        ctry_name, cl = names_colors[iso]
        df_ctry = df[df["ISO"] == iso].sort_values(by="lag", ascending=True)

        ax.plot(x, df_ctry["med"], c=cl, lw=1.0, label=f"{ctry_name}")
        ax.fill_between(
            x, df_ctry["low"], df_ctry["high"],
            facecolor=cl, alpha=0.3, edgecolor="none",
        )

    ax.hlines(0, -1, timehorizon + 1, color="black", linestyle=":", linewidth=0.5, zorder=20)
    ax.set_xlim(0, timehorizon)
    ax.set_xlabel("Lag years")
    ax.set_ylabel(
        f"Average of lag-cumulative\nimpacts on growth (%)",
        fontsize=ylabelsize,
    )
    if tick_right:
        ax.yaxis.tick_right()
        ax.yaxis.set_label_position("right")
        ax.spines['left'].set_visible(False)
        ax.spines['top'].set_visible(False)
    ax.legend(loc="lower left", ncol=1, frameon=False)


class MedianPointHandler:
    def legend_artist(self, legend, orig_handle, fontsize, handlebox):
        x0, y0 = handlebox.xdescent, handlebox.ydescent
        width, height = handlebox.width, handlebox.height
        radius = 4
        patch = mpatches.Circle([x0 + 0.5 * width, y0 + 1.4 * radius], radius=radius,
                                facecolor=orig_handle.get_facecolor()[0],
                                edgecolor=orig_handle.get_edgecolor()[0],
                                lw=orig_handle.get_linewidth()[0],
                                transform=handlebox.get_transform())
        handlebox.add_artist(patch)
        patch = mtext.Text(x0 + 0.5 * width - 0.7 * radius, y0 + 0.8 * radius, 'n',
                           fontsize=7, transform=handlebox.get_transform())
        handlebox.add_artist(patch)
        patch = mlines.Line2D([x0 + 0.5 * width, x0 + 0.5 * width],
                              [y0 + 0.4 * radius, y0 - 1.6 * radius],
                              ls="dashed", c="k", lw=0.3, transform=handlebox.get_transform())
        handlebox.add_artist(patch)
        return patch


def plot_impact_hist(ax, df, decorate=True, legend=True, ax_legend=None, ymax=None, lims=None):
    country = df["ISO"].values[0] if "ISO" in df.columns else None
    if country is not None:
        iso_name = pd.read_csv(u_const.ISO_NAMES_CSV)
        name_ctry = iso_name[iso_name.iso3 == country].values[0][1]

    colors = [
        "red",
        "blue",
        "orange",
        "purple",
        "yellow",
        "maroon",
        "salmon",
    ]
    alpha = 0.7

    bins = df.loc[df["variable"].str.startswith("bin"), "variable"]
    bins = np.unique(bins.str.split("_", expand=True)[2].astype(float).values)
    dbin = bins[1] - bins[0]
    bins = np.concatenate([bins, [bins[-1] + dbin]])

    timehorizons = np.unique(df["lag"].values)
    hist_timehorizons = np.unique(df.loc[df["variable"].str.startswith("bin"), "lag"].values)
    max_timehorizon = timehorizons[-1]

    medians = df[df["variable"] == "median"].sort_values(by="lag", ascending=True)["value"].values
    hists = {
        l: df[
            (df["lag"] == l) & df["variable"].str.startswith("bin")
        ].sort_values(by="variable", ascending=True)["value"].values
        for l in hist_timehorizons
    }

    i_color = 0
    for l, weights in hists.items():
        ax.hist(
            bins[:-1], bins=bins, weights=weights,
            histtype='stepfilled',
            color=colors[i_color],
            alpha=alpha,
            rwidth=1,
            zorder=10 + timehorizons[-1] - l,
            label=f"{l}",
        )
        ax.hist(
            bins[:-1], bins=bins, weights=weights,
            histtype='step',
            color='k',
            linewidth=0.2,
        )
        i_color = (i_color + 1) % len(colors)

    # avoid overlap of median indicators in global plot
    ymax = ax.get_ylim()[1] if ymax is None else ymax
    ymaxs = np.full(medians.size, ymax)
    if country is None:
        ymaxs *= 1.05
        ymax_frac = 0.12 * ymax
        ymaxs[[2, 3, 4, 7, 8, 10, 11, 13]] += ymax_frac
        ymaxs[[4, 13]] += ymax_frac

    ymins = np.zeros(max_timehorizon + 1)
    for l, weights in hists.items():
        ymins[l] = weights[np.argmax(medians[l] < bins) - 1]

    ax.vlines(medians, ymins, ymaxs, linestyles="dashed",
              color="black", linewidth=0.3, zorder=3)

    for i, (lag, med) in enumerate(zip(timehorizons, medians)):
        i_hist = hist_timehorizons.tolist().index(lag) if lag in hist_timehorizons else None
        color = u_plot.lighten_mcolor(
            "grey" if i_hist is None else colors[i_hist],
            1 - alpha,
        )
        zorder = (5 if i_hist is None else 20) + i
        _med_points = ax.scatter(
            [med], ymaxs[i], marker="o", s=100, facecolor=color,
            edgecolor="k", linewidths=0.5, zorder=zorder, label="Median with\nn lag years",
        )
        if i_hist is None:
            med_points = _med_points
        ax.text(
            med, 0.997 * ymaxs[i], lag,
            fontsize=7,
            ha="center",
            va="center",
            zorder=zorder + 1,
        )

    ax.axvline(0, color="black", linewidth=0.75, zorder=20)

    if country is not None:
        xwidth = bins[-1] - bins[0]
        xpad = 0.04 * xwidth
        ax.set_xlim(bins[0] - xpad, bins[-1] + xpad)
        ax.set_ylim(0, 1.1 * ymaxs[0])
        ax.locator_params(axis="x", nbins=5)

    if lims is not None:
        ax.set_xlim(*lims[0])
        ax.set_ylim(*lims[1])

    if legend:
        legend_horizontal = ax_legend is not None
        ax_legend = ax if ax_legend is None else ax_legend
        handles, labels = ax.get_legend_handles_labels()
        leg_med_points = ax_legend.legend(
            [med_points], [labels[len(hist_timehorizons)]],
            bbox_to_anchor=(0.0, 0.2) if legend_horizontal else (1.0, 1.0),
            loc="lower left" if legend_horizontal else "upper right",
            frameon=False,
            handler_map={mcollections.PathCollection: MedianPointHandler()})
        handles = ['Lag years'] + handles[:len(hist_timehorizons)]
        labels = [''] + labels[:len(hist_timehorizons)]
        ax_legend.legend(
            handles, labels,
            bbox_to_anchor=(0.4, 0.0) if legend_horizontal else (1.0, 0.7),
            loc="lower left" if legend_horizontal else "upper right",
            frameon=False,
            ncol=2,
            handler_map={str: u_plot.LegendTitle()},
        )
        ax_legend.add_artist(leg_med_points)

    if country is not None:
        ax.text(ax.get_xlim()[0], ax.get_ylim()[1], name_ctry, ha="left", va="bottom", zorder=10)

    if decorate:
        ax.set_xlabel(
            "Cumulative impact on growth per 1% change in people exposed (%)"
            if country is None else
            "Average of lag-cumulative impacts on growth (%)"
        )
    else:
        plt.setp(ax.get_xticklabels(), visible=False)

    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.axes.get_yaxis().set_visible(False)


def main():
    u_plot.main_setup_mpl()

    figsize = (u_const.PLOT_WIDTH_IN, 4.2)
    fig = plt.figure(figsize=figsize)

    # derive panel sizes, depending on the world map aspect ratio and on the figsize
    fig_ratio = figsize[0] / figsize[1]
    map_ratio = 2.0825
    map_height = 0.45
    map_width = map_ratio * map_height / fig_ratio
    map_y = 1 - map_height
    wpad = 0.03
    rpad = 0.09
    ax1_width = 1 - map_width - wpad - rpad
    ax1_x = map_width + wpad
    ax1_y = map_y + 0.09
    ax1_height = 1 - ax1_y - 0.04
    ax2_y = 0.10
    ax2_height = map_y - ax2_y - 0.04

    # shift the antimeridian to focus on the most exposed areas
    proj = ccrs.PlateCarree(central_longitude=185)

    axs = [
        fig.add_axes([0.0, map_y, map_width, map_height],
                     in_layout=False, frameon=False, projection=proj),
        fig.add_axes([ax1_x, ax1_y, ax1_width, ax1_height], in_layout=False),
        fig.add_axes([0.0, ax2_y, 1.0, ax2_height], in_layout=False),
    ]

    ax = axs[0]
    data = pd.read_csv(u_const.SOURCEDATA_DIR / "fig1a.csv")
    plot_map_countries(ax, data)
    ax.text(0.01, 0.99, "a", va="top", ha="left",
            fontweight="bold", fontsize=10, transform=ax.transAxes)

    ax = axs[1]
    data = pd.read_csv(u_const.SOURCEDATA_DIR / "fig1b.csv")
    plot_impact_cum(ax, data)
    ax.text(0, ax.get_ylim()[1], "b", fontweight="bold", fontsize=10)

    ax = axs[2]
    data = pd.read_csv(u_const.SOURCEDATA_DIR / "fig1c.csv")
    plot_impact_hist(ax, data)
    ax.text(0.01, 0.99, "c", va="top", ha="left", fontweight="bold", fontsize=10,
            transform=ax.transAxes)

    outpath = u_const.PLOTS_DIR / f"Figure1.pdf"
    print(f"Writing to {outpath} ...")
    fig.savefig(outpath)


if __name__ == "__main__":
    main()
