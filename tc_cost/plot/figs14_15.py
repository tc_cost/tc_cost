
import matplotlib.gridspec as mgridspec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from tc_cost.plot.fig3 import plot_damfun
import tc_cost.util.constants as u_const
import tc_cost.util.plot as u_plot


def plot_damfun_multi(fig, df_ell, df_mrk, df_fun):
    l_countries = np.unique(df_mrk["ISO"].values)
    n_ctry = len(l_countries)
    iso_name = pd.read_csv(u_const.ISO_NAMES_CSV)

    gs0 = mgridspec.GridSpec(1, 1, wspace=0, hspace=0)
    rows, columns = int(np.ceil(n_ctry / 3)), 3
    gs_size = rows * columns
    gs = mgridspec.GridSpecFromSubplotSpec(
        rows, columns, subplot_spec=gs0[0], wspace=0, hspace=0)
    axes = [fig.add_subplot(gs[0])]
    axes += [fig.add_subplot(gs[i], sharex=axes[0], sharey=axes[0]) for i in range(1, n_ctry)]

    for i_ax, (ax, iso) in enumerate(zip(axes, l_countries)):
        name_ctry = iso_name[iso_name.iso3 == iso].values[0][1]

        df_ell_iso = None if df_ell is None else df_ell[df_ell["ISO"] == iso]
        df_mrk_iso = df_mrk[df_mrk["ISO"] == iso]
        df_fun_iso = df_fun[df_fun["ISO"] == iso]
        rcp_colors = plot_damfun(ax, df_ell_iso, df_mrk_iso, df_fun_iso)

        ax.text(
            0.5, 0.07, f"{name_ctry}",
            va="bottom",
            ha="center",
            transform=ax.transAxes,
        )
        handles, labels = ax.get_legend_handles_labels()
        handles = list(zip(handles, rcp_colors))
        handles = handles[1:] + handles[:1]
        labels = labels[1:] + labels[:1]

        ax.xaxis.tick_top()
        ax.xaxis.set_ticks_position("both")
        ax.yaxis.set_ticks_position("both")
        ax.tick_params(axis="both", which="both", direction="in", length=2)

    ax = axes[0]

    gmt_bounds = (0.8, 4.9)
    xpad = 0.1 * (gmt_bounds[1] - gmt_bounds[0])
    xmin, xmax = (gmt_bounds[0] - xpad, gmt_bounds[1] + xpad)
    ymin, ymax = -1.9, 0.4
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)
    ax.set_yticks(np.arange(-1.6, 0.4, 0.4))
    ax.set_xticks(np.arange(1, 5))

    for i_ax, ax in enumerate(axes):
        if i_ax >= columns:
            plt.setp(ax.get_xticklabels(), visible=False)
        if i_ax % columns != 0:
            plt.setp(ax.get_yticklabels(), visible=False)

    ax0 = fig.add_subplot(gs0[0], frameon=False)
    ax0.set_xticks([])
    ax0.set_yticks([])
    ax0.xaxis.set_label_position("top")
    ax0.set_ylabel("TC-induced growth changes (percentage point)")
    ax0.set_xlabel("Global mean temperature change (°C)")
    ax0.xaxis.labelpad = 15
    ax0.yaxis.labelpad = 25

    if n_ctry < gs_size:
        ax = fig.add_subplot(gs[n_ctry], frameon=False)
    else:
        ax = fig.add_subplot(gs0[0], frameon=False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.legend(
        handles,
        labels,
        loc="lower right" if n_ctry < gs_size else "upper center",
        bbox_to_anchor=None if n_ctry < gs_size else (0.5, -0.005),
        ncol=1 if n_ctry < gs_size else 4,
        frameon=False,
        handler_map={
            tuple: u_plot.LineCIHandler("ellipse"),
        },
    )


def main():
    u_plot.main_setup_mpl()

    for fig_no in [14, 15]:
        df_ell = None
        df_mrk = pd.read_csv(u_const.SOURCEDATA_DIR / f"figs{fig_no}_markers.csv")
        df_fun = pd.read_csv(u_const.SOURCEDATA_DIR / f"figs{fig_no}_lines.csv")

        fig = plt.figure(figsize=(u_const.PLOT_WIDTH_IN, 8.5))

        plot_damfun_multi(fig, df_ell, df_mrk, df_fun)

        path = u_const.PLOTS_DIR / f"FigureS{fig_no}.pdf"
        print(f"Writing to {path} ...")
        fig.savefig(path, bbox_inches="tight")


if __name__ == "__main__":
    main()
