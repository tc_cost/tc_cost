
import matplotlib.gridspec as mgridspec
import matplotlib.pyplot as plt
import pandas as pd

from tc_cost.plot.fig2 import plot_cdadpc_to_income
import tc_cost.util.constants as u_const
import tc_cost.util.plot as u_plot


def main():
    u_plot.main_setup_mpl()

    fig = plt.figure(figsize=(u_const.PLOT_WIDTH_IN, 9))
    outer = mgridspec.GridSpec(2, 1)

    axs = [fig.add_subplot(outer[0])]
    axs.append(fig.add_subplot(outer[1], sharex=axs[0]))
    for ax, panel in zip(axs, "ab"):
        data = pd.read_csv(u_const.SOURCEDATA_DIR / f"figs13{panel}.csv")
        plot_cdadpc_to_income(ax, data)
        ax.text(-0.05, 1.03, panel, va="bottom", ha="right", fontweight="bold", fontsize=10,
                transform=ax.transAxes)
        if panel == "a":
            ax.set_xlabel(None)

    path = u_const.PLOTS_DIR / f"FigureS13.pdf"
    print(f"Writing to {path} ...")
    fig.savefig(path, bbox_inches="tight")


if __name__ == "__main__":
    main()
