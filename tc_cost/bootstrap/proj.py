
import itertools
import os
import sys

import numpy as np
import pandas as pd
import xarray as xr

import tc_cost.util.constants as u_const
import tc_cost.util.data as u_data
import tc_cost.util.io as u_io


OUTPUT_PATH = u_const.BOOTSTRAP_DIR / "growth_losses"
OUTPUT_PATH.mkdir(parents=True, exist_ok=True)


def simulate_gdppc_pathways(ds, ds_aff):
    for sc in ["full", "baseline"]:
        ds[f"{sc}_growth_losses"] = (
            ds_aff[sc]
            .rolling(year=ds.dims['aff'])
            .construct(year="aff")
            .isel(aff=slice(None, None, -1))
            .dot(ds['coeff'], dims=["aff"])
        )

    # this replaces the dataset in a nontrivial way, reducing the number of years etc.
    ds["gdppc_ini"] = ds["gdppc"].sel(year=u_const.SSP_PERIOD[0])
    ds = ds.reindex(year=np.arange(u_const.SSP_PERIOD[0] + 1, u_const.SSP_PERIOD[1] + 1))
    for sc in ["full", "baseline"]:
        ds[f'gdppc_{sc}_damage'] = ds["gdppc_ini"] * np.exp(
            (ds["gdppc_ch"] + ds[f"{sc}_growth_losses"]).cumsum(dim="year")
        )
    ds['gdppc_growth_cost'] = ds["full_growth_losses"] - ds["baseline_growth_losses"]

    return ds[[
        "gdppc_growth_cost",
        "gdppc_full_damage",
        "gdppc_baseline_damage",
    ]].compute()


def main():
    try:
        task = int(os.environ["SLURM_ARRAY_TASK_ID"])
    except:
        task = int(sys.argv[-1])

    use_ols = "ols" in sys.argv
    use_temp = "temp" in sys.argv
    suffix = f"{'_ols' if use_ols else ''}{'_temp' if use_temp else ''}"

    # we only project the TC-affected countries into the future
    countries = u_const.L_COUNTRIES

    # ~40-50 seconds per parameter set, 41 sets (~30 minutes) per job
    timehorizon, ssp, rcp = list(itertools.product(
        # 96 configurations
        np.arange(0, u_const.MAX_TIMEHORIZON + 1),
        u_const.L_SSP,
        u_const.L_RCP_LOWER,
    ))[task]

    ds_ssp = (
        u_data.load_ssp_data()
        .sel(SSP=ssp, ISO=countries)
        .reindex(
            year=np.arange(u_const.SSP_PERIOD[0] - timehorizon, u_const.SSP_PERIOD[1] + 1)
        )
    )

    ds_ssp['coeff'] = u_data.read_betas(
        timehorizon, subdir="bootstrap", group_type="glb", use_ols=use_ols, use_temp=use_temp,
    )['coeff']

    ds_aff = {
        sc: u_data.load_affected_people(rcp, ssp, sc, shares=True)
        for sc in ["full", "baseline"]
    }

    for iso in countries:
        path = OUTPUT_PATH / f"{ssp}-{timehorizon}-{rcp}-{iso}{suffix}.nc"
        if u_io.path_exists_safely(path):
            print(f"Skipping {path.name} because it already exists ...")
            continue

        ds_ssp_ctry = ds_ssp.sel(ISO=iso).copy(deep=True)
        ds_aff_ctry = {sc: ds.sel(ISO=iso) for sc, ds in ds_aff.items()}
        ds = simulate_gdppc_pathways(ds_ssp_ctry, ds_aff_ctry)
        u_io.write_safely(ds, path)


if __name__ == "__main__":
    main()
