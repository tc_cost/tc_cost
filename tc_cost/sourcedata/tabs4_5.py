
import numpy as np
import pandas as pd

from tc_cost.sourcedata.fig4 import load_aggregate_scc
import tc_cost.util.constants as u_const


def data_tabs4_5(ds_scc_agg, ssp, l_countries):
    ds = ds_scc_agg.sel(SSP=ssp, ISO=l_countries)

    dr_names = ds["dr"].values.astype(str)
    for dr_name, dr in u_const.L_DR['ramsey'].items():
        i_dr = ((ds["rho"] == dr[0]) & (ds["eta"] == dr[1])).compute().values.nonzero()[0]
        dr_names[i_dr]= dr_name
    ds = ds.assign_coords(dr=dr_names)

    df = ds[
        [f"scc{s}{q}" for s in ["", "_temp"] for q in ["50", "17", "83"]]
    ].to_dataframe().reset_index()

    df_wld = ds[
        [f"scc_wld{s}{q}" for s in ["", "_temp"] for q in ["50", "17", "83"]]
    ].to_dataframe().reset_index()
    df_wld = df_wld.rename(columns={
        c: c.replace("_wld", "")
        for c in df_wld.columns if "_wld" in c
    })
    df_wld["ISO"] = "GLB"

    return (
        pd.concat([df_wld, df])
        .rename(columns={"dr": "dr_name"})
        .drop(columns=["quantile"])
    )


def tex_tabs4_5(df):
    l_countries, inds = np.unique(df["ISO"].values, return_index=True)
    l_countries = l_countries[np.argsort(inds)]
    l_dr_names, inds = np.unique(df["dr_name"].values, return_index=True)
    l_dr_names = l_dr_names[np.argsort(inds)]
    l_rcp = np.unique(df["rcp"].values)

    df = df.set_index(["rcp", "dr_name", "ISO"])

    iso_name = pd.read_csv(u_const.ISO_NAMES_CSV).set_index("iso3")["name"].to_dict()
    iso_name["GLB"] = "Global"
    tex_str = (
        "\\begin{tabular}{llllllllllll}\n\\toprule\n"
        + "& RCP & & "
        + " & ".join([iso_name[c] for c in l_countries])
        + "\\\\"
    )
    for i_dr, dr_name in enumerate(l_dr_names):
        for i_rcp, rcp in enumerate(l_rcp):
            for i_tc, w_tc in enumerate([True, False]):
                s = "" if w_tc else "_temp"
                if i_rcp == 0 and i_tc == 0:
                    tex_str += "\\midrule\n"
                    tex_str += "\\multirow{6}{*}{\\rotatebox[origin=c]{-90}{%s}}" % (dr_name)
                tex_str += " & "
                if i_tc == 0:
                    tex_str += (
                        "\\multirow{2}{*}{%.1f}"
                        % (float(rcp[-2:]) / 10)
                    )
                tex_str += " & "
                tex_str += "%s\,TCs " % ("w/" if w_tc else "w/o")

                for iso in l_countries:
                    df_ctry = df.loc[(rcp, dr_name, iso)]
                    d50 = df_ctry[f"scc{s}50"].item()
                    d17 = df_ctry[f"scc{s}17"].item()
                    d83 = df_ctry[f"scc{s}83"].item()
                    d50_s = f"{d50:.0f}"
                    inc = ""
                    if w_tc:
                        inc = d50 / df_ctry[f"scc_temp50"].item() - 1
                        inc = f" [{100 * inc:+.0f}\%]"
                    tex_str += f"& {d50_s} ({d17:.0f} – {d83:.0f}){inc} "

                tex_str += "\\\\\n"
    tex_str += "\\bottomrule\n\\end{tabular}"
    return tex_str


def main():
    timehorizon = 8
    use_ols = True
    use_temp = True

    ds_scc_agg = load_aggregate_scc(timehorizon, use_ols, use_temp)

    # Top 5 TC-affected countries (by TC-SCC according to main specification)
    ds_def = ds_scc_agg.sel(rcp="rcp60", SSP="SSP2")
    def_dr_name = "Ricke"
    def_dr = u_const.L_DR['ramsey'][def_dr_name]
    def_dr_indexer = ((ds_def.rho == def_dr[0]) & (ds_def.eta == def_dr[1])).compute()
    ds_def = ds_def.sel(dr=def_dr_indexer).squeeze()
    l_countries = list(ds_def["ISO"].sortby(ds_def["scc_tc50"], ascending=False).values[:5])

    for i_ssp, ssp in enumerate(u_const.L_SSP):
        path = u_const.SOURCEDATA_DIR / f"tabs{4 + i_ssp}.csv"
        df = data_tabs4_5(ds_scc_agg, ssp, l_countries)
        print(f"Writing to {path} ...")
        df.to_csv(path, index=None)

        path = u_const.TABLES_DIR / f"tabs{4 + i_ssp}.tex"
        s_tex = tex_tabs4_5(df)
        print(f"Writing to {path} ...")
        with open(path, "w") as fp:
            fp.write(s_tex)


if __name__ == "__main__":
    main()
