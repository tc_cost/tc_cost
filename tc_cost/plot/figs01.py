
import matplotlib.gridspec as mgridspec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import tc_cost.util.constants as u_const
import tc_cost.util.plot as u_plot


def plot_tdf_comparison(
    gs, pred_vals, l_plotdata, reorder_legend=None, legend_ncol=2,
):
    # comparison plot of temperature damage functions (parabolas)
    fig = plt.gcf()
    ncols = len(l_plotdata)
    inner = mgridspec.GridSpecFromSubplotSpec(
        3, ncols, subplot_spec=gs, height_ratios=[5, 5, 1.5], hspace=0.4,
    )
    axs = np.array([
        [fig.add_subplot(inner[0, 0])] + [None] * (ncols - 1),
        [fig.add_subplot(inner[1, 0])] + [None] * (ncols - 1),
    ])
    for i in range(1, ncols):
        axs[0, i] = fig.add_subplot(inner[0, i], sharex=axs[0, 0], sharey=axs[0, 0])
        axs[1, i] = fig.add_subplot(inner[1, i], sharex=axs[1, 0], sharey=axs[1, 0])

    handles = []
    labels = []
    for i_col, (title, plotdata) in enumerate(l_plotdata.items()):
        for conf, temp_pred, tc_pred, _ in plotdata:
            label, color, plot_kwargs = conf
            for i_row, (predicted, pred_ci) in enumerate([temp_pred, tc_pred]):
                if predicted is None:
                    continue
                ax = axs[i_row, i_col]
                prvals = pred_vals[i_row]
                pred_max = 0
                ax.plot(
                    prvals,
                    predicted - pred_max,
                    color=color,
                    label=label,
                    **plot_kwargs,
                )

                if pred_ci is None:
                    continue
                ax.fill_between(
                    prvals,
                    pred_ci[:, 0] - pred_max,
                    pred_ci[:, 1] - pred_max,
                    alpha=0.1,
                    facecolor=color,
                )

        # configure Axes
        ax = axs[0, i_col]
        if title != "":
            ax.set_title(title)
        ax.set_ylabel("Change in ln(GDP per capita)")
        ax.set_yticks([-0.1, 0.0, 0.1])
        ax.set_xlabel("Annual average temperature (°C)")
        ax.set_xticks([0, 5, 10, 15, 20, 25, 30])
        ax.set_xlim(-3, 30.5)
        ax.spines[['left', 'right', 'top']].set_visible(False)
        ax.spines['bottom'].set_bounds([0, 30])

        ax = axs[1, i_col]
        ax.axhline(0, color="black", linestyle=":", linewidth=0.5, zorder=20)
        ax.set_ylabel("Cumulative impact on growth\nper 1% change in exposed people (%)")
        ax.set_yticks([0.01, 0.0, -0.01, -0.02, -0.03])
        ax.yaxis.set_major_formatter(lambda v, pos: f"{100 * v:.0f}")
        ax.set_xlabel("Lag year")
        ax.set_xticks(pred_vals[1])
        ax.set_xlim(pred_vals[1][0], pred_vals[1][-1])
        ax.spines[['left', 'right', 'top']].set_visible(False)

        for ax in [axs[0, i_col], axs[1, i_col]]:
            for h, l in zip(*ax.get_legend_handles_labels()):
                if l not in labels:
                    handles.append(h)
                    labels.append(l)

    ax = fig.add_subplot(inner[2, 0], frameon=False)
    ax.set_xticks([])
    ax.set_yticks([])
    if reorder_legend is not None:
        handles = [handles[i] for i in reorder_legend]
        labels = [labels[i] for i in reorder_legend]
    ax.legend(
        handles, labels,
        frameon=False,
        ncol=legend_ncol,
        loc="lower center",
    )


def main():
    u_plot.main_setup_mpl()

    fig = plt.figure(figsize=(u_const.PLOT_WIDTH_IN, 7))
    gs = mgridspec.GridSpec(1, 1)

    labels = {
        "bhm_True_True 1961-2010": "Burke et al. 2015 data w/ TC-effects (1961-2010)",
        "bhm_True_False 1961-2010": (
            "Burke et al. 2015 data w/o TC-effects (1961-2010)"
        ),
        "own_True_True 1961-2010": "Our data w/ TC-effects (1961-2010)",
        "own_True_False 1961-2010": "Our data w/o TC-effects (1961-2010)",
        "own_True_True 1981-2015": "Our main specification (1981-2015)",
        "own_True_False 1981-2015": "Our main specification w/o TC-effects (1981-2015)",
        "own_False_True 1981-2015": "Our main specification w/o temp. effects (1981-2015)",
    }

    colors = {
        "bhm_True_True 1961-2010": "tab:blue",
        "bhm_True_False 1961-2010": "tab:blue",
        "own_True_True 1961-2010": "tab:orange",
        "own_True_False 1961-2010": "tab:orange",
        "own_True_True 1981-2015": "tab:green",
        "own_True_False 1981-2015": "tab:green",
        "own_False_True 1981-2015": "tab:purple",
    }

    data = pd.read_csv(u_const.SOURCEDATA_DIR / "figs01.csv")
    data["data"] = data["data"].str.replace("Burke et al. 2015", "bhm")
    data["key"] = (
        data["data"]
        + "_" + data["temp_effects"].astype(str)
        + "_" + data["tc_effects"].astype(str)
        + " " + data["period"]
    )

    pred_vals = [
        np.unique(data.loc[data["variable"] == "temp", "x"].values),
        np.unique(data.loc[data["variable"] == "tc", "x"].values),
    ]

    l_plotdata = []
    for key, label in labels.items():
        df = data[data["key"] == key].sort_values(by="x", ascending=True)
        notc = not df["tc_effects"].values[0]
        notemp = not df["temp_effects"].values[0]
        color = colors[key]
        plot_kwargs = dict(
            lw=(
                2 if notc
                else 1 if notemp
                else 0.5
            ),
            ls="--" if notemp else ":" if notc else None,
        )
        pred = {}
        for v in ["temp", "tc"]:
            var_mask = (df["variable"] == v)
            skip = (v == "temp" and notemp) or (v == "tc" and notc)
            pred[v] = (None, None) if skip else (
                df.loc[var_mask, "y_mean"].values,
                np.stack([
                    df.loc[var_mask, "y_5"].values,
                    df.loc[var_mask, "y_95"].values,
                ], axis=1),
            )
        l_plotdata.append((
            (label, color, plot_kwargs),
            pred["temp"], pred["tc"], None,
        ))

    l_plotdata = {"": l_plotdata}

    plot_tdf_comparison(
        gs[0], pred_vals, l_plotdata,
        reorder_legend=[0, 1, 2, 3, 4, 5, 6],
    )

    outpath = u_const.PLOTS_DIR / f"FigureS01.pdf"
    print(f"Writing to {outpath} ...")
    fig.savefig(outpath, bbox_inches="tight")


if __name__ == "__main__":
    main()
