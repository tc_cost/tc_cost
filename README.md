This is the code to replicate the results (including data preprocessing, regressions and plots) from
the following paper:

```
Krichene, H., Vogt, T., Piontek, F., Geiger, T., Schötz, C., Otto, C. (2023): The Social Cost of Tropical Cyclones.
Submitted to Nature Communications.
```

The version of the code that was originally used to generate the results in the paper is stored permanently in
a [Zenodo repository](https://dx.doi.org/10.5281/zenodo.8056520). You find the most recent version of the code in
the [official git repository](https://gitlab.pik-potsdam.de/tc_cost/tc_cost).


Input data
----------

Depending on how you obtained this code, it might be necessary to obtain additional input data from third parties
to run it.

The following data is taken from the Ricke et al. 2018 repository:

- socioeconomic data along SSP pathways,
- national population-weighted temperature time series,
- the Burke et al. damage function,
- population-weighted temperature response of an additional emission pulse.

If you obtained this code from Zenodo, the Ricke et al. 2018 repository is already included (in a subdirectory
called `cscc-paper-2018`). Otherwise, the "Installation" section below contains instructions on how to obtain the data.

In addition to that, more input data from third parties is required:

- global mean temperature time series as used by
the [tropical cyclone emulator](https://gitlab.pik-potsdam.de/tovogt/tc_emulator) in `./data/input/GMT.nc`,
- the [input data](https://purl.stanford.edu/wb587wt4560) of [Burke et al. 2015](https://dx.doi.org/10.1038/nature15725)
in `./data/input/GrowthClimateDataset.dta`,
- historical GDP per capita data from [James et al. 2012](https://doi.org/10.1186/1478-7954-10-12) (downloaded
from [IHME](https://ghdx.healthdata.org/record/ihme-data/gross-domestic-product-gdp-estimates-country-1950-2015)),
stored in `./data/input/IHME_GLOBAL_GDP_ESTIMATES_1950_2015.csv`,
- population-weighted average national temperature time series for the historical period
in `./data/input/mean_temperature_gswp3-w5e5.csv`,
- the global mean temperature response of an additional emission pulse according
to [Ricke & Caldeira 2014](https://dx.doi.org/10.1088/1748-9326/9/12/124002)
in `./data/input/pulse_response_ricke_caldeira_2014.csv`,
- historical (national) numbers of people affected by tropical cyclones according
to [TCE-DAT](https://doi.org/10.5880/pik.2017.011)
in `./data/input/tcdata/TCE-DAT_historic-exposure_1950-2015.csv` with the corresponding total population counts
in `./data/input/tcdata/TotalPopulation.csv`,
- projected (national) shares of people affected by tropical cyclones according to
the [tropical cyclone emulator](https://gitlab.pik-potsdam.de/tc_cost/tc_emulator) as computed by the scripts in
the [corresponding repository](https://gitlab.pik-potsdam.de/tc_cost/tc_people_affected)
in `./data/input/tcdata/emulator/`,
- the bulk data from the [World Inequality Database](https://wid.world/bulk_download/wid_all_data.zip) stored
in `./data/input/wid_all_data.zip`.

For reproducibility, a full copy of all input data sets listed above is stored permanently in
a [Zenodo repository](https://dx.doi.org/10.5281/zenodo.8063450). Make sure to place all the files in the correct
locations (listed above) before running the scripts.


Installation
------------

Set up a new conda environment, install this repository using pip, and install the remaining R requirements as follows:

```bash
$ conda env create -n tc_cost -f requirements.yml
$ conda activate tc_cost
(tc_cost) $ pip install -e ./
(tc_cost) $ R -e 'install.packages(c("meboot", "robustbase"), repos="https://ftp.fau.de/cran/")'
```

If you obtained this repository via git, you also need to clone the Ricke et al. 2018 repository as follows:

```bash
(tc_cost) $ git submodule update --init
```


Running the scripts
-------------------

The main scripts are submodules of the `tc_cost.bootstrap` package and can be run like this:

```bash
(tc_cost) $ python -m tc_cost.bootstrap.bootstrap
```

Since running the whole pipeline with 100 TC realizations and 1200 bootstraps requires a lot of time, the scripts are
optimised for running on a SLURM cluster. The file `sbatch_pipeline.sh` contains code to run the whole pipeline from
generating the bootstraps, fitting the panel model, projecting the disturbed GDPpc pathways, computing the discounted
annual damages, to fitting the temperature damage functions and computing the social cost of carbon.

Once the pipeline is ready, the source data for the plots and tables (published together with the paper) can be
generated using the submodules of the `tc_cost.sourcedata` package, like this:

```bash
(tc_cost) $ python -m tc_cost.sourcedata.fig1
```

Source data is stored as CSV files in `./data/sourcedata/`, for example `./data/sourcedata/fig1.csv`.
Similarly, plots are generated using the submodules of the `tc_cost.plot` package, like this:


```bash
(tc_cost) $ python -m tc_cost.plot.fig1
```

Plots are stored as PDF files in `./data/plots/`, for example `./data/plots/Figure1.pdf`.
