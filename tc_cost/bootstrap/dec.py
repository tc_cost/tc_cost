
import itertools
import os
import sys

import numpy as np
import pandas as pd
import xarray as xr

import tc_cost.util.constants as u_const
import tc_cost.util.data as u_data
import tc_cost.util.io as u_io


OUTPUT_DIR = u_const.BOOTSTRAP_DIR / "dec"
OUTPUT_DIR.mkdir(parents=True, exist_ok=True)


def compute_dec(ds, disc_growth):
    year0 = ds['year'].values[0]

    gdppc_var = {
        "SSP": "gdppc",
        "baseline": "gdppc_baseline_damage",
        "damaged": "gdppc_full_damage",
    }[disc_growth]
    ds['dgrowth'] = np.log(ds[gdppc_var]) - np.log(ds[gdppc_var].sel(year=year0))
    ds['dfac'] = np.exp(-ds['rho'] * ds['dyear'] - ds['eta'] * ds['dgrowth'])

    ds['marginal_dec_pc'] = ds['dfac'] * ds['marginal_tc_cost']

    return ds[['marginal_dec_pc', 'rho', 'eta']].compute()


def load_loss_data(ds_ssp, timehorizon, iso, suffix):
    ssp = ds_ssp.SSP.item()

    ds = xr.open_mfdataset([
        u_const.BOOTSTRAP_DIR / "growth_losses"
        / f"{ssp}-{timehorizon}-{rcp}-{iso}{suffix}.nc"
        for rcp in u_const.L_RCP_LOWER
    ], combine="nested", concat_dim=["rcp"])
    ds['rcp'] = ("rcp", u_const.L_RCP_LOWER)

    ds['marginal_tc_cost'] = ds["gdppc_baseline_damage"] - ds["gdppc_full_damage"]

    ds['gdppc'] = ds_ssp.sel(ISO=iso, year=ds['year'].values)['gdppc']

    dr_values = np.array(
        sum([list(l_dr.values()) for l_dr in u_const.L_DR.values()], [])
    )
    ds['rho'] = ('dr', dr_values[:, 0])
    ds['eta'] = ('dr', dr_values[:, 1])

    year0 = ds['year'].values[0]
    ds['dyear'] = ds['year'] - year0

    return ds.compute()


def main():
    try:
        task = int(os.environ["SLURM_ARRAY_TASK_ID"])
    except:
        task = int(sys.argv[-1])

    use_ols = "ols" in sys.argv
    use_temp = "temp" in sys.argv
    suffix = f"{'_ols' if use_ols else ''}{'_temp' if use_temp else ''}"

    # to reduce number of jobs, split countries into 8 groups
    l_countries = np.array_split(u_const.L_COUNTRIES, 8)

    # ~5 minutes per parameter set, 15 sets (~75 minutes) per job
    timehorizon, ssp, countries = list(itertools.product(
        # 256 configurations
        np.arange(0, u_const.MAX_TIMEHORIZON + 1),
        u_const.L_SSP,
        l_countries,
    ))[task]

    ds_ssp = u_data.load_ssp_data().sel(SSP=ssp)

    for iso in countries:
        paths = {
            disc_growth: OUTPUT_DIR / (
                f"{ssp}-{timehorizon}-{iso}_{disc_growth}{suffix}.nc"
            )
            for disc_growth in ["baseline"]
            # the alternative growth rates "SSP" and "damaged" are not considered any more
        }

        if all(u_io.path_exists_safely(p) for p in paths.values()):
            continue

        print(f"Reading loss data for {iso} ...")
        ds = load_loss_data(ds_ssp, timehorizon, iso, suffix)

        for disc_growth, path in paths.items():
            if u_io.path_exists_safely(path):
                continue

            ds_dec = compute_dec(ds, disc_growth)
            u_io.write_safely(ds_dec, path)


if __name__ == "__main__":
    main()
