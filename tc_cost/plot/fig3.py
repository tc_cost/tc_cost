
import matplotlib.collections as mcollections
import matplotlib.colors as mcolors
import matplotlib.gridspec as mgridspec
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
import numpy as np
import pandas as pd

import tc_cost.util.constants as u_const
import tc_cost.util.plot as u_plot


def plot_damfun(ax, df_ell, df_mrk, df_fun):
    conf_ellipses = None
    if df_ell is not None:
        ds_ell = (
            df_ell
            .set_index(["rcp", "v", "w"])
            .to_xarray()
            .reindex(v=["gmt", "cost"])
            .reindex(w=["gmt", "cost"])
        )
        ds_ell["means"] = ds_ell["means"].isel(w=0)

        conf_ellipses = {}
        n_std = 1.0
        for rcp in ds_ell["rcp"].values:
            ds_sel = ds_ell.sel(rcp=rcp)
            cov = np.array([
                [ds_sel["covs"].values[0, 0], 100 * ds_sel["covs"].values[0, 1]],
                [100 * ds_sel["covs"].values[1, 0], 100**2 * ds_sel["covs"].values[1, 1]],
            ])
            means = np.array([ds_sel["means"].values[0], 100 * ds_sel["means"].values[1]])

            # explicit formula for eigenvalues of a 2x2 covariance matrix
            pearson = cov[0, 1] / (
                1 if cov[0, 1] == 0
                else np.sqrt(cov[0, 0] * cov[1, 1])
            )
            width = 2 * np.sqrt(1 + pearson)
            height = 2 * np.sqrt(1 - pearson)

            # compute transform of the confidence ellipse
            transform = (
                mtransforms.Affine2D()
                .rotate_deg(45)
                .scale(*[np.sqrt(cov[i, i]) * n_std for i in [0, 1]])
                .translate(*means)
            )
            conf_ellipses[rcp] = (width, height, transform)

    l_rcps = ["all"] + u_const.L_RCP_LOWER
    rcp_colors = [u_const.L_RCP_COLORS[rcp] for rcp in l_rcps]
    rcp_styles = ["-", "--", "-.", ":"]

    for rcp, rcp_st, rcp_cl in zip(l_rcps, rcp_styles, rcp_colors):
        rcp_name = (
            "Fixed effects shared by RCPs"
            if rcp == "all" else
            f"RCP{float(rcp[-2:]) / 10:.1f}"
        )
        if rcp != "all":
            if conf_ellipses is not None:
                width, height, transform = conf_ellipses[rcp]
                ellipse = mpatches.Ellipse(
                    (0, 0), width=width, height=height,
                    facecolor='none',
                    edgecolor=rcp_cl,
                    alpha=0.7,
                    linewidth=0.5,
                    zorder=2,
                )
                ellipse.set_transform(transform + ax.transData)
                ax.add_patch(ellipse)

            df_sel = df_mrk[df_mrk["rcp"] == rcp]
            heights = 100 * df_sel["cost_range"].values
            widths = np.full_like(heights, 0.04)
            angles = np.zeros_like(heights)
            offsets = np.stack([
                df_sel["gmt"].values,
                100 * df_sel["cost_mid"].values,
            ], axis=1)
            ax.add_collection(mcollections.EllipseCollection(
                widths, heights, angles,
                units="xy",
                offsets=offsets,
                offset_transform=ax.transData,
                facecolor=mcolors.to_rgb(rcp_cl) + (0.15,),
                edgecolor="none",
                zorder=1,
            ))

        df_rcp = (
            df_fun[df_fun["rcp"] == ("__all__" if rcp == "all" else rcp)]
            .sort_values(by=["gmt"])
        )
        ax.plot(
            df_rcp["gmt"].values,
            100 * df_rcp["50"],
            color=0.7 * np.array(mcolors.to_rgb(rcp_cl)) + 0.3 * np.zeros(3),
            lw=0.75,
            linestyle=rcp_st,
            label=rcp_name,
            zorder=100,
        )
        ax.fill_between(
            df_rcp["gmt"].values,
            100 * df_rcp["17"],
            100 * df_rcp["83"],
            alpha=0.3,
            facecolor=rcp_cl,
            edgecolor="none",
            zorder=10 if rcp == "all" else 5,
        )

    return rcp_colors


def plot_damfun_standalone(ax, df_ell, df_mrk, df_fun, panel=None):
    rcp_colors = plot_damfun(ax, df_ell, df_mrk, df_fun)
    handles, labels = ax.get_legend_handles_labels()
    handles = list(zip(handles, rcp_colors))
    handles = handles[1:] + handles[:1]
    labels = labels[1:] + labels[:1]
    ax.legend(
        handles, labels,
        loc="lower left", ncol=1, frameon=False,
        handler_map={
            tuple: u_plot.LineCIHandler("ellipse"),
        },
    )

    ax.set_ylabel("TC-induced growth changes\npercentage point")
    ax.set_xlabel("Global mean temperature change (°C)")

    xlim = (df_fun["gmt"].min(), df_fun["gmt"].max())
    xpad = 0.01 * (xlim[1] - xlim[0])
    ax.set_xlim(xlim[0] - xpad, xlim[1] + xpad)

    ylim = (100 * df_fun["17"].min(), 100 * df_fun["83"].max())
    ypad = 0.07 * (ylim[1] - ylim[0])
    ax.set_ylim(ylim[0] - ypad, ylim[1] + 2 * ypad)

    if panel is not None:
        ax.text(-0.085, 1.01, panel, va="bottom", ha="right", fontweight="bold", fontsize=10,
                transform=ax.transAxes)


def plot_damfun_slopes(gs0, df, fontsize=6):
    box_xgap = 0.13
    group_sizes = {
        g: (df["group"] == g).sum()
        for g in u_const.COUNTRY_GROUP_ORDER
    }

    y1, y2 = (df["17"].min(), df["83"].max())
    lims = y1 - 0.28 * (y2 - y1), y2 + 0.02 * (y2 - y1)

    df = df.merge(
        pd.read_csv(u_const.ISO_NAMES_CSV).rename(columns={"iso3": "ISO"}),
        on="ISO", how="left",
    )
    df["name"] = df["name"].fillna(df["ISO"])

    df["significant"] = df["17"] * df["83"] > 0
    df = (
        df
        .sort_values(by=["group", "significant", "median"], ascending=[True, False, True])
        .reset_index(drop=True)
    )

    fig = plt.gcf()
    outer_grid = mgridspec.GridSpecFromSubplotSpec(
        1, 2, subplot_spec=gs0, width_ratios=[1, 10], wspace=0,
    )
    inner_grid = mgridspec.GridSpecFromSubplotSpec(
        1, sum(group_sizes.values()), subplot_spec=outer_grid[0, 1], wspace=0.8,
    )
    cumul_sizes = np.cumsum([0] + list(group_sizes.values())[:-1])
    axs = {
        group: fig.add_subplot(inner_grid[0, c:c + s])
        for (group, s), c in zip(group_sizes.items(), cumul_sizes)
    }

    for group_i, (group, ax) in enumerate(axs.items()):
        ax.set_ylim(lims)
        if group_i > 0:
            plt.setp(ax.get_yticklabels(), visible=False)
        ax.set_xlim((1, 0))
        ax.set_xticks([])
        ax.set_xlabel({
            "L": "Low-income" + " " * (8 + 2 * int(fontsize - 6)),
            "LM": "Lower-middle-income" + " " * (1 + 10 * int(fontsize - 6)),
            "UM": "Upper-middle-income" + " " * (2 * int(fontsize - 6)),
            "H": "High-income",
        }[group])

        gdata = df[df["group"] == group].copy()
        invwidth = group_sizes[group]
        for i, (_, row) in enumerate(gdata.iterrows()):
            x1 = i / invwidth
            x2 = (i + 1) / invwidth
            if i % 2 == 0:
                ax.fill_betweenx(
                    lims,
                    x1,
                    x2,
                    color=u_const.TAB20_COLORS["lightgrey"],
                    alpha=0.5,
                    linewidth=0,
                )

            pos = row["median"]
            limL, limU = row["17"], row["83"]
            name = row["name"]

            significant = row["significant"]
            if not significant:
                color = u_const.TAB20_COLORS["lightgray"]
            elif pos < 0:
                color = u_const.TAB20_COLORS["lightred"]
            else:
                color = u_const.TAB20_COLORS["lightgreen"]

            ax.text(
                1 - (x2 + x1) / 2,
                0.01,
                name,  # + ("*" if significant else ""),
                horizontalalignment="center",
                verticalalignment="bottom",
                transform=ax.transAxes,
                fontsize=fontsize,
                alpha=1 if significant else 0.5,
                rotation=90,
            )

            ax.add_patch(
                mpatches.Rectangle(
                    (x1 + box_xgap / invwidth, limL),
                    (1 - 2 * box_xgap) / invwidth,
                    limU - limL,
                    fc=color,
                    ec="black",
                    linewidth=0.2,
                )
            )
            ax.plot(
                (x1 + box_xgap / invwidth, x2 - box_xgap / invwidth),
                (pos, pos),
                linewidth=0.2,
                color="black",
            )

        ax.axhline(0, color="black", linestyle=":", linewidth=0.5)

    ax = fig.add_subplot(outer_grid[0, 1], frameon=False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_ylabel(
        "Country-specific cumulative\ngrowth-effect per 1°C change\nin global mean temperature",
        labelpad=27,
    )


def main():
    u_plot.main_setup_mpl()

    fig = plt.figure(figsize=(u_const.PLOT_WIDTH_IN, 5.55))
    outer = mgridspec.GridSpec(2, 1, hspace=0.26, height_ratios=[1, 1.3])
    inner = mgridspec.GridSpecFromSubplotSpec(
        1, 2, subplot_spec=outer[0], wspace=0, width_ratios=[1, 13])

    ax = fig.add_subplot(inner[1])
    df_ell = pd.read_csv(u_const.SOURCEDATA_DIR / "fig3a_ellipses.csv")
    df_mrk = pd.read_csv(u_const.SOURCEDATA_DIR / "fig3a_markers.csv")
    df_fun = pd.read_csv(u_const.SOURCEDATA_DIR / "fig3a_lines.csv")
    plot_damfun_standalone(ax, df_ell, df_mrk, df_fun, panel="a")

    data = pd.read_csv(u_const.SOURCEDATA_DIR / "fig3b.csv")
    plot_damfun_slopes(outer[1], data)

    ax = fig.add_subplot(outer[1], frameon=False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.text(-0.01, 1.01, "b", va="bottom", ha="right", fontweight="bold", fontsize=10,
            transform=ax.transAxes)

    path = u_const.PLOTS_DIR / f"Figure3.pdf"
    print(f"Writing to {path} ...")
    fig.savefig(path, bbox_inches="tight")


if __name__ == "__main__":
    main()
