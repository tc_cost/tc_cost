
import pickle
import sys

import matplotlib.pyplot as plt
import matplotlib.gridspec as mgridspec
import matplotlib.lines as mlines
import matplotlib.patches as mpatches
import numpy as np
import pandas as pd
import scipy.linalg
import xarray as xr

import tc_cost.util.constants as u_const
import tc_cost.util.data as u_data
import tc_cost.util.plot as u_plot


def compute_ci_nobootstrap_ols(timehorizon, glb, countries, cumulative):
    # compute (cumulative) coefficients with confidence intervals using error propagation
    base_path = u_const.NOBOOTSTRAP_DIR
    prefix = "glb" if glb else "ctry"
    fname = f"{prefix}_{u_const.INDICATOR}_1981-2015_{timehorizon}_ols.pickle"
    inpath = base_path / "coefficients" / fname
    with open(inpath, "rb") as fp:
        res = pickle.load(fp)

    exog = pd.DataFrame(
        data=np.zeros(((timehorizon + 1) * len(countries), res.params.size)),
        columns=res.params.index.values,
    )
    aff_cols = [
        f"aff{lag}:C(ISO)[{c}]"
        for c in countries
        for lag in range(timehorizon + 1)
    ]
    exog[aff_cols] = scipy.linalg.block_diag(*[
        np.tril(np.ones(timehorizon + 1)) if cumulative else np.eye(timehorizon + 1)
        for c in countries
    ])
    print(res.pvalues[aff_cols])
    result = {
        "med": res.model.predict(res.params, exog),
    }
    res_pred = res.get_prediction()
    pred_ci = res_pred.conf_int(alpha=0.99)
    result["q17"], result["q83"] = pred_ci[:, 0], pred_ci[:, 1]
    pred_ci = res_pred.conf_int(alpha=0.95)
    result["qmin"], result["qmax"] = pred_ci[:, 0], pred_ci[:, 1]
    result_shape = (len(countries), timehorizon + 1)
    for k in result.keys():
        result[k] = pd.DataFrame(
            data=result[k].reshape(result_shape),
            columns=[f"aff{lag}" for lag in range(timehorizon + 1)],
            index=countries,
        )
    return result


def plot_betas(timehorizon, cumulative, average_annual_aff):
    base_read_args = (timehorizon,)
    read_kwargs = {
        'ctry': {},
        'ctry_sub_ctry_1': dict(subdir="sub_ctry/config1"),
        'ctry_legacy': dict(legacy=True),
        'ctry_nb_glmrob': dict(nobootstrap=True),
        'ctry_nb_ols': dict(nobootstrap=True, use_ols=True),
        'grp_config1': dict(group_type="grp", subdir=f"sub_ctry/config1"),
        'grp_config2': dict(group_type="grp", subdir=f"sub_ctry/config2"),
        'global': dict(group_type="glb"),
        'global_sub_ctry': dict(group_type="glb", subdir="sub_ctry"),
        'global_sub_ctry_1': dict(group_type="glb", subdir="sub_ctry/config1"),
        'global_bs': dict(group_type="glb", subdir="bootstrap"),
        'global_bs_ols': dict(group_type="glb", subdir="bootstrap", use_ols=True),
        'global_bs_ols_temp': dict(
            group_type="glb", subdir="bootstrap", use_ols=True, use_temp=True,
        ),
        'global_nb': dict(group_type="glb", nobootstrap=True),
        'global_nb_single': dict(group_type="glb", nobootstrap=True, nb_elt="1981-2015"),
        'global_all_ctry': dict(group_type="glb", subdir="all_ctry"),
    }
    for l in range(timehorizon + 1):
        read_kwargs[f'global_nb_l{l}'] = read_kwargs['global_nb']
        read_kwargs[f'global_nb_single_l{l}'] = read_kwargs['global_nb_single']
        read_kwargs[f'global_bs_ols_l{l}'] = read_kwargs['global_bs_ols']

    read_args = {k: base_read_args for k in read_kwargs.keys()}
    read_args.update({'global': (min(8, timehorizon),)})
    for l in range(timehorizon + 1):
        read_args[f'global_nb_l{l}'] = (l,)
        read_args[f'global_nb_single_l{l}'] = (l,)
        read_args[f'global_bs_ols_l{l}'] = (l,)

    plot_settings = {
        "ctry": ('tab:orange', '--', "country beta\n(41 countries)"),
        "ctry_sub_ctry_1": ('tab:blue', '--', "country beta\n(14 countries)"),
        "ctry_legacy": ('tab:red', '--', "country (legacy)\nbeta + alpha"),
        "ctry_nb_glmrob": ('grey', '-', "no bootstrap"),
        "ctry_nb_ols": ('tab:green', '-', "no bootstrap\n(OLS)"),
        "grp_config1": ('tab:red', '--', "group beta\n(basins)"),
        "grp_config2": ('tab:green', '--', "group beta\n(size)"),
        "global": ('tab:blue', '-', "global beta\n(41 countries,\n 1971-2014)"),
        "global_sub_ctry": ('tab:orange', '-', "global beta\n(41 countries,\n 1981-2015)"),
        "global_sub_ctry_1": ('grey', '-', "global beta\n(14 countries)"),
        "global_bs": ('grey', '-', "global beta\n(41 c., new,\n 1981-2015)"),
        "global_bs_ols": ('tab:orange', '-', "global beta\n(41 c., OLS,\n 1981-2015)"),
        "global_nb": ('grey', '-', "no bootstrap\n(global,\n 35 years)"),
        "global_nb_single": ('grey', '-', "no bootstrap\n(global,\n 1981-2015)"),
        "global_all_ctry": ('tab:red', '-', "global beta\n(all countries)"),
    }
    for l in range(timehorizon + 1):
        plot_settings[f'global_nb_l{l}'] = plot_settings['global_nb']
        plot_settings[f'global_nb_single_l{l}'] = plot_settings['global_nb_single']
        plot_settings[f'global_bs_ols_l{l}'] = plot_settings['global_bs_ols']

    # selection = [f'global_nb_single_l{l}' for l in range(timehorizon + 1)]
    # selection = [f'global_nb_l{l}' for l in [2, 5, 9, 15]]
    # selection = [f'global_bs_ols_l{l}' for l in range(timehorizon + 1)]
    # selection = [f'global_bs_l{l}' for l in [2, 5, 9, 15]]
    selection = ["global_bs_ols", "global_bs"]

    only_global = not average_annual_aff and all(k.startswith("global") for k in selection)

    betas = {}
    for k in selection:
        betas[k] = u_data.read_betas(*read_args[k], **read_kwargs[k])
        if "ISO" not in betas[k].dims:
            betas[k]['coeff'] = xr.broadcast(betas[k]['coeff'], betas[k]['ISO'])[0]
        betas[k] = betas[k].to_dataframe().reset_index()

    groups = [k[4:] for k in selection if k.startswith("grp_")]
    group_dfs = {g: get_group_spec(g) for g in groups}

    if average_annual_aff:
        avg_aff_df = (
            u_data.load_avg_aff(timehorizon)
            .reindex(year=np.arange(1981, 2016))['avg_aff']
            .to_dataframe()
            .reset_index()
            .rename(columns={"avg_aff": "value"})
        )

    for key, df in betas.items():
        if average_annual_aff:
            df = df.merge(avg_aff_df, on=["ISO", "aff"], how="left")
            df["coeff"] *= df["value"]
            df = df.drop(columns="value")

        df = df.sort_values(by=["elt", "ISO", "aff"])

        if cumulative:
            df["coeff"] = df.groupby(by=["ISO", "elt"]).coeff.cumsum()

        # for %-units we apply exp, but close to zero, this is almost indistinguishable:
        df["coeff"] = 100 * (np.exp(df["coeff"]) - 1)

        betas[key] = {'all': df}
        df = df.groupby(by=["ISO", "aff"])
        betas[key].update({
            'med': df["coeff"].quantile(q=0.50).reset_index(),
            'q17': df["coeff"].quantile(q=0.17).reset_index(),
            'q83': df["coeff"].quantile(q=0.83).reset_index(),
            'min': df["coeff"].agg("min").reset_index(),
            'max': df["coeff"].agg("max").reset_index(),
        })

        if key == "ctry_nb_ols":
            countries = betas[key]['med'].index.values
            betas[key].update(
                compute_ci_nobootstrap_ols(
                    *read_args, False, countries, cumulative,
                )
            )

    extreme = {
        op: getattr(pd.concat(
            [getattr(betas[s][op].groupby("ISO").coeff, op)() for s in selection], axis=1,
        ), op)(axis=1)
        for op in ["min", "max"]
    }
    countries = (extreme['max'] - extreme['min']).sort_values().index.values
    if only_global:
        countries = countries[:1]

    iso_name = pd.read_csv(u_const.ISO_NAMES_CSV)

    sel_countries = ["USA", "JPN", "CHN", "IND", "PHL", 'KHM', 'MOZ']
    n_ctry = len(countries)

    if only_global:
        rows, columns = 1, 2
        figsize = (10, 5.5)
        width_ratios = (8, 2)
    else:
        rows, columns = 7, 6
        figsize = (13, 14)
        width_ratios = None
    fig = plt.figure(figsize=figsize)
    gs0 = mgridspec.GridSpec(1, 1, wspace=0, hspace=0)
    gs = mgridspec.GridSpecFromSubplotSpec(
        rows, columns, subplot_spec=gs0[0], wspace=0, hspace=0, width_ratios=width_ratios)

    axes = []
    for i_row in range(rows):
        ax0 = fig.add_subplot(gs[columns * i_row], sharex=axes[0] if i_row > 0 else None)
        n_cols = columns if i_row + 1 < rows else columns - 1
        axes += [ax0]
        axes += [fig.add_subplot(gs[columns * i_row  + i], sharex=axes[0], sharey=ax0)
                 for i in range(1, n_cols)]

    handles = None

    for i_ctry, iso in enumerate(countries):
        ax = axes[i_ctry]
        ax.axhline(0, c='gray', linestyle=':', linewidth=0.75)
        name_ctry = iso_name[iso_name.iso3 == iso].values[0][1]

        ctry_group_names = {}
        if any(k.startswith("grp_") for k in selection):
            ctry_group_names = {
                key: df.loc[df.ISO == iso, 'group'].values[0]
                for key, df in group_dfs.items()
            }

        c_betas = {
            key: {
                k: df[df.ISO == iso].set_index("aff")['coeff']
                for k, df in dfs.items() if k != 'all'
            }
            for key, dfs in betas.items()
        }
        line_handles = []
        for key in selection:
            color, linestyle, _ = plot_settings[key]
            values = {
                q: c_betas[key][q] for q in ["med", "q17", "q83", "min", "max"]
            }

            if values["med"] is None:
                continue

            k_x = values["med"].index.values
            line_handles.append(
                ax.plot(k_x, values["med"], c=color, linestyle=linestyle, linewidth=0.75)[0]
            )
            if (values["max"] - values["min"]).max() != 0:
                alpha = 0.05 if len(selection) > 10 else 0.2
                fill_kwargs = dict(facecolor=color, alpha=alpha, edgecolor="none")
                ax.fill_between(k_x, values["q17"], values["q83"], **fill_kwargs)
                ax.fill_between(k_x, values["min"], values["max"], **fill_kwargs)

        xmin, xmax = ax.get_xlim()
        xmid = 0.5 * (xmin + xmax)
        if only_global:
            country_title = "Global"
        else:
            country_title = f"{name_ctry}"
            if len(ctry_group_names) > 0:
                country_title += f" ({', '.join(ctry_group_names.values())})"
        ax.text(0.5, 0.95, country_title, va="top", ha="center", transform=ax.transAxes,
                fontweight='bold' if iso in sel_countries else 'normal')

        if len(line_handles) == len(selection) and handles is None:
            handles, labels = ax.get_legend_handles_labels()
            # exclude duplicate labels from legend
            selection_uniq_ind = np.unique(
                [plot_settings[k][2] for k in selection], return_index=True
            )[1]
            line_handles = [
                 # pairs of handle and color for CIs
                 (line_handles[i], plot_settings[selection[i]][0])
                 for i in selection_uniq_ind
            ]
            line_labels = [plot_settings[selection[i]][2] for i in selection_uniq_ind]
            handles = line_handles + handles
            labels = line_labels + labels

        ax.xaxis.tick_top()
        ax.xaxis.set_ticks_position("both")
        ax.yaxis.set_ticks_position("both")
        ax.tick_params(axis="both", which="both", direction="in", length=2)

    for i_row in range(rows):
        ax = axes[i_row * columns]
        x = np.arange(timehorizon + 1)
        if i_row == 0:
            ax.set_xticks(x[::2] if x.size > 9 else x)
        ymin, ymax = ax.get_ylim()
        min_ymax = 0.2 * (ymax - ymin)
        if ymax < min_ymax:
            ax.set_ylim(ymin, min_ymax)

    for i_ax, ax in enumerate(axes):
        if i_ax >= columns:
            plt.setp(ax.get_xticklabels(), visible=False)
        if i_ax % columns != 0:
            plt.setp(ax.get_yticklabels(), visible=False)

    ax0 = fig.add_subplot(gs0[0], frameon=False)
    ax0.set_xticks([])
    ax0.set_yticks([])
    ax0.xaxis.set_label_position("top")
    ax0.set_ylabel(
        (f"Average of {'lag-cumulative' if cumulative else 'lag-specific'} impacts on growth (%)")
        if average_annual_aff else
        (f"{'Cumulative impact' if cumulative else 'Impact'} on growth "
         "per 1% change in exposed people (%)")
    )
    ax0.set_xlabel("Lag years")
    ax0.xaxis.labelpad = 25
    ax0.yaxis.labelpad = 40

    ax = fig.add_subplot(gs[n_ctry], frameon=False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.legend(
        handles,
        labels,
        loc="center",
        ncol=1,
        frameon=False,
        handler_map={
            tuple: u_plot.LineCIHandler("none"),
        })

    fname = (
        f"{timehorizon}{'_cum' if cumulative else ''}{'_avgaff' if average_annual_aff else ''}.pdf"
    )
    path = u_const.PLOTS_DIR / "betas" / fname
    print(f"Writing to {path} ...")
    fig.savefig(path, bbox_inches="tight")


def main():
    try:
        max_timehorizon = int(sys.argv[-1])
    except:
        max_timehorizon = 9
    plot_betas(max_timehorizon, "cum" in sys.argv[1:], "avg_aff" in sys.argv[1:])


if __name__ == "__main__":
    main()
