
import itertools
import os
import sys

import numpy as np
import xarray as xr

import tc_cost.util.constants as u_const
import tc_cost.util.io as u_io


def main():
    try:
        task = int(os.environ["SLURM_ARRAY_TASK_ID"])
    except:
        task = int(sys.argv[-1])

    timehorizon = 8
    disc_growth = "baseline"
    dr_name = "Ricke"
    use_ols = "ols" in sys.argv
    use_temp = "temp" in sys.argv
    suffix = f"{'_ols' if use_ols else ''}{'_temp' if use_temp else ''}"

    ssp, rcp = list(itertools.product(
        # 6 configurations
        u_const.L_SSP,
        u_const.L_RCP_LOWER,
    ))[task]

    path = u_const.BOOTSTRAP_DIR / "dec" / (
        f"aggregate_unc_{ssp}-{timehorizon}-{rcp}_{disc_growth}_{dr_name}{suffix}.nc"
    )
    if u_io.path_exists_safely(path):
        return

    inpaths = [
        u_const.BOOTSTRAP_DIR / "dec" /
        f"aggregate_unc_{ssp}-{timehorizon}-{rcp}_{disc_growth}_{dr_name}-{real}{suffix}.nc"
        for real in range(100)
    ]

    ds = xr.open_mfdataset(inpaths, combine="nested", concat_dim=["realisation"])
    ds['realisation'] = ("realisation", np.arange(100))

    u_io.write_safely(ds, path, unlink=inpaths)


if __name__ == "__main__":
    main()
