
import numpy as np
import pandas as pd

from tc_cost.sourcedata.fig2 import concat_dad_ssp
import tc_cost.util.constants as u_const
import tc_cost.util.data as u_data


def data_figs12a(timehorizon, disc_growth, use_ols, use_temp):
    gwp_2021 = u_data.load_ssp_data()['gdp'].sel(year=2021).sum(dim="ISO")
    l_dec = []
    ec_columns = [f"marginal_dec_{p:02d}" for p in [50, 17, 83]]
    for ssp in u_const.L_SSP:
        df = concat_dad_ssp(timehorizon, ssp, disc_growth, use_ols, use_temp)
        df = df[~np.isnan(df['year'])].copy()
        df = df.groupby(["year", "rcp", "rho", "eta"]).sum().reset_index()
        df[ec_columns] = df[ec_columns] / gwp_2021.sel(SSP=ssp).item() * 100
        df["SSP"] = ssp
        l_dec.append(df)
    df = pd.concat(l_dec)
    return (
        df
        .drop(columns=["ISO"] + [c for c in df.columns if "dec_pc" in c])
        .rename(columns={f"marginal_dec_{p:02d}": f"dec{p}" for p in [50, 17, 83]})
    )


def data_figs12b(timehorizon, disc_growth, use_ols, use_temp, dr_name):
    dr = u_const.L_DR['ramsey'][dr_name]

    l_countries = u_const.L_COUNTRIES

    # Compare two scenarios: the most liberal vs the most conservative
    l_rcp = ["rcp60", "rcp85"]
    l_ssp = ["SSP2", "SSP5"]

    all_dfs = []
    for i, (ssp, rcp) in enumerate(zip(l_ssp, l_rcp)):
        df = concat_dad_ssp(timehorizon, ssp, disc_growth, use_ols, use_temp)
        df = df[
            df['year'].isna()
            & (df["rho"] == dr[0])
            & (df["eta"] == dr[1])
            & (df["rcp"] == rcp)
        ].copy()
        df["SSP"] = ssp
        all_dfs.append(df)
    df = (
        pd.concat(all_dfs)
        .rename(columns={f"marginal_dec_pc_{p:02d}": f"decpc{p}" for p in [50, 17, 83]})
    )
    df = df.drop(columns=["year", "rho", "eta"] + [c for c in df.columns if "_dec_" in c])
    df["dr_name"] = dr_name

    return df


def main():
    timehorizon = 8
    use_ols = True
    use_temp = True
    dr_name = "Ricke"
    disc_growth = "baseline"

    path = u_const.SOURCEDATA_DIR / "figs12a.csv"
    df = data_figs12a(timehorizon, disc_growth, use_ols, use_temp)
    print(f"Writing to {path} ...")
    df.to_csv(path, index=None)

    path = u_const.SOURCEDATA_DIR / "figs12b.csv"
    df = data_figs12b(timehorizon, disc_growth, use_ols, use_temp, dr_name)
    print(f"Writing to {path} ...")
    df.to_csv(path, index=None)


if __name__ == "__main__":
    main()
